package com.example.advmvvm.di


import com.fss.mtcdemo.api.ApiService
import com.fss.mtcdemo.api.retrofit.ApiServiceOutSide
import com.fss.mtcdemo.ui.account.AccountRepository
import com.fss.mtcdemo.ui.addDevice.main.AddDeviceRepository
import com.fss.mtcdemo.ui.device.DeviceControlParentRepository
import com.fss.mtcdemo.ui.device.main.DeviceControlRepository
import com.fss.mtcdemo.ui.device.verifyPin.VerifyPinRepository
import com.fss.mtcdemo.ui.home.HomeActRepository
import com.fss.mtcdemo.ui.home.main.HomeRepository
import com.fss.mtcdemo.ui.login.loginByPhone.LoginByPhoneRepository
import com.fss.mtcdemo.ui.login.pin.PinRepository
import com.fss.mtcdemo.ui.motion.archive.ArchiveMotionRepository
import com.fss.mtcdemo.ui.motion.cloud.CloudMotionRepository
import com.fss.mtcdemo.ui.motion.sdcard.SDCardRepository
import com.fss.mtcdemo.ui.motonPlayback.main.MotionPlaybackRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * Created by anhdv31 on 2/21/2022.
 */
@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {
    
    @Provides
    fun provideLoginByPhoneProvider(apiService: ApiService): LoginByPhoneRepository{
        return LoginByPhoneRepository(apiService)
    }
    
    @Provides
    fun provideHomeProvider(apiService: ApiService): HomeRepository{
        return HomeRepository(apiService)
    }
    
    @Provides
    fun provideHomeActProvider(apiService: ApiService): HomeActRepository {
        return HomeActRepository(apiService)
    }
    
    @Provides
    fun provideAccountProvider(apiService: ApiService): AccountRepository {
        return AccountRepository(apiService)
    }
    
    @Provides
    fun provideSDCardProvider(): SDCardRepository{
        return SDCardRepository()
    }
    
    @Provides
    fun provideCloudMotionProvider(): CloudMotionRepository{
        return CloudMotionRepository()
    }
    
    @Provides
    fun provideMotioPlaybackRepository(): MotionPlaybackRepository {
        return MotionPlaybackRepository()
    }
    
    @Provides
    fun provideArchiveMotionRepository(): ArchiveMotionRepository {
        return ArchiveMotionRepository()
    }
    
    @Provides
    fun provideAddDeviceRepository(): AddDeviceRepository {
        return AddDeviceRepository()
    }

    @Provides
    fun providePinRepository(apiServiceOutSide: ApiServiceOutSide,
                             apiServiceInSide: ApiService): PinRepository {
        return PinRepository(apiServiceOutSide,apiServiceInSide)
    }
    
    @Provides
    fun provideVerifyPinRepository(apiServiceOutSide: ApiServiceOutSide): VerifyPinRepository {
        return VerifyPinRepository(apiServiceOutSide)
    }
    
    @Provides
    fun provideDeviceControlRepository(apiService: ApiService,apiServiceOutSide: ApiServiceOutSide): DeviceControlRepository {
        return DeviceControlRepository(apiService,apiServiceOutSide)
    }
    
    @Provides
    fun provideDeviceControlParentRepository(): DeviceControlParentRepository {
        return DeviceControlParentRepository()
    }
}