package com.fss.mtcdemo.utils.date;

import static com.fss.mtcdemo.utils.date.DateFormat.DATE_FORMAT_19;
import static com.fss.mtcdemo.utils.date.DateFormat.DATE_FORMAT_20;
import static com.fss.mtcdemo.utils.date.DateFormat.DATE_FORMAT_28;
import static com.fss.mtcdemo.utils.date.DateFormat.DATE_FORMAT_32;
import static com.fss.mtcdemo.utils.date.DateFormat.DATE_FORMAT_5;
import static com.fss.mtcdemo.utils.date.DateFormat.SERVER_FORMAT_DATE_TIME;
import static com.fss.mtcdemo.utils.date.DateFormat.TYPE_CONVERT;

import android.annotation.SuppressLint;
import android.text.format.DateFormat;

import com.fss.mtcdemo.utils.LocaleManager;
import com.fss.mtcdemo.utils.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public final class DateUtils {

    public static Calendar getCalendarByString(String time, String format) {
        SimpleDateFormat dfm = new SimpleDateFormat(format, Locale.getDefault());
        try {
            Date dt = dfm.parse(time);
            Calendar calendar = Calendar.getInstance(Locale.getDefault());
            if (dt != null) {
                calendar.setTime(dt);
            }
            return calendar;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date getDateByCalendar(Calendar calendar) {
        if (calendar == null)
            return null;
        return calendar.getTime();
    }


    public static Calendar getCalendarTimestamp(long timestamp) {
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        cal.setTimeInMillis(timestamp * 1000);
        return cal;
    }

    public static Date getDateByTimestamp(long timestamp) {
        return getCalendarTimestamp(timestamp).getTime();
    }

    public static String getStringDateTimeStamp(long timeStamp, String format) {
        Calendar cal = getCalendarTimestamp(timeStamp);
        return DateFormat.format(format, cal).toString();
    }

    public static String getDateByTimeStamp(long time, String format) {
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        cal.setTimeInMillis(time * 1000);
        return DateFormat.format(format, cal).toString();
    }

    public static String getCurrentDateToString(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        return sdf.format(new Date());
    }

    public static String getCurrentTime(String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    public static String getCurrentTime(String format, Locale locale) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, locale);
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    public static String getTimeByDate(Date date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
        return dateFormat.format(date);
    }

    public static Long getTimestamp(Calendar calendar) {
        if (null == calendar) {
            return null;
        }
        return getTimestamp(calendar.getTime());
    }

    public static Long getTimestamp(Date date) {
        if (null == date) {
            return null;
        }
        return date.getTime() / 1000L;
    }

    public static Long getTimestampCurrent() {
        return getTimestamp(Calendar.getInstance());
    }


    public static String convertFormatToFormat(String time, String format, String formatTo) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        SimpleDateFormat sdfs = new SimpleDateFormat(formatTo, Locale.getDefault());
        try {
            Date dt = sdf.parse(time);
            if (dt != null) {
                return sdfs.format(dt);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    //55520
    public static String getTimesByStringFormat(String serverTime, String formatDate, String formatUi) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatDate, Locale.getDefault());
        try {
            Date date = sdf.parse(serverTime);
            SimpleDateFormat sdf2 = new SimpleDateFormat(formatUi, Locale.getDefault());
            if (date != null) {
                return sdf2.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static long convertStringDateToLong(String dateInString, String format) {
        if (dateInString == null || StringUtils.isEmpty(dateInString)) {
            return 0L;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        Date date;
        try {
            date = sdf.parse(dateInString);
            if (date != null) {
                return date.getTime() / 1000;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0L;
    }


    // yyyy-mm-dd 00:00:00 -> (String)23:00 - 19-02-2019
    public static String convertDateToString(Date date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(format);
        String[] dateFomat = dateFormat.format(date).split(" ");

        return dateFomat[0] +
                " - " +
                dateFomat[1];
    }


    public static String convertDateToString(Calendar calendar, String format) {
        if (calendar == null) return "";
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }

    @SuppressLint("SimpleDateFormat")
    public static String parseDate(String time) {
        String inputPattern = SERVER_FORMAT_DATE_TIME;
        String outputPattern;
        String language = LocaleManager.getInstance().getLanguage();
        if (language.equals("vi")) {
            outputPattern = DATE_FORMAT_5;
        } else {
            outputPattern = DATE_FORMAT_20;
        }

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    //length=0;beginTime=2020-06-10 14:52:51;endTime=2020-06-10 14:53:50;TotalTimes=60
    public static int parsePlayBeginTimeUrl(String str, String format) {
        try {
            if (str.contains("=")) {
                str = str.substring(str.indexOf("=") + 1);
            }
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);
            return (int) (sdf.parse(str).getTime() / 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String addDay(String format, String dayStart, int value) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);
            Calendar cal = Calendar.getInstance();
            cal.setTime(dateFormat.parse(dayStart));
            cal.add(Calendar.DATE, value);
            return getTimeByDate(cal.getTime(), format);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
        //return cal.getTime()
    }

    public static long timeInFutureMilisecond(String startTime, String endTime) {
        long result = 0;
        long DELAYTIME = 500;
        long MILISECOND = 1000;
        long currentTime = System.currentTimeMillis() / MILISECOND;

        if (startTime.isEmpty() || endTime.isEmpty()) return -1;
        String currentTimeDate = DateUtils.getStringDateTimeStamp(currentTime, TYPE_CONVERT);
        String startTimeStr = startTime;
        String endTimeStr = endTime;

        int startTimeValue = Integer.parseInt(startTimeStr.replace(":", ""));
        int endTimeValue = Integer.parseInt(endTimeStr.replace(":", ""));

        int currentTimeValue = Integer.parseInt(DateUtils.getStringDateTimeStamp(currentTime, DATE_FORMAT_19).replace(":", ""));

        String tomorrowDay = DateUtils.addDay(TYPE_CONVERT, currentTimeDate, 1);
        String toDay = DateUtils.addDay(TYPE_CONVERT, currentTimeDate, 0);

        String endTimeTomorrowDay = endTime + " " + tomorrowDay;
        String endTimeToDay = endTime + " " + toDay;
        String startTimeToDay = startTime + " " + toDay;
        String startTimeTomorrowDay = startTime + " " + tomorrowDay;

        long endTimeTomorrowDayValue = DateUtils.convertStringDateToLong(endTimeTomorrowDay, DATE_FORMAT_28);
        long endTimeToDayValue = DateUtils.convertStringDateToLong(endTimeToDay, DATE_FORMAT_28);
        long startTimeToDayvalue = DateUtils.convertStringDateToLong(startTimeToDay, DATE_FORMAT_28);
        long startTimeTomorrowDayValue = DateUtils.convertStringDateToLong(startTimeTomorrowDay, DATE_FORMAT_28);

        if (currentTimeValue >= startTimeValue && currentTimeValue <= endTimeValue) {
            if (endTimeValue < startTimeValue) {
                result = (endTimeTomorrowDayValue - currentTime);
            } else {
                if (currentTime > endTimeToDayValue) {
                    result = Math.abs(currentTime - endTimeTomorrowDayValue);
                } else {
                    result = endTimeToDayValue - currentTime;
                }
            }
            if (startTimeValue == endTimeValue) {
                result = (endTimeToDayValue - currentTime);
            }
            return addDelayTime(DELAYTIME, result);
        }
        if (currentTimeValue < startTimeValue) {
            result = startTimeToDayvalue - currentTime;
            return addDelayTime(DELAYTIME, result);
        }
        /*
        sta: 8:30
        cur: 9:00
        end: 4:30
        */
        if (currentTimeValue >= endTimeValue && currentTimeValue >= startTimeValue) {
            if (startTimeValue >= endTimeValue) {
                result = Math.abs(currentTime - startTimeTomorrowDayValue);
            } else {
                result = Math.abs(currentTime - endTimeTomorrowDayValue);
            }
            return addDelayTime(DELAYTIME, result);
        }
        return addDelayTime(DELAYTIME, result);
    }

    public static long addDelayTime(long timeDelayMili, long rootTimeMili) {
        return rootTimeMili > 0 ? rootTimeMili * 1000 + timeDelayMili : rootTimeMili;
    }

    public static int getDayInWeekValue() {
        int dayValue = 0;
        Locale locale = new Locale("EN", "US");
        String day = getCurrentTime(DATE_FORMAT_32, locale);
        switch (day.toLowerCase()) {
            case "mon":
                dayValue = 0;
                break;
            case "tue":
                dayValue = 1;
                break;
            case "wed":
                dayValue = 2;
                break;
            case "thur":
                dayValue = 3;
                break;
            case "fri":
                dayValue = 4;
                break;
            case "sat":
                dayValue = 5;
                break;
            case "sun":
                dayValue = 6;
                break;
        }
        return dayValue;
    }
}
