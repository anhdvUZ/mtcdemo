package com.fss.mtcdemo.utils.audio;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.lib.MTC.MTC;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * Created by anhdv31 on 4/28/2022.
 */
public class RecordAudioV1 {
    private String TAG = this.getClass().getSimpleName();
    private static RecordAudioV1 INSTANCE = null;
    private Thread mStreamThread;
    private AudioRecord mAudioRecord;
    private boolean mStatus;
    private Context mContext;
    private int mBuffsize = 400;
    public static RecordAudioV1 getInstance() {
        if (INSTANCE == null) {
            synchronized (RecordAudioV1.class) {
                INSTANCE = new RecordAudioV1();
            }
        }
        return INSTANCE;
    }

    private int sampleRate = 8000; //44100;
    private int channelConfig = AudioFormat.CHANNEL_IN_MONO;
    private int audioFormat = AudioFormat.ENCODING_PCM_16BIT;
    int minBufSize = AudioRecord.getMinBufferSize(sampleRate, channelConfig, audioFormat);

    private int mLoginId =-1;

    public void startStreamingEncoding(Context context, int loginId) {
        mContext = context;
        mLoginId = loginId;
        mStatus = true;
        mStreamThread = new Thread(new Runnable() {

            @Override
            public void run() {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
                byte[] buffer = new byte[mBuffsize];
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                            (Activity) mContext,
                            new String[]{Manifest.permission.RECORD_AUDIO},
                            1
                    );
                }

                mAudioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                        sampleRate, channelConfig,
                        audioFormat,
                        minBufSize * 10);
                mAudioRecord.startRecording();

                /////Encoding:
                CMG711 encoder = new CMG711();
                byte[] outBuffer = new byte[mBuffsize];

                int read, encoded;
                while (mStatus) {
                    read = mAudioRecord.read(buffer, 0, buffer.length);
                    Log.d(TAG, "read: " + read);

                    //Encoding:
                    encoded = encoder.encode(buffer, 0, read, outBuffer);
                    MTC.DEV_AudioSend(mLoginId,outBuffer,mBuffsize);
                }

            }

        });
        mStreamThread.start();
    }

    public void stopStreamingEncoding() {
        mStreamThread.interrupt();
        mAudioRecord.stop();
        mStatus = false;
    }
}
