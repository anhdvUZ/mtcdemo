package com.fss.mtcdemo.utils

/**
 * Created by anhdv31 on 4/6/2022.
 */
object StringUtils {
    @JvmStatic
    fun isEmpty(cs: CharSequence?): Boolean {
        return cs == null || cs.length == 0
    }
    
    fun getRandomString(length: Int) : String {
        val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
        return (1..length)
            .map { allowedChars.random() }
            .joinToString("")
    }
}