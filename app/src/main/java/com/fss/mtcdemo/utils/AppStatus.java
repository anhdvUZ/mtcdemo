package com.fss.mtcdemo.utils;

import android.content.Context;

import com.fss.mtcdemo.AppApplication;
import com.fss.mtcdemo.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by anhdv31 on 10/31/2022.
 */
public class AppStatus {
    public static final int SUCCESS = 0;
    public static final int OK_E2E_READY_TO_COMBINE = 0;
    public static final int LOGIN_SUCCESS = 0;
    public static final int EE_UNKNOWN = -111111;
    public static final int EE_ADDCAM_MARKPIN = -100004 ;
    public static final int EE_INVALID_PIN = -100001;
    public static final int EE_E2E_NOT_READY_TO_COMBINE = -100002;
    public static final int EE_CONNECT_DEVICE_FAILED = -100003;
    public static final int EE_E2E_PUT_KEY = -100004;
    public static final int EE_INVALID_QRCODE = -100005;

    private static Map<Integer, Integer> mStatusStrMap = new HashMap<Integer, Integer>();

    static{
        //mStatusStrMap.put(OK_E2E_READY_TO_COMBINE, R.string.EE_OK);
        mStatusStrMap.put(EE_INVALID_PIN, R.string.text_invalid_pin);
        mStatusStrMap.put(EE_E2E_NOT_READY_TO_COMBINE, R.string.text_not_ready_to_combine);
        mStatusStrMap.put(EE_E2E_PUT_KEY, R.string.text_error_put_key);
        mStatusStrMap.put(EE_UNKNOWN, R.string.text_error_unknown);
    }

    public static String getErrorStr(Integer errCode) {
        Integer errStrId = mStatusStrMap.get(errCode);
        if ( null != errStrId ) {
            return AppApplication.getAppContext().getResources().getString(errStrId);
        }
        return "Unknown Error [" + errCode + "]";
    }
}
