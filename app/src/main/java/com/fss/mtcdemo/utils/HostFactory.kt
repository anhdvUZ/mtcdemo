package  com.fss.mtcdemo.utils;

import com.fss.mtcdemo.BuildConfig

/**
 * Created by anhdv31 on 2/17/2022.
 */
object HostFactory {
    const val API_HOST = BuildConfig.URL_SM + ":" + BuildConfig.PORT_SM + "/"
}