package com.fss.mtcdemo.utils.date;

public interface DateFormat {
    //https://developer.android.com/reference/java/text/SimpleDateFormat
    /*https://androidwave.com/format-datetime-in-android/*/
    /*https://androidwave.com/format-datetime-in-android/*/
    String CLIENT_FORMAT_DATE_TIME_MOMENT = "HH:mm:ss dd/MM/yyyy";

    String SERVER_FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
    String TYPE_CONVERT_INBOX = "dd-MM-yyyy";
    String TYPE_CONVERT = "dd/MM/yyyy";
    String CLIENT_FORMAT_DATE_TIME = "HH:mm:ss dd/MM/yyyy";
    String DATE_FORMAT_1 = "hh:mm a";
    String DATE_FORMAT_2 = "h:mm a";
    String DATE_FORMAT_3 = "yyyy-MM-dd";
    String DATE_FORMAT_4 = "dd-MMMM-yyyy";
    String DATE_FORMAT_5 = "dd MMMM yyyy";
    String DATE_FORMAT_6 = "dd MMMM yyyy zzzz";
    String DATE_FORMAT_7 = "EEE, MMM d, ''yy";
    String DATE_FORMAT_8 = "yyyy-MM-dd HH:mm:ss";
    String DATE_FORMAT_9 = "h:mm a dd MMMM yyyy";
    String DATE_FORMAT_10 = "K:mm a, z";
    String DATE_FORMAT_11 = "hh 'o''clock' a, zzzz";
    String DATE_FORMAT_12 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    String DATE_FORMAT_13 = "E, dd MMM yyyy HH:mm:ss z";
    String DATE_FORMAT_14 = "yyyy.MM.dd G 'at' HH:mm:ss z";
    String DATE_FORMAT_15 = "yyyyy.MMMMM.dd GGG hh:mm aaa";
    String DATE_FORMAT_16 = "EEE, d MMM yyyy HH:mm:ss Z";
    String DATE_FORMAT_17 = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    String DATE_FORMAT_18 = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    String DATE_FORMAT_19 = "HH:mm";
    String DATE_FORMAT_20 = "dd MMM yyyy";
    String DATE_FORMAT_21 = "ddMMyyyy-HHmmss";
    String DATE_FORMAT_22 = "EEEE dd/MM/yyyy hh:mm a";
    String DATE_FORMAT_23 = "EEE dd/MM/yyyy hh:mm a";
    String DATE_FORMAT_24 = "yyyy-MM-dd_hh:mm:ss";//2020-12-29_14:46:18
    String DATE_FORMAT_25 = "mm:ss";
    String DATE_FORMAT_26 = "EEE dd/MM/yyyy HH:mm";
    String DATE_FORMAT_27 = "EEEE dd/MM/yyyy HH:mm";
    String DATE_FORMAT_START = "yyyy-MM-dd 00:00:00";
    String DATE_FORMAT_END = "yyyy-MM-dd 23:59:59";

    String DATE_FORMAT_28 = "HH:mm dd/MM/yyyy";
    String DATE_FORMAT_29 = "HH:mm:ss - dd/MM/yyyy";
    String DATE_FORMAT_30 = "HH:mm:ss";
    String DATE_FORMAT_31 = "HHmmss";
    String DATE_FORMAT_32 = "EEE";
    String DATE_FORMAT_33 = "EEE, d MM yyyy HH:mm";
    String DATE_FORMAT_34 = "HH:mm:ss, dd/MM/yyyy";
    String DATE_FORMAT_35 = "dd MMMM, yyyy";
    String DATE_FORMAT_36 = "MMMM dd, yyyy";
    String DATE_START_TIME = "00:00:00";
    String DATE_END_TIME = "23:59:59";
    String DATE_HALF_TIME = "11:59:59";
}
