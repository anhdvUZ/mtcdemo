package  com.fss.mtcdemo.utils;

import com.fss.mtcdemo.BuildConfig

/**
 * Created by anhdv31 on 2/17/2022.
 */
object Constants {
    /*    companion object AccountInfo{
            @JvmStatic
            public var IS_DEBUG = BuildConfig.DEBUG
            val uuid : String = "b7661b2fd0a642a88d956kmmflclvmtq"
            val uname : String = "admin"
            val pwd : String = "888888"
        }*/
    var IS_DEBUG = BuildConfig.DEBUG
    const val DEFAULT_VALUE_COUNTRY_CODE = 84
    const val DEFAULT_VALUE_STRING = ""
    const val DEFAULT_VALUE_INT = 0
    const val DEFAULT_VALUE_DOUBLE = 0.0
    const val DEFAULT_VALUE_LONG = 0L
    const val DEFAULT_VALUE_BOOLEAN = false
    const val PBKDF_DEFAULT_ITERATIONS = 100
    
    const val PORT_01 = 9011
    const val PORT_02 = 9012
    const val PORT_03 = 9013
    const val PORT_04 = 9014
    const val PORT_05 = 9015
    const val PORT_06 = 9016
}