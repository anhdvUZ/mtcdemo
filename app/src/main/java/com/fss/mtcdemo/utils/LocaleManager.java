package com.fss.mtcdemo.utils;

import static android.os.Build.VERSION_CODES.JELLY_BEAN_MR1;
import static android.os.Build.VERSION_CODES.N;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;

import java.util.Locale;

public class LocaleManager {
    public static final String[] arraysLanguage = {"vi", "en"};
    private String languageDefault;
    private static LocaleManager INSTANCE = null;

    public static LocaleManager getInstance() {
        synchronized (LocaleManager.class) {
            if (INSTANCE == null) {
                INSTANCE = new LocaleManager();
            }
        }
        return INSTANCE;
    }

    public void initialized(Context context) {
        executiveLanguageLocale(context);
    }

    void executiveLanguageLocale(Context context) {
        Resources resources = Utility.getTopLevelResources(context);
        Locale locale = getLocale(resources);
        String languageLocale = locale.getLanguage();
        // Lấy ngôn ngữ đầu tiên vào app
        if (arraysLanguage[0].equalsIgnoreCase(languageLocale)) {
            languageDefault = arraysLanguage[0];
        } else {
            languageDefault = arraysLanguage[1];
        }
    }

    public String getLanguage() {
        /*String languageSave = getLanguageSavePref();
        return StringUtils.isEmpty(languageSave) ? languageDefault : languageSave;*/
        return "en";
    }

    public static Locale getLocale(Resources res) {
        Configuration config = res.getConfiguration();
        return Utility.isAtLeastVersion(N) ? config.getLocales().get(0) : config.locale;
    }
}