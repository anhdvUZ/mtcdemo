package com.fss.mtcdemo.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.media.*
import android.util.Log
import androidx.core.app.ActivityCompat


/**
 * Created by anhdv31 on 28/04/2022.
 */
class AudioRec {

    companion object{
        private const val TAG = "AUDIO_RECORD_PLAYBACK"
        private const val isRunning = true
        private var m_thread /* Thread for running the Loop */: Thread? = null

        private var recorder: AudioRecord? = null
        private var track: AudioTrack? = null
        private lateinit var mContext: Context

        var bufferSize = 320 /* Buffer for recording data */
        var buffer = ByteArray(bufferSize)

       /* var instance = AudioRec(mContext)
        @JvmName("getInstance1")
        private fun getInstance(): AudioRec {
            if (instance == null)
                instance = AudioRec(mContext)
            return instance
        }*/

         fun do_loopback(flag: Boolean,context: Context) {
            mContext = context
            m_thread = Thread { run_loop(flag) }
            m_thread?.start()
        }

        fun findAudioTrack(track: AudioTrack?): AudioTrack? {
            var track = track
            Log.d(TAG, "===== Initializing AudioTrack API ====")
            val m_bufferSize = AudioTrack.getMinBufferSize(
                8000,
                AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT
            )
            if (m_bufferSize != AudioTrack.ERROR_BAD_VALUE) {
                track = AudioTrack(
                    AudioManager.STREAM_MUSIC, 8000,
                    AudioFormat.CHANNEL_OUT_MONO,
                    AudioFormat.ENCODING_PCM_16BIT, m_bufferSize,
                    AudioTrack.MODE_STREAM
                )
                if (track.state == AudioTrack.STATE_UNINITIALIZED) {
                    Log.e(TAG, "===== AudioTrack Uninitialized =====")
                    return null
                }
            }
            return track
        }


        fun findAudioRecord(recorder: AudioRecord?): AudioRecord? {
            var recorder = recorder
            Log.d(TAG, "===== Initializing AudioRecord API =====")
            val m_bufferSize = AudioRecord.getMinBufferSize(
                8000,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT
            )
            if (m_bufferSize != AudioRecord.ERROR_BAD_VALUE) {

                if (ActivityCompat.checkSelfPermission(
                        (mContext as Activity?)!!,
                        Manifest.permission.RECORD_AUDIO
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // TODO: Consider calling
                    ActivityCompat.requestPermissions(
                        (mContext as Activity?)!!,
                        arrayOf(Manifest.permission.RECORD_AUDIO),
                        1
                    )
                }
                recorder = AudioRecord(
                    MediaRecorder.AudioSource.MIC, 8000,
                    AudioFormat.CHANNEL_IN_MONO,
                    AudioFormat.ENCODING_PCM_16BIT, m_bufferSize
                )
                if (recorder.state == AudioRecord.STATE_UNINITIALIZED) {
                    Log.e(TAG, "====== AudioRecord UnInitilaised ====== ")
                    return null
                }
            }
            return recorder
        }

        fun run_loop(isRunning: Boolean) {
            /** == If Stop Button is pressed ==  */
            if (isRunning == false) {
                Log.d(TAG, "=====  Stop Button is pressed ===== ")
                if (AudioRecord.STATE_INITIALIZED == recorder?.getState()) {
                    recorder?.stop()
                    recorder?.release()
                }
                if (AudioTrack.STATE_INITIALIZED == track?.getState()) {
                    track?.stop()
                    track?.release()
                }
                return
            }
            /** ======= Initialize AudioRecord and AudioTrack ========  */
            recorder = findAudioRecord(recorder)
            if (recorder == null) {
                Log.e(TAG, "======== findAudioRecord : Returned Error! =========== ")
                return
            }
            track = findAudioTrack(track)
            if (track == null) {
                Log.e(TAG, "======== findAudioTrack : Returned Error! ========== ")
                return
            }
            if (AudioRecord.STATE_INITIALIZED == recorder?.getState() &&
                AudioTrack.STATE_INITIALIZED == track?.getState()
            ) {
                recorder?.startRecording()
                Log.d(TAG, "========= Recorder Started... =========")
                track?.play()
                Log.d(TAG, "========= Track Started... =========")
            } else {
                Log.d(TAG, "==== Initilazation failed for AudioRecord or AudioTrack =====")
                return
            }
            /** ------------------------------------------------------  */

            /* Recording and Playing in chunks of 320 bytes */bufferSize = 320
            /*while (isRunning == true) {
                *//* Read & Write to the Device *//*
                recorder?.read(buffer, 0, bufferSize)
                track?.write(buffer, 0, bufferSize)
            }*/
            Log.i(TAG, "Loopback exit")
            return
        }
    }
}