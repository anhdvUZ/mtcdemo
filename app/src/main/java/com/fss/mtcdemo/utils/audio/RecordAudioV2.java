package com.fss.mtcdemo.utils.audio;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.lib.MTC.MTC;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * Created by anhdv31 on 4/28/2022.
 * MIMETYPE_AUDIO_G711_ALAW
 */
public class RecordAudioV2 {
    private String TAG = this.getClass().getSimpleName();
    private static RecordAudioV2 INSTANCE = null;
    private Thread mStreamThread;
    private int mBuffSize = 400;
    private boolean mStatus;
    private Context mContext;

    public static RecordAudioV2 getInstance() {
        if (INSTANCE == null) {
            synchronized (RecordAudioV2.class) {
                INSTANCE = new RecordAudioV2();
            }
        }
        return INSTANCE;
    }

    private int sampleRate = 8000; //44100;
    private int channelConfig = AudioFormat.CHANNEL_IN_MONO;
    private int audioFormat = AudioFormat.ENCODING_PCM_16BIT;
    int minBufSize = AudioRecord.getMinBufferSize(sampleRate, channelConfig, audioFormat);

    private int mLoginId =-1;
    boolean G711Running = false;
    AudioRecord mAudioRecord = null;

    public void startStreamingEncoding(Context context, int loginId) {
        G711Running = true;
        mContext = context;
        mLoginId = loginId;
        mStatus = true;
        mStreamThread = new Thread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);

                mAudioRecord = getAudioRecord();
                //int frameSizeG711 = 160;
                short [] audioData = new short [mBuffSize];
                byte  [] encodeData = new byte[mBuffSize];
                int numRead = 0;
                while(G711Running)
                {
                    numRead = mAudioRecord.read(audioData, 0, mBuffSize);
                    if(numRead<=0) continue;

                    calc2(audioData,0,numRead);
                    G711.linear2ulaw(audioData, 0, encodeData, numRead);
                    MTC.DEV_AudioSend(mLoginId,encodeData,mBuffSize);
                }
            }
        });
        mStreamThread.start();
    }

    void calc2(short[] lin,int off,int len) {
        int i,j;

        for (i = 0; i < len; i++)
        {
            j = lin[i+off];
            lin[i+off] = (short)(j>>1); // ?
        }
    }

    private  AudioRecord getAudioRecord(){
        int min = AudioRecord.getMinBufferSize(sampleRate,
                channelConfig,
                audioFormat);
        Log.e("TAG", "min buffer size:"+min);

        // 创建一个新的AudioRecord类record
        AudioRecord record = null;
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    (Activity) mContext,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    1
            );
        }
        record = new AudioRecord(
                MediaRecorder.AudioSource.MIC,          //音频源，MIC
                sampleRate,                              //采样频率，一般为8000hz/s
                channelConfig, // 声道，单声道
                audioFormat,         // PCM量化位数，16位
                min                                     // 最小缓存长度，min
        );
        //  开始采集原始PCM音频数据
        record.startRecording();

        return record;
    }

    public void stopStreamingEncoding() {
        mStreamThread.interrupt();
        mAudioRecord.stop();
        mAudioRecord = null;
        mStatus = false;
        G711Running = false;
    }
}
