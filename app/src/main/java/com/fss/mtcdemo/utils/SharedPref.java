package com.fss.mtcdemo.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import androidx.annotation.StringDef;

import com.fss.mtcdemo.AppApplication;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashSet;


//==================HOW TO USE THIS CLASS==================//

//********SET VALUE TO SHARED PREFERENCES*********//
//SharedPref.put(context, key, value);

//********GET VALUE TO SHARED PREFERENCES*********//
//SharedPref.get(context, key, default value);

/*
 * key: get from enum
 * default value: it's mean value of key is null --> shared pref return default value
 * */

/**
 * Created by BangDx2 on 16,May,2019
 */
public class SharedPref {

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            AUTH_TOKEN,
            AUTH_PHONE_NUMBER,
            AUTH_OLD_USER,
            AUTH_TRACK_SKIP_IN_PROGRESS_RENEW_LIVE,
            AUTH_TRACK_SKIP_EXPIRED_LIVE,
            AUTH_UPDATE_DEVICE_NOTIFICATION,
            AUTH_KEY_PRE_COOKIE,
            AUTH_KEY_PRE_GRID_MODE,
            AUTH_TOKEN_FIRE_BASE,
            AUTH_TOKEN_USER_UPDATE_NOTIFICATION,
            AUTH_LANGUAGE_KEY,
            AUTH_USER_LOGGED,
            AUTH_CMR_CONFIG,
            SCREEN_SCALE_DPI,
            AUTH_USER_ITERATION,
            AUTH_CMR_CONFIG_GUARD_ZONE,
            AUTH_SME_TOKEN,
            AUTH_LICENSE,
            AUTH_PHONE_NUMBER_FULL,
            AUTH_KEY,
            AUTH_KEY_E2E_KEY,
            AUTH_KEY_E2E_PIN
    })
    // Create an interface for validating String types
    public @interface AuthDef {
    }

    // Declare the constants
    public static final String AUTH_TOKEN = "token user";
    public static final String AUTH_PHONE_NUMBER = "phone number user";
    public static final String AUTH_OLD_USER = "old user";

    public static final String AUTH_TRACK_SKIP_IN_PROGRESS_RENEW_LIVE = "track skip in progress renew live user";
    public static final String AUTH_TRACK_SKIP_EXPIRED_LIVE = "track skip expired live user";
    public static final String AUTH_UPDATE_DEVICE_NOTIFICATION = "update device notification user flag";
    public static final String AUTH_KEY_PRE_COOKIE = "session cookie";
    public static final String AUTH_KEY_PRE_GRID_MODE = "grid mode view";
    public static final String AUTH_TOKEN_FIRE_BASE = "token user fire base";
    public static final String AUTH_TOKEN_USER_UPDATE_NOTIFICATION = "token user update notification";
    public static final String AUTH_LANGUAGE_KEY = "language key";
    public static final String AUTH_USER_LOGGED = "user logged";
    public static final String AUTH_CMR_CONFIG = "serial";
    public static final String SCREEN_SCALE_DPI = "screen scale dpi";
    public static final String AUTH_USER_ITERATION = "iteration";
    public static final String AUTH_CMR_CONFIG_GUARD_ZONE = "serial guard zone";

    public static final String AUTH_SME_TOKEN = "token sme user";
    public static final String AUTH_LICENSE = "license";
    public static final String AUTH_PHONE_NUMBER_FULL = "phone_number";
    public static final String AUTH_KEY = "decrypt key";
    public static final String AUTH_KEY_E2E_KEY = "key end 2 end sent cam";
    public static final String AUTH_KEY_E2E_PIN = "decrypt key end 2 end pin";
    public static final String IS_MARKED_E2E_PIN = "is marked e2e pin";

    private final SharedPreferences sharedPreferences;

    private static SharedPref INSTANCE = null;


    public static SharedPref getInstance() {
        if (INSTANCE == null) {
            synchronized (SharedPref.class) {
                INSTANCE = new SharedPref();
            }
        }
        return INSTANCE;
    }

    public SharedPref() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(AppApplication.getAppContext());
    }

    public boolean isInitialized() {
        return sharedPreferences != null;
    }

    public synchronized HashSet<String> getCookies(@AuthDef String key) {
        return (HashSet<String>) sharedPreferences.getStringSet(key, new HashSet<>());
    }

    public synchronized void saveCookies(@AuthDef String key, HashSet<String> cookies) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(key, cookies).apply();
    }

    public synchronized void put(@AuthDef String key, String value) {
        savePref(key, value);
    }

    public synchronized void put(@AuthDef String key, boolean value) {
        savePref(key, value);
    }

    public synchronized void put(@AuthDef String key, long value) {
        savePref(key, value);
    }

    public synchronized void put(@AuthDef String key, int value) {
        savePref(key, value);
    }

    public synchronized String get(@AuthDef String key, String defValue) {
        return sharedPreferences.getString(key, defValue);
    }

    public synchronized boolean get(@AuthDef String key, boolean defValue) {
        return sharedPreferences.getBoolean(key, defValue);
    }

    public synchronized long get(@AuthDef String key, long defValue) {
        return sharedPreferences.getLong(key, defValue);
    }

    public synchronized int get(@AuthDef String key, int defValue) {
        return sharedPreferences.getInt(key, defValue);
    }


    private synchronized void savePref(String key, int value) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    private synchronized void savePref(String key, String value) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    private synchronized void savePref(String key, boolean value) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    private synchronized void savePref(String key, long value) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    private static final String DEFAULT_TOKEN_FAKE = "";//bangdx2@fpt.com.vn

    public static String getAuthTokenFireBase() {
        return SharedPref.getInstance().get(SharedPref.AUTH_TOKEN_FIRE_BASE, DEFAULT_TOKEN_FAKE);
    }

    private static final String DEFAULT_AUTH_TOKEN_FAKE = "";

    public static String getAuthUserToken() {
        return SharedPref.getInstance().get(SharedPref.AUTH_TOKEN, DEFAULT_AUTH_TOKEN_FAKE);
    }
}
