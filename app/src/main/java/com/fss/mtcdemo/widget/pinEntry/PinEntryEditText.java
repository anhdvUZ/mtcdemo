package com.fss.mtcdemo.widget.pinEntry;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;

import androidx.core.content.ContextCompat;

import com.fss.mtcdemo.R;
import com.fss.mtcdemo.widget.FixedKeyboardEditText;

public class PinEntryEditText extends FixedKeyboardEditText {
    public static final String XML_NAMESPACE_ANDROID = "http://schemas.android.com/apk/res/android";

    private float mSpace = 24; //24 dp by default, space between the lines
    private float mCharSize;
    private float mNumChars = 4;
    private float mLineSpacing = 12; //8dp by default, height of the text from our lines
    private int mMaxLength = 4;


    private float mLineStroke = 1; //1dp by default
    private float mLineStrokeSelected = 2; //2dp by default
    private Paint mLinesPaint;

    private PinEntryEditTextListener mPinEntryEditTextListener;


    public PinEntryEditText(Context context) {
        super(context, null);
    }

    public PinEntryEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public PinEntryEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        float multi = context.getResources().getDisplayMetrics().density;
        mLineStroke = multi * mLineStroke;
        mLineStrokeSelected = multi * mLineStrokeSelected;
        mLinesPaint = new Paint(getPaint());
        mLinesPaint.setStrokeWidth(mLineStroke);
        if (!isInEditMode()) {
            TypedValue outValue = new TypedValue();
            context.getTheme().resolveAttribute(R.attr.colorControlActivated,
                    outValue, true);

            context.getTheme().resolveAttribute(R.attr.colorPrimaryDark,
                    outValue, true);

            context.getTheme().resolveAttribute(R.attr.colorControlHighlight,
                    outValue, true);
        }
        setBackgroundResource(0);
        mSpace = multi * mSpace; //convert to pixels for our density
        mLineSpacing = multi * mLineSpacing; //convert to pixels for our density
        mMaxLength = attrs.getAttributeIntValue(XML_NAMESPACE_ANDROID, "maxLength", 4);
        mNumChars = mMaxLength;

        ComponentListener componentListener = new ComponentListener();
        //Disable copy paste
        super.setCustomSelectionActionModeCallback(componentListener);
        // When tapped, move cursor to end of text.
        this.setOnTouchListener(componentListener);
        this.addTextChangedListener(componentListener);

        registerOnGlobalLayoutListener();

        focusAndShowKeyboard();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //super.onDraw(canvas);
        int availableWidth = getWidth() - getPaddingRight() - getPaddingLeft();
        if (mSpace < 0) {
            mCharSize = (availableWidth / (mNumChars * 2 - 1));
        } else {
            mCharSize = (availableWidth - (mSpace * (mNumChars - 1))) / mNumChars;
        }

        int startX = getPaddingLeft();
        int bottom = getHeight() - getPaddingBottom();

        //Text Width
        Editable text = getText();
        if (text == null) return;
        int textLength = text.length();
        @SuppressLint("DrawAllocation") float[] textWidths = new float[textLength];
        getPaint().getTextWidths(getText(), 0, textLength, textWidths);

        for (int i = 0; i < mNumChars; i++) {
            updateColorForLines(i == textLength, textLength);
            canvas.drawLine(startX, bottom, startX + mCharSize, bottom, mLinesPaint);

            if (getText().length() > i) {
                float middle = startX + mCharSize / 2;
                canvas.drawText(text, i, i + 1, middle - textWidths[0] / 2, bottom - mLineSpacing, getPaint());
            }

            if (mSpace < 0) {
                startX += mCharSize * 2;
            } else {
                startX += mCharSize + mSpace;
            }
        }
    }

    public void setPinEntryEditTextListener(PinEntryEditTextListener mPinEntryEditTextListener) {
        this.mPinEntryEditTextListener = mPinEntryEditTextListener;
    }

    public void release() {
        unRegisterOnGlobalLayoutListener();
        setPinEntryEditTextListener(null);
    }

    /**
     * @param next Is the current char the next character to be input?
     */
    private void updateColorForLines(boolean next, int length) {
        if (isFocused()) {
            mLinesPaint.setStrokeWidth(mLineStrokeSelected);
            mLinesPaint.setColor(ContextCompat.getColor(getContext(), R.color.color_line_normal));
            if (next) {
                mLinesPaint.setColor(ContextCompat.getColor(getContext(), R.color.color_line_next));
            }
        } else {
            mLinesPaint.setStrokeWidth(mLineStroke);
            if (length == mMaxLength) {
                mLinesPaint.setColor(ContextCompat.getColor(getContext(), R.color.nice_blue));
            } else {
                mLinesPaint.setColor(ContextCompat.getColor(getContext(), R.color.color_ipl_light_edt_underline_failure));
            }
        }
    }

    public void registerOnGlobalLayoutListener() {
        this.getViewTreeObserver().addOnGlobalLayoutListener(onGlobalLayoutListener);
    }


    public void unRegisterOnGlobalLayoutListener() {
        this.getViewTreeObserver().removeOnGlobalLayoutListener(onGlobalLayoutListener);
    }

    private boolean wasOpened;
    private final int DefaultKeyboardDP = 100;

    private final ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener =
            new ViewTreeObserver.OnGlobalLayoutListener() {
                // From @nathanielwolf answer...  Lollipop includes button bar in the root. Add height of button bar (48dp) to maxDiff
                private final int EstimatedKeyboardDP = DefaultKeyboardDP + (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? 48 : 0);
                private final Rect r = new Rect();

                @Override
                public void onGlobalLayout() {
                    if (mPinEntryEditTextListener == null)
                        return;
                    // Convert the dp to pixels.
                    int estimatedKeyboardHeight = (int) TypedValue
                            .applyDimension(TypedValue.COMPLEX_UNIT_DIP, EstimatedKeyboardDP, getResources().getDisplayMetrics());
                    // Conclude whether the keyboard is shown or not.
                    getWindowVisibleDisplayFrame(r);
                    int heightDiff = getRootView().getHeight() - (r.bottom - r.top);
                    boolean isShown = heightDiff >= estimatedKeyboardHeight;

                    if (isShown == wasOpened) {
                        return;
                    }
                    wasOpened = isShown;

                    if (wasOpened) {
                        mPinEntryEditTextListener.onPinEntryKeyboardUp(PinEntryEditText.this);
                    } else {
                        mPinEntryEditTextListener.onPinEntryKeyboardDown(PinEntryEditText.this);
                    }
                }
            };


    private class ComponentListener implements
            ActionMode.Callback,
            View.OnTouchListener,
            View.OnFocusChangeListener,
            TextWatcher {

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            return false;
        }

        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouch(View view, MotionEvent event) {
            if (view instanceof EditText) {
                view.setOnFocusChangeListener(this); // User touched edittext
            }
            return false;
        }

        @Override
        public void onFocusChange(View v, boolean focus) {
            if (getText() != null && focus) {
                setSelection(getText().length());
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (mPinEntryEditTextListener != null) {
                mPinEntryEditTextListener.onPinEntryAfterTextChanged(PinEntryEditText.this, s.toString(), s.length());
            }
        }
    }

    public interface PinEntryEditTextListener {

        default void onPinEntryKeyboardUp(View view) {
        }

        default void onPinEntryKeyboardDown(View view) {
        }

        default void onPinEntryAfterTextChanged(View view, String input, int length) {
        }
    }
}
