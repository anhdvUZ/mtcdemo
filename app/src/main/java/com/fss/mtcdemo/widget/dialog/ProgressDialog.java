package com.fss.mtcdemo.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.TextView;

import com.fss.mtcdemo.R;

/**
 * Created by anhdv31 on 11/4/2022.
 */
public class ProgressDialog {
    private Context context;
    private Dialog dialog;

    public ProgressDialog(Context context) {
        this.context = context;
    }

    public void initializeProgressDialog() {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                context.getResources().getDrawable(
                        R.drawable.dialog_progress_background));
        dialog.setContentView(R.layout.dialog_spinner);
        dialog.setCancelable(false);
    }

    public TextView getTextView()
    {
        return (TextView)dialog.findViewById(R.id.textView1);
    }

    public Dialog getDialog()
    {
        return dialog;
    }

    public void dismissProgressDialog() {
        dialog.dismiss();
    }

    public void showProgressDialog() {
        dialog.show();
    }

    public void cancelProgressDialog() {
        dialog.cancel();
    }

    public boolean isShowing() {
        return dialog.isShowing();
    }
}