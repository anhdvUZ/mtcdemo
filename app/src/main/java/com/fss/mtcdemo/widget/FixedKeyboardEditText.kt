package com.fss.mtcdemo.widget

import android.content.Context
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.inputmethod.InputMethodManager
import com.fss.mtcdemo.utils.Constants.DEFAULT_VALUE_STRING
import com.google.android.material.floatingactionbutton.FloatingActionButton
/**
 * Author: bangdinh95@gmail.com
 * Date: 12/3/2021
 * https://developer.squareup.com/blog/showing-the-android-keyboard-reliably/
 */
open class FixedKeyboardEditText(context: Context, attributeSet: AttributeSet?) :
    androidx.appcompat.widget.AppCompatEditText(context, attributeSet) {
    
    private var showKeyboardDelayed = false
    
    fun setShowKeyboard(show: Boolean) {
        showKeyboardDelayed = show
    }
    
    /**
     * Will request focus and try to show the keyboard.
     * It has into account if the containing window has focus or not yet.
     * And delays the call to show keyboard until it's gained.
     */
    fun focusAndShowKeyboard() {
        requestFocus()
        showKeyboardDelayed = true
        maybeShowKeyboard()
    }
    
    private var canPaste = false
    fun disableCanPaste(isNotPaste: Boolean) {
        canPaste = isNotPaste
    }
    
    override fun onTextContextMenuItem(id: Int): Boolean {
        if (canPaste && (id == 16908374 || id == android.R.id.paste)) { //16908374 android.R.id.clipboard
            return false
        }
        return super.onTextContextMenuItem(id)
    }
    
    @Override
    override fun onWindowFocusChanged(hasWindowFocus: Boolean) {
        super.onWindowFocusChanged(hasWindowFocus)
        maybeShowKeyboard()
    }
    
    private fun maybeShowKeyboard() {
        if (hasWindowFocus() && showKeyboardDelayed) {
            if (isFocused) {
                post {
                    val imm =
                        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.showSoftInput(this@FixedKeyboardEditText, InputMethodManager.SHOW_IMPLICIT)
                }
            }
            showKeyboardDelayed = false
        }
    }
    
    private var isKeyboardDown: Boolean = false
    override fun onKeyPreIme(keyCode: Int, event: KeyEvent?): Boolean {
        if (!isKeyboardDown && keyCode == KeyEvent.KEYCODE_BACK) {
            isKeyboardDown = true
            listener?.onKeyBoardDown()
        }else{
            isKeyboardDown = false
            listener?.onKeyBoardUp()
        }
        return super.onKeyPreIme(keyCode, event)
    }
    
    var listener: Listener? = null
    
    
    interface Listener {
        fun onKeyBoardDown();
        fun onKeyBoardUp();
    }
    
    fun removeSpecialCharacter(): String {
        return text?.trim()?.replace("""[\p{P}\p{S}&&[^.]]+""".toRegex(), "") ?: DEFAULT_VALUE_STRING
    }
}