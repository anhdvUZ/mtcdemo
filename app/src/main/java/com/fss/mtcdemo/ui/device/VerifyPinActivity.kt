package com.fss.mtcdemo.ui.device

import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.navArgs
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar.view.*

@AndroidEntryPoint
class VerifyPinActivity : BaseActivity() {
    override fun getLayoutRes() = R.layout.activity_verify_pin
    val args : VerifyPinActivityArgs by navArgs()
    override fun initializeComponents() {
        setSupportActionBar(il_app_bar.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.verify_pin_nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        navController.setGraph(R.navigation.nav_verify_pin,args.toBundle())
        //initializePermission()
    }
    
/*    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }*/
}