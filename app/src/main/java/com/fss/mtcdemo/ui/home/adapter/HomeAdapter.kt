package com.fss.mtcdemo.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.fss.mtcdemo.BR
import com.fss.mtcdemo.base.BaseItemViewModel

/**
 * Created by anhdv31 on 2/21/2022.
 */
class HomeAdapter : RecyclerView.Adapter<HomeViewHolder>() {
    var itemViewModels: List<BaseItemViewModel> = emptyList()
    private val viewTypeToLayoutId: MutableMap<Int, Int> = mutableMapOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val binding: ViewDataBinding =
            DataBindingUtil
                .inflate(
                    LayoutInflater.from(parent.context), viewTypeToLayoutId[viewType] ?: 0,
                    parent,
                    false
                )
        return HomeViewHolder(binding)
    }
    
    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        holder.bind(itemViewModels[position])
    }
    
    override fun getItemViewType(position: Int): Int {
        val item = itemViewModels[position]
        if (!viewTypeToLayoutId.containsKey(item.viewType)) {
            viewTypeToLayoutId[item.viewType] = item.layoutId
        }
        return item.viewType
    }
    
    override fun getItemCount(): Int = itemViewModels.size
    
    fun updateItems(items: List<BaseItemViewModel>?) {
        itemViewModels = items ?: emptyList()
        notifyDataSetChanged()
    }
    
}

class HomeViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(itemViewModel: BaseItemViewModel) {
        binding.setVariable(BR.homeCameraItemViewModel, itemViewModel)
    }
}

@BindingAdapter("homeCameraItemViewModels")
fun bindingItemViews(recyclerView: RecyclerView, itemViewModels:List<BaseItemViewModel>?){
    val adapter = getOrCreateAdapter(recyclerView)
    adapter.updateItems(itemViewModels)
}

private fun getOrCreateAdapter(recyclerView: RecyclerView): HomeAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is HomeAdapter) {
        recyclerView.adapter as HomeAdapter
    } else {
        val bindableRecyclerAdapter = HomeAdapter()
        recyclerView.adapter = bindableRecyclerAdapter
        bindableRecyclerAdapter
    }
}
