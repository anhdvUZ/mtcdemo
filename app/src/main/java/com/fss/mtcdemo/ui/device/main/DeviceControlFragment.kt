package com.fss.mtcdemo.ui.device.main

import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseFragment
import com.fss.mtcdemo.base.ItemClickListener
import com.fss.mtcdemo.databinding.FragmentDeviceControlBinding
import com.fss.mtcdemo.utils.*
import com.fss.mtcdemo.utils.audio.RecordAudioV2
import com.fss.mtcdemo.widget.AppVideoView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DeviceControlFragment : DataBindingBaseFragment<FragmentDeviceControlBinding, BaseViewModel>(BaseViewModel::class.java)
    , ItemClickListener
    , AppVideoView.AppVideoListener, View.OnTouchListener {
    
    override fun getLayoutRes() = R.layout.fragment_device_control
    
    private val mViewModel : DeviceControlViewModel by viewModels()
    
    var loginID = -1;
    //val args: DeviceControlFragmentArgs by navArgs()
    
    override fun initializeComponent() {
        binding.itemClickListener = this
        binding.deviceControlViewModel = mViewModel
        binding.mtcVideoview.setOnInfoListener(this)
        binding.mtcVideoview.initPlayHandleId()
        binding.loadingStreamInfo = true
        binding.streamStatus = false
        binding.isRetry = false

        binding.ibMic.setOnTouchListener(this)
        mViewModel.events.observe(this) {
            handleAction(it.getContentIfNotHandled())
        }
        //loginDevice()
    }
    
    private fun handleAction(uiDeviceControlState: UiDeviceControlState?) {
        when(uiDeviceControlState){
            is UiDeviceControlState.ReadyToStream ->{
                AppLog.d(TAG, ">>UiDeviceControlState.ReadyToStream 0 ${uiDeviceControlState.key}  ${uiDeviceControlState.loginId}")
                binding.mtcVideoview.setRealDevice(uiDeviceControlState.loginId!!,0)
                binding.mtcVideoview.SetDecryptKey(uiDeviceControlState.key)
                binding.mtcVideoview.SetDecryptEnable(1)
                /*if (uiDeviceControlState.key?.trim()?.isNotEmpty() == true){
                    binding.mtcVideoview.SetDecryptKey(uiDeviceControlState.key)
                    binding.mtcVideoview.SetDecryptEnable(1)
                }else{
                    MaterialAlertDialogBuilder(requireActivity())
                        .setTitle("Enter PIN")
                        .setMessage("Enter your PIN code to unlock this stream")
                        .setView(R.layout.dialog_enter_pin)
                        .setPositiveButton(getString(R.string.text_ok)) { dialog, which ->
                            val pin = (dialog as? AlertDialog)?.findViewById<EditText>(R.id.text_pin)?.text?.toString()
                            AppLog.d(TAG, ">>UiDeviceControlState.ReadyToStream 1 $pin")
                            dialog.dismiss()
                        }
                        .setNegativeButton(getString(android.R.string.cancel)) { dialog, _ ->
                            dialog.dismiss()
                        }
                        .show()
                }*/
                //binding.mtcVideoview.SetDecryptKey("02136514581457")
                //02136514581457
                //fGdwZOJ1QOkwV1ISIUGO
            }
            is UiDeviceControlState.FailToStream -> {
                AppLog.d(TAG, ">>UiDeviceControlState.FailToStream")
                setShowDialogNotice(R.string.text_error,AppStatus.getErrorStr(uiDeviceControlState.errorCode))
            }
        }
    }
    
    override fun onItemClickListener(view: View) {
        when (view.id) {
            R.id.ib_device_replay -> {
                val action = DeviceControlFragmentDirections.actionDeviceControlFragmentToMotionActivity(binding.mtcVideoview.handleId)
                Navigation
                    .findNavController(binding.ibDeviceReplay)
                    .navigate(action)
            }
            R.id.ib_retry -> {
                binding.loadingStreamInfo = true
                binding.isRetry = false
                //loginDevice()
            }

            R.id.ib_mic ->{

            }
        }
    }
/*
    override fun onLoginSuccess() {
        binding.mtcVideoview.setRealDevice(loginID, 0)
    }
    
*//*    override fun onDestroyView() {
        binding.mtcVideoview.unSetOnInfoListener(this)
        MTC.DeviceRelease(loginID);
        binding.mtcVideoview.release();
        super.onDestroyView()
    }*//*
    
    
    override fun onLoginError(status: Int) {
        binding.loadingStreamInfo = false
        binding.streamStatus = false
        binding.isRetry = true
        MTC.DeviceRelease(loginID)
        loginID = -1
    }*/
    
    override fun onAppMediaInfo(codecID: Int, width: Int, height: Int) {
        binding.loadingStreamInfo = false
        binding.streamStatus = true
    }
    
    override fun onAppMediaStop() {
    
    }

    override fun onTouch(p0: View?, event: MotionEvent?): Boolean {
        when(p0?.id){
            R.id.ib_mic->{
                if (event?.getAction() == MotionEvent.ACTION_DOWN) {
                    //AudioRecordWordker.getInstance().do_loopback(loginID,true,requireContext())
                    RecordAudioV2.getInstance().startStreamingEncoding(requireContext(),loginID);

                } else if (event?.getAction() == MotionEvent.ACTION_UP) {
                    //AudioRecordWordker.getInstance().do_loopback(loginID,false,requireContext())
                    RecordAudioV2.getInstance().stopStreamingEncoding()
                }
            }
        }
        return false
    }
    
    override fun getTitleRes() = R.string.text_title_device_control_screen
    
    override fun onDestroyView() {
        super.onDestroyView()
        binding.mtcVideoview.release()
    }
    
}