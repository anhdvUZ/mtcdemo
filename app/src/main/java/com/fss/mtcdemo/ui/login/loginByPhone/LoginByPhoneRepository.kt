package com.fss.mtcdemo.ui.login.loginByPhone

import com.fss.mtcdemo.R
import com.fss.mtcdemo.api.ApiService
import com.fss.mtcdemo.api.retrofit.ApiFilterResultState
import com.fss.mtcdemo.api.retrofit.RepositoryDTO
import com.fss.mtcdemo.base.BaseRepository
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseFragment
import com.fss.mtcdemo.base.QueuedMutableLiveData
import com.fss.mtcdemo.databinding.FragmentLoginByPhoneBinding
import com.fss.mtcdemo.utils.AppLog
import com.fss.mtcdemo.utils.AppStatus
import com.fss.mtcdemo.utils.AppStatus.LOGIN_SUCCESS
import com.fss.mtcdemo.utils.SharedPref
import com.google.android.datatransport.cct.internal.LogResponse.fromJson
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.hilt.android.AndroidEntryPoint
import ftel.cmr.tool.model.LoginReq
import ftel.cmr.tool.model.LoginRes
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.adapter.rxjava2.Result

/**
 * Created by anhdv31 on 10/21/2022.
 */
class LoginByPhoneRepository(var apiService: ApiService): BaseRepository() {
    
    var success: QueuedMutableLiveData<Int?> = QueuedMutableLiveData<Int?>()
    var failure: QueuedMutableLiveData<String?> = QueuedMutableLiveData<String?>()
    var mLoginReq : LoginReq? = null
    
    fun signIn(loginReq: LoginReq?): Single<Result<RepositoryDTO<Any>>> {
        return apiService.signInUp(loginReq)
    }
    
    fun requestSignInRx(loginReq: LoginReq?){
        mLoginReq = loginReq
        signIn(loginReq)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe {showProgress()}
            .doOnSuccess { hideProgress() }
            .doOnError { hideProgress() }
            //.doOnTerminate {hideProgress()}
            .observeOn(AndroidSchedulers.mainThread())
            .filter {
                //isShowProgress.value = false
                filterApi(it, apiFilter)
            }
            .subscribe()
    }
    
    private var apiFilter= object : OnFilterCallBack<Any> {
        override fun callback(apiFilterResult: ApiFilterResultState<Any>) {
            when (apiFilterResult) {
                is ApiFilterResultState.OnSuccess -> {
                    AppLog.d(TAG,">> ApiFilterResultState.OnSuccess ${Gson().toJson(apiFilterResult.resultApi.data)}")
                    //AppLog.d(TAG,">> ApiFilterResultState.OnSuccess ${apiFilterResult.resultApi.data?.token}")
                    val jObj = Gson().toJson(apiFilterResult.resultApi.data)
                    val res = Gson().fromJson(jObj, LoginRes::class.java)
                    //val res : LoginRes = apiFilterResult.resultApi.data as LoginRes
                    SharedPref.getInstance().put(SharedPref.AUTH_PHONE_NUMBER,mLoginReq?.phone_number)
                    SharedPref.getInstance().put(SharedPref.AUTH_TOKEN,res.token)
                    SharedPref.getInstance().put(SharedPref.IS_MARKED_E2E_PIN,res.is_pincode)
                    success.postValue(LOGIN_SUCCESS)
                }
                is ApiFilterResultState.OnError -> {
                    AppLog.d(TAG,">> ApiFilterResultState.OnError")
                    failure.postValue(apiFilterResult.error.message)
                }
                is ApiFilterResultState.OnUnKnown ->{
                    failure.postValue(AppStatus.getErrorStr(AppStatus.EE_UNKNOWN))
                }
            }
        }
    }
}