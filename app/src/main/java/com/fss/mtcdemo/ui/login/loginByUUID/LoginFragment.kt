package com.fss.mtcdemo.ui.login.loginByUUID

import android.text.Editable
import android.view.View
import androidx.navigation.Navigation
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseFragment
import com.fss.mtcdemo.base.ItemClickListener
import com.fss.mtcdemo.databinding.FragmentLoginBinding
import com.fss.mtcdemo.utils.LSharedPrefsUtil

class LoginFragment : DataBindingBaseFragment<FragmentLoginBinding,BaseViewModel>(BaseViewModel::class.java)
, ItemClickListener {
    override fun getLayoutRes() = R.layout.fragment_login
    
    override fun initializeComponent() {
        binding.lifecycleOwner = this
        binding.itemClickListener = this
        initDataLogin()
    }
    
    fun initDataLogin(){
        var strUuid = LSharedPrefsUtil.instance.getString("pref_uuid")
        var strUsr = LSharedPrefsUtil.instance.getString("pref_usr")
        var strPwd = LSharedPrefsUtil.instance.getString("pref_pwd")
        if (strUuid.isNotEmpty()){
            binding.tieEmail.text = Editable.Factory.getInstance().newEditable(strUuid)
        }else{
            binding.tieEmail.text = Editable.Factory.getInstance().newEditable("01234567a1b1c1dd");
        }
        if (strUsr.isNotEmpty()){
            binding.tieUsr.text = Editable.Factory.getInstance().newEditable(strUsr)
        }else{
            binding.tieUsr.text =Editable.Factory.getInstance().newEditable("admin");
        }
        if (strPwd.isNotEmpty()){
            binding.tiePwd.text = Editable.Factory.getInstance().newEditable(strPwd)
        }else{
            binding.tiePwd.text =Editable.Factory.getInstance().newEditable("888888");
        }
    }
    
    override fun onItemClickListener(view: View) {
        when(view.id){
            R.id.btn_login ->{
                
                //Navigation.findNavController(binding.btnLogin).navigate(R.id.action_loginFragment_to_homeActivity,null,getNavOptions())
                LSharedPrefsUtil.instance.putString("pref_uuid",binding.tieEmail.text.toString());
                LSharedPrefsUtil.instance.putString("pref_usr",binding.tieUsr.text.toString());
                LSharedPrefsUtil.instance.putString("pref_pwd",binding.tiePwd.text.toString());
                Navigation.findNavController(binding.btnLogin).navigate(R.id.action_loginFragment_to_homeActivity,null)
            }
        }
    }
    
    override fun getTitleRes() =R.string.text_title_device_home_screen
}