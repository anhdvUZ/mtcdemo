package com.fss.mtcdemo.ui.motion.adapter

import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseItemViewModel
import com.fss.mtcdemo.model.VideoDevice
import kotlin.reflect.KFunction1

/**
 * Created by anhdv31 on 3/24/2022.
 */
class MotionVideoItemViewModel(
/*    val id : Int,
    val startTime : Long,
    val endTime : Long,
    val motionImage : String,
    val typeStorage : Int,*/
    val id: Int,
    val player_handleId: Int?,
    val vid_url: String?,
    val start_time: String?,
    val thumb_url: String?,
    val typeStorage: Int,
    val typeItem : Int?,
    val onItemClick: KFunction1<VideoDevice, Unit>,
): BaseItemViewModel{
    
    var videoDevice : VideoDevice = VideoDevice(player_handleId,vid_url,start_time,thumb_url,typeItem)
    
    override val layoutId: Int
        get() {
            return if (viewType == 0){
                R.layout.item_motion_video
            }else{
                R.layout.item_motion_video_date
            }
        }
    override val viewType: Int
        get() {
            return if (typeItem==0){
                0
            }else{
                1
            }
        }
    
    fun onClick(){
        onItemClick(videoDevice);
    }
    
    fun typeByString() : Int {
        if (typeStorage == 1){
            return R.string.text_video_type_motondetect
        }else{
            return R.string.text_video_type_fulltime
        }
    }
}