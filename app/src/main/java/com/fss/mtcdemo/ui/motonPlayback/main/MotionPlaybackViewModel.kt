package com.fss.mtcdemo.ui.motonPlayback.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fss.mtcdemo.model.VideoDevice
import com.fss.mtcdemo.ui.motion.sdcard.SDCardRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * Created by anhdv31 on 4/7/2022.
 */
@HiltViewModel
class MotionPlaybackViewModel @Inject constructor(
    private val motionPlaybackViewModel: MotionPlaybackRepository?
): ViewModel(){
    val videoDevice : LiveData<VideoDevice> get() = _videoDevice
    val _videoDevice  = MutableLiveData<VideoDevice>()
    
    fun videoDevice(obj: VideoDevice?){
        _videoDevice.value = obj!!
    }
}