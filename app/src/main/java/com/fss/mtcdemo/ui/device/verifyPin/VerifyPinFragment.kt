package com.fss.mtcdemo.ui.device.verifyPin

import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseFragment
import com.fss.mtcdemo.base.ItemClickListener
import com.fss.mtcdemo.databinding.FragmentVerifyPinBinding
import com.fss.mtcdemo.model.FssDevice
import com.fss.mtcdemo.secure.shamirSecret.E2E.PIN_LENGTH
import com.fss.mtcdemo.ui.device.DeviceControlActivity
import com.fss.mtcdemo.ui.home.main.HomeFragmentDirections
import com.fss.mtcdemo.utils.AppLog
import com.fss.mtcdemo.utils.AppStatus
import com.fss.mtcdemo.utils.AppStatus.EE_INVALID_PIN
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by anhdv31 on 11/1/2022.
 */
@AndroidEntryPoint
class VerifyPinFragment : DataBindingBaseFragment<FragmentVerifyPinBinding, BaseViewModel>(BaseViewModel::class.java),
    ItemClickListener {
    
    override fun getLayoutRes() = R.layout.fragment_verify_pin
    
    private val mViewModel: VerifyPinviewModel by viewModels()
    
    override fun initializeComponent() {
        binding.lifecycleOwner = this
        binding.itemClickListener = this
        mViewModel.events.observe(this) {
            handleAction(it.getContentIfNotHandled())
        }
    }
    
    override fun getTitleRes() = R.string.text_verify_pin
    
    private fun handleAction(state: VerifyPinUIState?) {
        when (state) {
            is VerifyPinUIState.ShowProgress -> {
                setShowProgress(state.isShow!!)
            }
            is VerifyPinUIState.Error -> {
                setShowDialogNotice(R.string.text_error, AppStatus.getErrorStr(state.errCode))
            }
            is VerifyPinUIState.Success -> {
                AppLog.d(TAG,">>VerifyPinUIState.Success ${Gson().toJson(state.fssDevice)}")
                val action = VerifyPinFragmentDirections.actionVerifyPinFragmentToDeviceControlActivity(state.fssDevice)
                Navigation.findNavController(binding.btnAccept).navigate(action)
                requireActivity().finish()
            }
            is VerifyPinUIState.SkipPin -> {
                val action = VerifyPinFragmentDirections.actionVerifyPinFragmentToDeviceControlActivity(state.fssDevice)
                Navigation.findNavController(binding.btnAccept).navigate(action)
            }
            else -> {}
        }
    }
    
    override fun onItemClickListener(view: View) {
        when(view.id){
            R.id.btn_accept -> {
                val pin = binding.pinOtpEdt.text?.toString()?.trim()
                if (pin!!.length == PIN_LENGTH){
                    mViewModel.verifyPin(binding.pinOtpEdt.text?.toString()?.trim())
                }else{
                    setShowDialogNotice(R.string.text_error, AppStatus.getErrorStr(EE_INVALID_PIN))
                }
            }
            R.id.btn_skip -> {
                mViewModel.skipPin()
            }
        }
    }
}