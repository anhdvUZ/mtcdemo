package com.fss.mtcdemo.ui.home.main

import androidx.lifecycle.MutableLiveData
import com.fss.mtcdemo.BuildConfig
import com.fss.mtcdemo.api.ApiService
import com.fss.mtcdemo.api.retrofit.ApiFilterResultState
import com.fss.mtcdemo.api.retrofit.RepositoryDTO
import com.fss.mtcdemo.base.BaseRepository
import com.fss.mtcdemo.base.QueuedMutableLiveData
import com.fss.mtcdemo.model.Camera
import com.fss.mtcdemo.model.FssDevice
import com.fss.mtcdemo.secure.shamirSecret.ShamirSecretSharing
import com.fss.mtcdemo.utils.AppLog
import com.fss.mtcdemo.utils.AppStatus
import com.fss.mtcdemo.utils.Constants.PORT_01
import com.fss.mtcdemo.utils.Constants.PORT_02
import com.fss.mtcdemo.utils.Constants.PORT_03
import com.fss.mtcdemo.utils.Constants.PORT_04
import com.fss.mtcdemo.utils.Constants.PORT_05
import com.fss.mtcdemo.utils.DateTimeUtils.ONE_SECOND
import com.fss.mtcdemo.utils.SharedPref
import com.fss.mtcdemo.utils.StringUtils
import com.google.android.gms.common.util.Base64Utils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lib.MTC.MTC
import com.lib.MTC.OnMTCLoginListener
import com.lib.MTC.basic.Futils
import com.lib.MTC.struct.NetCommon
import ftel.cmr.tool.model.LoginRes
import ftel.cmr.tool.model.ShamirKeyShareReq
import ftel.cmr.tool.model.ShamirKeyShareRes
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.adapter.rxjava2.Result
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by anhdv31 on 3/23/2022.
 */
class HomeRepository(var apiService: ApiService) : BaseRepository(),OnMTCLoginListener {
    
    val addSuccess: MutableLiveData<Int?> = MutableLiveData<Int?>()
    val putErr: MutableLiveData<String?> = MutableLiveData<String?>()
    
    var success: QueuedMutableLiveData<List<String>?> = QueuedMutableLiveData<List<String>?>()
    var failure: QueuedMutableLiveData<String?> = QueuedMutableLiveData<String?>()
    
    val COUNT = 5
    val THRESHOLD = 3
    val GENERATE_KEY_LENGTH = 20
    var mapHost = HashMap<Int?,String?>()
    var base64KeyShares : ArrayList<String> =  ArrayList<String>()
    var mMainKey = "" //serial_domain_sdt (encrypt by pin)
    var mPin :String?= ""
    val gson = Gson()
    lateinit var mFssDevice: FssDevice
    
    init {
        initializeHost()
        mFssDevice = FssDevice("","","","",23456)
    }
    
    private fun initializeHost(){
        mapHost[PORT_01] = BuildConfig.URL_SM + ":$PORT_01/keys/put/"
        mapHost[PORT_02] = BuildConfig.URL_SM + ":$PORT_02/keys/put/"
        mapHost[PORT_03] = BuildConfig.URL_SM + ":$PORT_03/keys/put/"
        mapHost[PORT_04] = BuildConfig.URL_SM + ":$PORT_04/keys/put/"
        mapHost[PORT_05] = BuildConfig.URL_SM + ":$PORT_05/keys/put/"
    }
    
    fun getHomeListData():List<FssDevice>{
        val typeToken = object: TypeToken<List<FssDevice>>(){}.type
        return gson.fromJson(getRawJsonHomeCamerasData(),typeToken)
    }
    
    private fun getCamsService(): Single<Result<RepositoryDTO<List<String>>>> {
        return apiService.getCams()
    }
    
    fun getCams(){
        getCamsService()
            .subscribeOn(Schedulers.io())
            .doOnSubscribe {showProgress()}
            .doOnSuccess { hideProgress() }
            .doOnError { hideProgress() }
            //.doOnTerminate {hideProgress()}
            .observeOn(AndroidSchedulers.mainThread())
            .filter {
                //isShowProgress.value = false
                filterApi(it, apiFilter)
            }
            .subscribe()
    }
    
    private var apiFilter= object : OnFilterCallBack<List<String>> {
        override fun callback(apiFilterResult: ApiFilterResultState<List<String>>) {
            when (apiFilterResult) {
                is ApiFilterResultState.OnSuccess -> {
                    AppLog.d(TAG,">> ApiFilterResultState.OnSuccess")
                    success.postValue(apiFilterResult.resultApi.data)
                }
                is ApiFilterResultState.OnError -> {
                    AppLog.d(TAG,">> ApiFilterResultState.OnError")
                    failure.postValue(apiFilterResult.error.message)
                }
                is ApiFilterResultState.OnUnKnown ->{
                    AppLog.d(TAG,">> ApiFilterResultState.else ${apiFilterResult.status.code}")
                    failure.postValue(AppStatus.getErrorStr(AppStatus.EE_UNKNOWN))
                }
            }
        }
    }
    
    fun requestConnect(info: String?,mainKey:String, pin: String?) {
        showProgress()
        val stringParts = info?.split("-")
        println("String split ${stringParts?.size}")
        if (stringParts?.size == 3) {
            mFssDevice.serial = stringParts?.get(0).toString()
            mFssDevice.usr = stringParts?.get(1).toString()
            mFssDevice.pwd = stringParts?.get(2).toString()
            mPin = pin
            mMainKey = mainKey
            loginCloud(mFssDevice.serial, mFssDevice.usr, mFssDevice.pwd)
        } else {
            failure.postValue(AppStatus.getErrorStr(AppStatus.EE_INVALID_QRCODE))
        }
        
    }
    
    private fun loginCloud(uuid: String?, usr: String?, pwd: String?): Int {
        val res = MTC.DEV_LoginCloud(uuid, usr, pwd)
        MTC.loginID = res
        MTC.registerOnDeviceListener(res, this)
        return res
    }
    
    private fun getRawJsonHomeCamerasData() : String {
        //return "[{id:0,title:\"Office 01\",description:\"\",status:1,icon:0},{id:1,title:\"Office 02\",description:\"\",status:0,icon:0},{id:2,title:\"Office 02\",description:\"\",status:1,icon:0},{id:3,title:\"Office 03\",description:\"\",status:0,icon:0},{id:4,title:\"Office 04\",description:\"\",status:1,icon:0},{id:5,title:\"Office 05\",description:\"\",status:1,icon:0}]".trimIndent()
        return "[{serial:\"01234567a1b1c1dd\",usr:\"admin\",pwd:\"888888\"}]".trimIndent()
    }
    
    fun createSearchKey(serial : String?,host:String) : String{
        return serial + "_" + host + "_" + SharedPref.getInstance().get(SharedPref.AUTH_PHONE_NUMBER,"")
    }
    
    override fun onLoginSuccess() {
        val netCommon = NetCommon()
        val array = Futils.ObjToBytes(netCommon)
        val param = ByteArray(array.size)
        Completable
            .timer(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                var ret = MTC.DEV_GetNetCommon(MTC.loginID, param)
                AppLog.d(TAG, ">> DEV_GetNetCommon ${MTC.loginID} $ret")
                if (ret == 0) {
                    Futils.BytesToObj(netCommon, param)
                    mFssDevice.ip = netCommon.st_1_hostIP.ToString()
                    mFssDevice.port = 2131
                    AppLog.d(TAG, ">> GetNetCommon IP  ${mFssDevice.ip} ${mFssDevice.port} ${mMainKey}")
                    //syncKey()
                    CoroutineScope(Dispatchers.IO).launch {
                        val retSyncKey = MTC.DEV_SyncKey(mFssDevice.ip, mFssDevice.port!!, mMainKey)
                        addCamnMarkPinRx()
                    }
                }
            }, {
                // Error
            })
    }
    private fun addCam(serial: String?): Single<Result<RepositoryDTO<Any>>> {
        return apiService.addCam(serial)
    }
    
    private fun pinMarked(): Single<Result<RepositoryDTO<Any>>> {
        return apiService.pincodeMarked()
    }
    private fun addCamnMarkPinRx() {
        Single.zip(
            addCam(mFssDevice.serial).subscribeOn(Schedulers.io()),
            pinMarked().subscribeOn(Schedulers.io())
        ) { r1, r2 ->
            var statusR1 = r1.response()?.body()?.getCodeStatus()
            var statusR2 = r2.response()?.body()?.getCodeStatus()
            
            return@zip statusR1 == 1200 && statusR2 == 1200
        }
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress() }
            .doFinally { hideProgress() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { value ->
                    if (value) {
                        addSuccess.postValue(AppStatus.SUCCESS)
                    } else {
                        failure.postValue("EE_CANNOT_ADDCAM_MARKEDPIN")
                    }
                    
                },
                { error ->
                    failure.postValue(error.message)
                }
            )
    }
    
    override fun onLoginError(status: Int) {
        TODO("Not yet implemented")
    }
}