package com.fss.mtcdemo.ui.motonPlayback

import android.util.Log
import androidx.activity.viewModels
import androidx.navigation.navArgs
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseActivity
import com.fss.mtcdemo.model.VideoDevice
import com.fss.mtcdemo.ui.motonPlayback.main.MotionPlaybackViewModel
import com.fss.mtcdemo.utils.AppLog
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_motion.*
import kotlinx.android.synthetic.main.app_bar.view.*

/**
 * Created by anhdv31 on 4/5/2022.
 */
@AndroidEntryPoint
class MotionPlaybackActivity : BaseActivity() {
    
    override fun getLayoutRes() = R.layout.activity_motion_playback
    
    val motionPlaybackViewModel : MotionPlaybackViewModel by viewModels()
    //private val args : MotionPlaybackActivityArgs by navArgs()
    
    override fun initializeComponents() {
        setSupportActionBar(il_app_bar.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        var videoDevice = intent.getParcelableExtra<VideoDevice>(VideoDevice::class.java.simpleName)
        motionPlaybackViewModel.videoDevice(videoDevice)
        //AppLog.d(TAG,"initializeComponents args " + args.videoDevice.vid_url)
        /*val videoDevice : VideoDevice? = ar.getParcelable(VideoDevice::class.java.simpleName)
        AppLog.d(TAG,"initializeController args "  + Gson().toJson(videoDevice))*/
        AppLog.d(TAG,"initializeComponents " + videoDevice?.start_time)
        
        
    }
}