package com.fss.mtcdemo.ui.login

import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseActivity
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseActivity
import com.fss.mtcdemo.databinding.ActivityLoginBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar.view.*

@AndroidEntryPoint
class LoginActivity : BaseActivity() {
    override fun getLayoutRes(): Int {
        return R.layout.activity_login
    }
    
    override fun initializeComponents() {
        setSupportActionBar(il_app_bar.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        initializePermission()
    }

}