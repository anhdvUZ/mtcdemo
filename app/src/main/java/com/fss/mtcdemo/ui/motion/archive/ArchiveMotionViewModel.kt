package com.fss.mtcdemo.ui.motion.archive

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fss.mtcdemo.base.BaseItemViewModel
import com.fss.mtcdemo.base.Event
import com.fss.mtcdemo.model.VideoDevice
import com.fss.mtcdemo.ui.motion.adapter.MotionVideoItemViewModel
import com.fss.mtcdemo.utils.AppLog
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.random.Random

/**
 * Created by anhdv31 on 4/5/2022.
 */
@HiltViewModel
class ArchiveMotionViewModel @Inject constructor(
    private val archiveMotionRepository: ArchiveMotionRepository
) : ViewModel() {
    
    val data: LiveData<List<BaseItemViewModel>>
        get() = _data
    private val _data = MutableLiveData<List<BaseItemViewModel>>(emptyList())
    
    val events: LiveData<Event<ArchiveMotionItemEvent>>
        get() = _events
    private val _events = MutableLiveData<Event<ArchiveMotionItemEvent>>()
    
    init {
        loadData()
    }
    
    private fun loadData() {
        viewModelScope.launch(Dispatchers.IO) {
            val viewData = createViewModel(archiveMotionRepository.getArchiveVideosData())
            _data.postValue(viewData)
        }
    }
    
    private fun createViewModel(listVideoDevice: List<VideoDevice>): List<BaseItemViewModel> {
        val viewData = mutableListOf<BaseItemViewModel>()
        listVideoDevice.forEach {
            //viewData.add(MotionVideoItemViewModel(it.id, it.startTime, it.endTime, it.motionImage, it.typeStorage, ::onCloudMotionClicked))
            viewData.add(MotionVideoItemViewModel(Random.nextInt(0, 10000),it.player_handleId,it.vid_url, it.start_time, it.thumb_url,1,it.type,::onArchiveMotionClicked))
        }
        return viewData
    }
    
    private fun onArchiveMotionClicked(videoDevice: VideoDevice) {
        _events.postValue(Event(ArchiveMotionItemEvent.OnMotionClick(videoDevice)))
    }
}

sealed class ArchiveMotionItemEvent {
    data class OnMotionClick(val videoDevice: VideoDevice) : ArchiveMotionItemEvent()
}