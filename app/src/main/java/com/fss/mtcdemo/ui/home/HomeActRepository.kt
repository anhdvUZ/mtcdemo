package com.fss.mtcdemo.ui.home

import com.fss.mtcdemo.api.ApiService
import com.fss.mtcdemo.api.retrofit.ApiFilterResultState
import com.fss.mtcdemo.api.retrofit.RepositoryDTO
import com.fss.mtcdemo.base.BaseRepository
import com.fss.mtcdemo.base.QueuedMutableLiveData
import com.fss.mtcdemo.utils.AppLog
import com.fss.mtcdemo.utils.AppStatus
import com.fss.mtcdemo.utils.SharedPref
import ftel.cmr.tool.model.LoginReq
import ftel.cmr.tool.model.LoginRes
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.adapter.rxjava2.Result

/**
 * Created by anhdv31 on 11/1/2022.
 */
class HomeActRepository(var apiService: ApiService)  : BaseRepository() {
    
    var success: QueuedMutableLiveData<Int?> = QueuedMutableLiveData<Int?>()
    var failure: QueuedMutableLiveData<String?> = QueuedMutableLiveData<String?>()
    
    private fun signOutService(): Single<Result<RepositoryDTO<Any>>> {
        return apiService.signOut()
    }
    
    fun signOut(){
        signOutService()
            .subscribeOn(Schedulers.io())
            .doOnSubscribe {showProgress()}
            .doOnSuccess { hideProgress() }
            .doOnError { hideProgress() }
            //.doOnTerminate {hideProgress()}
            .observeOn(AndroidSchedulers.mainThread())
            .filter {
                //isShowProgress.value = false
                filterApi(it, apiFilter)
            }
            .subscribe()
    }
    
    private var apiFilter= object : OnFilterCallBack<Any> {
        override fun callback(apiFilterResult: ApiFilterResultState<Any>) {
            when (apiFilterResult) {
                is ApiFilterResultState.OnSuccess -> {
                    AppLog.d(TAG,">> ApiFilterResultState.OnSuccess")
                    SharedPref.getInstance().put(SharedPref.AUTH_PHONE_NUMBER,"")
                    SharedPref.getInstance().put(SharedPref.AUTH_TOKEN,"")
                    success.postValue(AppStatus.LOGIN_SUCCESS)
                }
                is ApiFilterResultState.OnError -> {
                    AppLog.d(TAG,">> ApiFilterResultState.OnError")
                    failure.postValue(apiFilterResult.error.message)
                }
                is ApiFilterResultState.OnUnKnown ->{
                    failure.postValue(AppStatus.getErrorStr(AppStatus.EE_UNKNOWN))
                }
            }
        }
    }
}