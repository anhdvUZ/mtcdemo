package com.fss.mtcdemo.ui.home.adapter

import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseItemViewModel
import com.fss.mtcdemo.model.FssDevice

/**
 * Created by anhdv31 on 3/23/2022.
 */
class HomeCameraItemViewModel(
    val title : String,
    val description : String,
    val icon : Int,
    val status : Int,
    //val fssDevice: FssDevice,
    val serial: String,
    val onItemClick : (String) -> Unit
) : BaseItemViewModel
{
    override val layoutId: Int = R.layout.item_home_camera
    
    override val viewType: Int
        get() = super.viewType
    
    fun onClick(){
        onItemClick(serial)
    }
    
    fun statusByString() : Int {
        if (status == 1){
            return R.string.text_device_status_on
        }else{
            return R.string.text_device_status_off
        }
    }
    
    fun backgroundByStatus(): Int{
        if (status == 1){
            return R.drawable.bg_text_device_status_on
        }else{
            return R.drawable.bg_text_device_status_off
        }
    }
}