package com.fss.mtcdemo.ui.motion.main

import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseFragment
import com.fss.mtcdemo.databinding.FragmentMotionBinding
import com.fss.mtcdemo.ui.motion.adapter.MotionTabAdapter
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_motion.*


class MotionFragment : DataBindingBaseFragment<FragmentMotionBinding, BaseViewModel>(BaseViewModel::class.java) {
    override fun getLayoutRes() = R.layout.fragment_motion
    
    override fun initializeComponent() {
        vp_motion.adapter = MotionTabAdapter(requireActivity().supportFragmentManager, lifecycle)
        vp_motion.offscreenPageLimit = 1
        TabLayoutMediator(tl_motion, vp_motion) { tab, position ->
            when (position) {
                0 -> {
                    tab.text = getString(R.string.text_title_tab_sdcard)
                }
                1 -> {
                    tab.text = getString(R.string.text_title_tab_cloud)
                }
                2 -> {
                    tab.text = getString(R.string.text_title_tab_archive)
                }
            }
        }.attach()
    }
    
    override fun getTitleRes() = R.string.text_title_device_event_screen
    
}