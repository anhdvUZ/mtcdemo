package com.fss.mtcdemo.ui.login.loginByPhone

import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseFragment
import com.fss.mtcdemo.base.ItemClickListener
import com.fss.mtcdemo.databinding.FragmentLoginByPhoneBinding
import com.fss.mtcdemo.ui.device.verifyPin.VerifyPinUIState
import com.fss.mtcdemo.utils.AppLog
import com.fss.mtcdemo.utils.AppStatus
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by anhdv31 on 10/21/2022.
 */
@AndroidEntryPoint
class LoginByPhoneFragment : DataBindingBaseFragment<FragmentLoginByPhoneBinding, BaseViewModel>(BaseViewModel::class.java)
, ItemClickListener {
    private val mViewModel: LoginByPhoneViewModel by viewModels()
    
    override fun getLayoutRes() = R.layout.fragment_login_by_phone
    
    override fun initializeComponent() {
        binding.lifecycleOwner = this
        binding.itemClickListener = this
        mViewModel.events.observe(this) {
            handleAction(it.getContentIfNotHandled())
        }
    }
    
    override fun getTitleRes() = R.string.text_login
    override fun onItemClickListener(view: View) {
        when(view.id){
            R.id.btn_login -> {
                //Navigation.findNavController(binding.btnLogin).navigate(R.id.action_loginByPhoneFragment_to_pinFragment,null,getNavOptions())
                //Navigation.findNavController(binding.btnLogin).navigate(R.id.action_loginByPhoneFragment_to_HomeActivity,null)
                mViewModel.requestLogin(binding.tieUsr.text.toString(),binding.tiePwd.text.toString())
            }
        }
    }
    private fun handleAction(state: UiSignInState?) {
        when (state) {
            is UiSignInState.ErrorInputPassword -> {
                setShowDialogNotice(R.string.text_error,getString(R.string.text_input_err_password))
            }
            is UiSignInState.ErrorInputPhoneNum -> {
                setShowDialogNotice(R.string.text_error,getString(R.string.text_input_err_phonenum))
            }
            is UiSignInState.ShowProgress -> {
                setShowProgress(state.isShow!!)
            }
            is UiSignInState.Success -> {
                Navigation.findNavController(binding.btnLogin).navigate(R.id.action_loginByPhoneFragment_to_HomeActivity,null)
                requireActivity().finish()
                AppLog.d(TAG,">> UiSignInState.Success")
            }
            is UiSignInState.Error -> {
                AppLog.d(TAG,">> UiSignInState.Error")
                setShowDialogNotice(R.string.text_error, state.status)
            }
        }
    }
}