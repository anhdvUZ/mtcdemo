package com.fss.mtcdemo.ui.home.main

import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseFragment
import com.fss.mtcdemo.base.ItemClickListener
import com.fss.mtcdemo.databinding.FragmentHomeBinding
import com.fss.mtcdemo.model.FssDevice
import com.fss.mtcdemo.ui.addDevice.main.AddDeviceFragment
import com.fss.mtcdemo.ui.login.loginByPhone.UiSignInState
import com.fss.mtcdemo.utils.AppLog
import com.fss.mtcdemo.utils.SharedPref
import com.google.gson.Gson
import com.google.mlkit.vision.barcode.common.Barcode
import com.google.mlkit.vision.codescanner.GmsBarcodeScannerOptions
import com.google.mlkit.vision.codescanner.GmsBarcodeScanning
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.view_add_device.view.*

@AndroidEntryPoint
class HomeFragment : DataBindingBaseFragment<FragmentHomeBinding, BaseViewModel>(BaseViewModel::class.java)
    , ItemClickListener {
    
    private val mViewModel : HomeViewModel by viewModels()
    
    override fun getLayoutRes() = R.layout.fragment_home
    
    override fun initializeComponent() {
        binding.itemClickListener = this
        binding.homeViewModel = mViewModel
        mViewModel.events.observe(this){
            handleAction(it.getContentIfNotHandled())
        }
        initializeComponentListerner()
        
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner){
            if (v_add_device.visibility == View.VISIBLE){
                v_add_device.visibility = View.GONE
                val rotate_anim = AnimationUtils.loadAnimation(
                    requireContext(),
                    R.anim.rotate_0
                )
                btn_add_device.startAnimation(rotate_anim)
            }else{
            
            }
        }
    }
    
    private fun initializeComponentListerner(){
        binding.vAddDevice.ibDeviceCam.setOnClickListener {
            AppLog.d(TAG,"initializeListerner ib_device_record")
            val mainKey = SharedPref.getInstance().get(SharedPref.AUTH_KEY_E2E_KEY,"")
            val pin = SharedPref.getInstance().get(SharedPref.AUTH_KEY_E2E_PIN,"")
            if (mainKey.isEmpty()){
                Navigation.findNavController(binding.vAddDevice.ibDeviceRecord).navigate(R.id.action_homeFragment_to_addDeviceActivity,null)
            }else{
                val optionsBuilder = GmsBarcodeScannerOptions.Builder()
                val gmsBarcodeScanner = GmsBarcodeScanning.getClient(requireActivity(), optionsBuilder.build())
                gmsBarcodeScanner
                    .startScan()
                    .addOnSuccessListener { barcode: Barcode ->
                        AppLog.d(TAG,"initializeListerner addOnSuccessListener ${barcode.rawValue}")
                        mViewModel.requestAddCam(barcode.rawValue,mainKey,pin)
                    }
                    .addOnFailureListener { e: Exception ->
                        AppLog.d(TAG,"initializeListerner addOnFailureListener ${e.toString()}}")
                    }
                    .addOnCanceledListener {
                        AppLog.d(TAG,"initializeListerner addOnCanceledListener")
                    }
            }
        }
    }
    
    override fun onItemClickListener(view: View) {
        when(view.id){
            R.id.btn_add_device -> {
                /*Navigation
                    .findNavController(binding.btnAddDevice)
                    .navigate(R.id.action_homeFragment_to_addDeviceFragment,null,getNavOptions())*/
                
                //.navigate(R.id.action_homeFragment_to_addDeviceFragment,null, NavOptions.Builder().setPopUpTo(R.id.homeFragment,true).build())
                //AddDeviceFragment().show(childFragmentManager,"AddDeviceFragment")
                
                var rotate_anim : Animation? = null
                var transition_anim : Animation? = null
                
                if (v_add_device.visibility == View.VISIBLE){
                    v_add_device.visibility = View.GONE
                    rotate_anim = AnimationUtils.loadAnimation(
                        requireContext(),
                        R.anim.rotate_0
                    )
                    transition_anim = AnimationUtils.loadAnimation(requireContext(),R.anim.slide_out_left)
                }else{
                    v_add_device.visibility = View.VISIBLE
                    rotate_anim = AnimationUtils.loadAnimation(
                        requireContext(),
                        R.anim.rotate_90
                    )
                    transition_anim = AnimationUtils.loadAnimation(requireContext(),R.anim.slide_in_left)
                }
    
                v_add_device.startAnimation(transition_anim)
                btn_add_device.startAnimation(rotate_anim)
            }
        }
    }
    
    private fun handleAction(homeItemEvent: HomeItemEvent?) {
        when(homeItemEvent){
            is HomeItemEvent.SwitchToDeviceControl -> {
                val res = homeItemEvent.serial
                //AppLog.d(TAG,"HomeItemEvent " + Gson().toJson(homeItemEvent.fssDevice))
                //Navigation.findNavController(binding.rvHome).navigate(R.id.action_homeFragment_to_deviceControlActivity,null,getNavOptions())
                //Navigation.findNavController(binding.rvHome).navigate(R.id.action_homeFragment_to_deviceControlActivity,null)
                if (SharedPref.getInstance().get(SharedPref.AUTH_KEY_E2E_KEY, "").isEmpty()) {
                    AppLog.d(TAG,">> setStartDestination verifyPinFragment")
                    val action = HomeFragmentDirections.actionHomeFragmentToVerifyPinActivity(FssDevice(homeItemEvent.serial,"admin","888888","",23456))
                    Navigation.findNavController(binding.rvHome).navigate(action)
                }else{
                    AppLog.d(TAG,">> setStartDestination deviceControlFragment")
                    val action = HomeFragmentDirections.actionHomeFragmentToDeviceControlActivity(FssDevice(homeItemEvent.serial,"admin","888888","",23456))
                    Navigation.findNavController(binding.rvHome).navigate(action)
                }
         
            }
            is HomeItemEvent.ShowProgress -> {
                setShowProgress(homeItemEvent.isShow!!)
            }
            
            is HomeItemEvent.onAddSuccess -> {
                AppLog.d(TAG,">> setStartDestination onAddSuccess")
                mViewModel.refreshCams()
            }
            is HomeItemEvent.onPutKeyError -> {
        
            }
        }
    }
    
    override fun getTitleRes() = R.string.text_title_device_home_screen
}