package com.fss.mtcdemo.ui.motion.cloud

import com.fss.mtcdemo.model.VideoDevice
import com.fss.mtcdemo.utils.AppLog
import com.fss.mtcdemo.utils.date.DateFormat.*
import com.fss.mtcdemo.utils.date.DateUtils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lib.MTC.MTC
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by anhdv31 on 4/5/2022.
 */
class CloudMotionRepository {
    
    var motionStrJson = arrayOfNulls<String>(1)
    
    val gson = Gson()
    
    fun getCloudMotionsData(): List<VideoDevice> {
        val typeToken = object : TypeToken<List<VideoDevice>>() {}.type
        //cloudSearchbyTime(1, "8792934df4756401", "2022-04-06 07:00:00", "2022-04-06 18:00:00", motionStrJson)
        return gson.fromJson(getRawJsonMotionsData(), typeToken)
    }
    
    private fun getRawJsonMotionsData(): String {
        return "[{id:0,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:0},{id:1,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:1},{id:2,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:0},{id:3,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:0},{id:4,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:0},{id:5,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:1},{id:6,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:0},{id:7,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:1},{id:8,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:0},{id:9,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:0},{id:10,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:0}]".trimIndent()
    }
    
    fun cloudSearchbyTime(
        serial: String,
        type: Int
    ): List<VideoDevice> {
        AppLog.d("CloudMotionRepository","cloudSearchbyTime")
        var startTime = "2022-04-12 06:00:00"
        var stopTime = "2022-04-12 18:00:00"
        
        if (type == 1) {
            startTime = DateUtils.getCurrentDateToString(DATE_FORMAT_3) + " " + DATE_START_TIME
            stopTime = DateUtils.getCurrentDateToString(DATE_FORMAT_3) + " " + DATE_HALF_TIME
        }else if (type == 2){
            startTime = DateUtils.getCurrentDateToString(DATE_FORMAT_3) + " " + DATE_START_TIME
            stopTime = DateUtils.getCurrentDateToString(DATE_FORMAT_3) + " " + DATE_END_TIME
        }else {
        
        }
        
        val typeToken = object : TypeToken<List<VideoDevice>>() {}.type
        var id = MTC.Play_OpenStream()
        val ret = MTC.Play_CloudSearchByTime(id, serial, startTime, stopTime, motionStrJson)
        AppLog.d("ANHDV","Play_CloudSearchByTime " + motionStrJson.get(0));
        if (ret == 0) {
            try {
                val doc = JSONObject(motionStrJson.get(0))
                val data_obj = doc.get("data") as JSONObject
                val VideoList_obj = data_obj["obj_list"] as JSONArray
                val mString = VideoList_obj.toString()
                var motionList : List<VideoDevice> = gson.fromJson(mString, typeToken)
                var videoDeviceDate = VideoDevice (-1,
                    "",
                    DateUtils.convertFormatToFormat(motionList.get(0).start_time,SERVER_FORMAT_DATE_TIME,TYPE_CONVERT),
                    "",
                    1)
                for (motion in motionList){
                    motion.player_handleId = id
                    motion.type = 0
                }
                var motionArrayList = ArrayList<VideoDevice>()
                motionArrayList.add(videoDeviceDate)
                motionArrayList.addAll(motionList)
                return motionArrayList
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        } else {
            
            //ret -5001
        }
        return emptyList()
    }
}