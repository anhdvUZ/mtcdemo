package com.fss.mtcdemo.ui.motion

import android.view.Menu
import android.view.MenuItem
import androidx.navigation.navArgs
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseActivity
import com.fss.mtcdemo.ui.bottomSheet.FragmentAppBottomSheet
import com.fss.mtcdemo.utils.AppLog
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_motion.*
import kotlinx.android.synthetic.main.app_bar.view.*

@AndroidEntryPoint
class MotionActivity : BaseActivity() {
    val args : MotionActivityArgs  by navArgs()
    override fun getLayoutRes() = R.layout.activity_motion
    override fun initializeComponents() {
        AppLog.d(TAG,"initializeComponents args " + args.playerHandleId)
        setSupportActionBar(il_app_bar.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
    
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.motion,menu)
        return true
    }
    
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
            R.id.action_motion_filter ->{
                val modalBottomSheet = FragmentAppBottomSheet()
                modalBottomSheet.show(supportFragmentManager, FragmentAppBottomSheet.TAG)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    
/*    lateinit var navController: NavController
    
    override fun initializeComponents() {
        setSupportActionBar(il_app_bar.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        vp_motion.adapter = MotionTabAdapter(supportFragmentManager, lifecycle)
        //vp_motion.offscreenPageLimit = 1
        TabLayoutMediator(tl_motion, vp_motion) { tab, position ->
            when (position) {
                0 -> {
                    tab.text = getString(R.string.text_title_tab_sdcard)
                }
                1 -> {
                    tab.text = getString(R.string.text_title_tab_cloud)
                }
                2 -> {
                    tab.text = getString(R.string.text_title_tab_archive)
                }
            }
        }.attach()
    }
    
    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }*/
}