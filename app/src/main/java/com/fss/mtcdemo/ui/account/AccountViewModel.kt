package com.fss.mtcdemo.ui.account

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fss.mtcdemo.base.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by anhdv31 on 11/3/2022.
 */
@HiltViewModel
class AccountViewModel @Inject constructor(
    private val accountRepository: AccountRepository
) : ViewModel() {
    val events: LiveData<Event<UiAccountState>>
        get() = _events
    private val _events = MutableLiveData<Event<UiAccountState>>()
    
    init {
        loadData()
    }
    
    private fun loadData() {
        viewModelScope.launch {
            accountRepository.isShowProgress.observeForever {
                _events.postValue(Event(UiAccountState.ShowProgress(it)))
            }
            accountRepository.success.observeForever {
                _events.postValue(Event(UiAccountState.Success))
            }
            accountRepository.failure.observeForever {
                _events.postValue(Event(UiAccountState.Error(it)))
            }
        }
    }
    
    fun signOut() {
        accountRepository.signOut()
    }
}

sealed class UiAccountState {
    object Success : UiAccountState()
    data class Error(val mes: String?) : UiAccountState()
    data class ShowProgress(val isShow: Boolean?) : UiAccountState()
}