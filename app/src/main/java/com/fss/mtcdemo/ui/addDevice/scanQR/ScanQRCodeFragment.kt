package com.fss.mtcdemo.ui.addDevice.scanQR

import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseFragment
import com.fss.mtcdemo.base.ItemClickListener
import com.fss.mtcdemo.databinding.FragmentScanQrcodeBinding
import com.fss.mtcdemo.ui.home.main.HomeViewModel
import com.fss.mtcdemo.utils.AppLog
import com.google.mlkit.vision.barcode.common.Barcode
import com.google.mlkit.vision.codescanner.GmsBarcodeScannerOptions
import com.google.mlkit.vision.codescanner.GmsBarcodeScanning
import ri.adv.qrcode.AutoFocusMode
import ri.adv.qrcode.CodeScanner
import ri.adv.qrcode.DecodeCallback
import ri.adv.qrcode.ErrorCallback
import ri.adv.qrcode.ScanMode

/**
 * Created by anhdv31 on 10/25/2022.
 */
class ScanQRCodeFragment : DataBindingBaseFragment<FragmentScanQrcodeBinding,BaseViewModel>(BaseViewModel::class.java)
, ItemClickListener {
    
    private lateinit var codeScanner: CodeScanner
    
    private val mViewModel : HomeViewModel by viewModels()
    
    override fun getLayoutRes() = R.layout.fragment_scan_qrcode
    
    override fun initializeComponent() {
        binding.itemClickListener = this
        initializeScanner()
    }
    
    override fun getTitleRes() =R.string.text_title_scan_qrcode
    
    override fun onItemClickListener(view: View) {
    
    }
    
    private fun initializeScanner(){
        /*codeScanner = CodeScanner(requireContext())
        codeScanner.view = binding.csvScanner
        codeScanner.camera = CodeScanner.CAMERA_BACK
        codeScanner.formats = CodeScanner.ALL_FORMATS
        codeScanner.autoFocusMode = AutoFocusMode.SAFE
        codeScanner.scanMode = ScanMode.SINGLE
        codeScanner.isAutoFocusEnabled = true
        codeScanner.isFlashEnabled = false
        codeScanner.decodeCallback = DecodeCallback {
            requireActivity().runOnUiThread {
                Toast.makeText(requireContext(), "Scan result: ${it.text}", Toast.LENGTH_SHORT).show()
            }
        }
    
        codeScanner.errorCallback = ErrorCallback {
            requireActivity().runOnUiThread {
                Toast.makeText(requireContext(), "Camera initialization error: ${it.message}",
                    Toast.LENGTH_SHORT).show()
            }
        }
    
        binding.csvScanner.setOnClickListener {
            codeScanner.startPreview()
        }*/
        val optionsBuilder = GmsBarcodeScannerOptions.Builder()
        val gmsBarcodeScanner = GmsBarcodeScanning.getClient(requireActivity(), optionsBuilder.build())
        gmsBarcodeScanner
            .startScan()
            .addOnSuccessListener { barcode: Barcode ->
                AppLog.d(TAG,"initializeListerner addOnSuccessListener ${barcode.rawValue}")
            }
            .addOnFailureListener { e: Exception ->
                AppLog.d(TAG,"initializeListerner addOnFailureListener ${e.toString()}}")
            }
            .addOnCanceledListener {
                AppLog.d(TAG,"initializeListerner addOnCanceledListener")
            }
    }
}