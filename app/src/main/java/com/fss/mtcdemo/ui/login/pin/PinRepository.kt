package com.fss.mtcdemo.ui.login.pin

import androidx.lifecycle.MutableLiveData
import com.fss.mtcdemo.BuildConfig
import com.fss.mtcdemo.api.ApiService
import com.fss.mtcdemo.api.retrofit.ApiServiceOutSide
import com.fss.mtcdemo.api.retrofit.RepositoryDTO
import com.fss.mtcdemo.base.BaseRepository
import com.fss.mtcdemo.base.QueuedMutableLiveData
import com.fss.mtcdemo.model.FssDevice
import com.fss.mtcdemo.secure.shamirSecret.E2E
import com.fss.mtcdemo.secure.shamirSecret.E2E.createSearchKey
import com.fss.mtcdemo.secure.shamirSecret.ShamirSecretSharing
import com.fss.mtcdemo.utils.*
import com.fss.mtcdemo.utils.AppStatus.*
import com.google.android.gms.common.util.Base64Utils
import com.google.gson.Gson
import com.lib.MTC.MTC
import com.lib.MTC.OnMTCLoginListener
import com.lib.MTC.basic.Futils
import com.lib.MTC.struct.NetCommon
import ftel.cmr.tool.model.ShamirKeyShareReq
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.apache.commons.codec.digest.DigestUtils
import retrofit2.adapter.rxjava2.Result
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Created by anhdv31 on 10/21/2022.
 */
class PinRepository(var apiServiceOutSide: ApiServiceOutSide, var apiService: ApiService) : BaseRepository(), OnMTCLoginListener {
    
    val putRes: MutableLiveData<String?> = MutableLiveData<String?>()
    val putErr: MutableLiveData<String?> = MutableLiveData<String?>()
    
    var success: QueuedMutableLiveData<Int?> = QueuedMutableLiveData<Int?>()
    var failure: QueuedMutableLiveData<String?> = QueuedMutableLiveData<String?>()
    
    var mapHost = HashMap<Int?, String?>()
    var mapHostSearchKey = HashMap<Int?, String?>()
    var mapSearchKey = HashMap<Int?, String?>()
    var base64KeyShares: ArrayList<String> = ArrayList<String>()
    var searchKey = "" //serial_domain_sdt (encrypt by pin)
    var mPin: String? = ""
    
    //var searchKey = "1418212369896Sph!nxRock"
    //var fakePin = "000000"
    var mGenerateKey = ""
    val gson = Gson()
    lateinit var mFssDevice: FssDevice
    
    init {
        initializeHost()
        mFssDevice = FssDevice("", "", "", "", 23456)
    }
    
    private fun putKeyShare(host: String, shareReq: ShamirKeyShareReq): Single<Result<RepositoryDTO<Any>>> {
        AppLog.d(TAG, ">>putKeyShare $host $searchKey ${shareReq.data}")
        return apiServiceOutSide.putKeyshare(host, shareReq.search_key, shareReq.data)
    }
    
    private fun addCam(serial: String?): Single<Result<RepositoryDTO<Any>>> {
        return apiService.addCam(serial)
    }
    
    private fun pinMarked(): Single<Result<RepositoryDTO<Any>>> {
        return apiService.pincodeMarked()
    }
    
    private fun initializeReq(position: Int, host: String): ShamirKeyShareReq {
        val key = createSearchKey(mPin,host)
        val searchKey = hashMD5(key)
        AppLog.d(TAG, ">> createSearchKey $searchKey -- $key")
        mapSearchKey.put(position, searchKey)
        return ShamirKeyShareReq(searchKey, base64KeyShares.get(position))
    }
    
    /*private fun createSearchKey(serial: String?, host: String): String {
        val searchKey = serial + "_" + host + "_" + SharedPref.getInstance().get(SharedPref.AUTH_PHONE_NUMBER, "") + "_" + mPin
        return searchKey
    }*/
    
    private fun initializeHost() {
        mapHost[Constants.PORT_01] = BuildConfig.URL_SM + ":${Constants.PORT_01}/keys/put/"
        mapHost[Constants.PORT_02] = BuildConfig.URL_SM + ":${Constants.PORT_02}/keys/put/"
        mapHost[Constants.PORT_03] = BuildConfig.URL_SM + ":${Constants.PORT_03}/keys/put/"
        mapHost[Constants.PORT_04] = BuildConfig.URL_SM + ":${Constants.PORT_04}/keys/put/"
        mapHost[Constants.PORT_05] = BuildConfig.URL_SM + ":${Constants.PORT_05}/keys/put/"
        
        mapHostSearchKey[Constants.PORT_01] = BuildConfig.URL_SM + ":${Constants.PORT_01}/"
        mapHostSearchKey[Constants.PORT_02] = BuildConfig.URL_SM + ":${Constants.PORT_02}/"
        mapHostSearchKey[Constants.PORT_03] = BuildConfig.URL_SM + ":${Constants.PORT_03}/"
        mapHostSearchKey[Constants.PORT_04] = BuildConfig.URL_SM + ":${Constants.PORT_04}/"
        mapHostSearchKey[Constants.PORT_05] = BuildConfig.URL_SM + ":${Constants.PORT_05}/"
    }
    
    fun requestConnect(info: String?, pin: String?) {
        showProgress()
        val stringParts = info?.split("-")
        println("String split ${stringParts?.size}")
        if (stringParts?.size == 3) {
            mFssDevice.serial = stringParts?.get(0).toString()
            mFssDevice.usr = stringParts?.get(1).toString()
            mFssDevice.pwd = stringParts?.get(2).toString()
            mPin = pin
            loginCloud(mFssDevice.serial, mFssDevice.usr, mFssDevice.pwd)
        } else {
            failure.postValue(getErrorStr(EE_INVALID_QRCODE))
        }
        
    }
    
    private fun loginCloud(uuid: String?, usr: String?, pwd: String?): Int {
        val res = MTC.DEV_LoginCloud(uuid, usr, pwd)
        MTC.loginID = res
        MTC.registerOnDeviceListener(res, this)
        return res
    }
    
    private fun addCamnMarkPinRx() {
        Single.zip(
            addCam(mFssDevice.serial).subscribeOn(Schedulers.io()),
            pinMarked().subscribeOn(Schedulers.io())
        ) { r1, r2 ->
            var statusR1 = r1.response()?.body()?.getCodeStatus()
            var statusR2 = r2.response()?.body()?.getCodeStatus()
            
            return@zip statusR1 == 1200 && statusR2 == 1200
        }
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress() }
            .doFinally { hideProgress() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { value ->
                    if (value) {
                        SharedPref.getInstance().put(SharedPref.AUTH_KEY_E2E_PIN, mPin)
                        SharedPref.getInstance().put(SharedPref.IS_MARKED_E2E_PIN, true)
                        SharedPref.getInstance().put(SharedPref.AUTH_KEY_E2E_KEY, mGenerateKey)
                        success.postValue(SUCCESS)
                    } else {
                        failure.postValue("EE_CANNOT_ADDCAM_MARKEDPIN")
                    }
                    
                },
                { error ->
                    failure.postValue(error.message)
                }
            )
    }
    
    private fun putKeyShareRx() {
        Single.zip(
            putKeyShare(mapHost[Constants.PORT_01]!!, initializeReq(0, mapHostSearchKey[Constants.PORT_01]!!)).subscribeOn(Schedulers.io()),
            putKeyShare(mapHost[Constants.PORT_02]!!, initializeReq(1, mapHostSearchKey[Constants.PORT_02]!!)).subscribeOn(Schedulers.io()),
            putKeyShare(mapHost[Constants.PORT_03]!!, initializeReq(2, mapHostSearchKey[Constants.PORT_03]!!)).subscribeOn(Schedulers.io()),
            putKeyShare(mapHost[Constants.PORT_04]!!, initializeReq(3, mapHostSearchKey[Constants.PORT_04]!!)).subscribeOn(Schedulers.io()),
            putKeyShare(mapHost[Constants.PORT_05]!!, initializeReq(4, mapHostSearchKey[Constants.PORT_05]!!)).subscribeOn(Schedulers.io())
        ) { r1, r2, r3, r4, r5 ->
            var staus00 = r1.response()?.body()?.getCodeStatus()
            var staus01 = r2.response()?.body()?.getCodeStatus()
            var staus02 = r3.response()?.body()?.getCodeStatus()
            var staus03 = r4.response()?.body()?.getCodeStatus()
            var staus04 = r5.response()?.body()?.getCodeStatus()
            return@zip staus00 == 1200 && staus01 == 1200 && staus02 == 1200 && staus03 == 1200 && staus04 == 1200
        }
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress() }
            .doFinally { hideProgress() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { value ->
                    if (value) {
                        addCamnMarkPinRx()
                    } else {
                        failure.postValue(getErrorStr(EE_E2E_PUT_KEY))
                    }
                },
                { error ->
                    failure.postValue(error.message)
                }
            )
    }
    
    private fun createShareByShamir() {
        val k = SharedPref.getInstance().get(SharedPref.AUTH_KEY_E2E_KEY, "")
        mGenerateKey = k.ifEmpty {
            StringUtils.getRandomString(E2E.GENERATE_KEY_LENGTH)
        }
        val secret: ByteArray = mGenerateKey.toByteArray(StandardCharsets.UTF_8)
        val parts = ShamirSecretSharing.createShares(secret, E2E.COUNT, E2E.MIN_THRESHOLD)
        for (value in parts) {
            val eB64Str = Base64Utils.encode(value)
            base64KeyShares.add(eB64Str)
            println("-----> $eB64Str")
            println("---byte--> ${Arrays.toString(value)}")
        }
    }
    
    override fun onLoginSuccess() {
        createShareByShamir()
        val netCommon = NetCommon()
        val array = Futils.ObjToBytes(netCommon)
        val param = ByteArray(array.size)
        Completable
            .timer(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                var ret = MTC.DEV_GetNetCommon(MTC.loginID, param)
                AppLog.d(TAG, ">> DEV_GetNetCommon ${MTC.loginID} $ret")
                if (ret == 0) {
                    Futils.BytesToObj(netCommon, param)
                    mFssDevice.ip = netCommon.st_1_hostIP.ToString()
                    mFssDevice.port = 2131
                    AppLog.d(TAG, ">> GetNetCommon IP  ${mFssDevice.ip} ${mFssDevice.port} ${mGenerateKey}")
                    //syncKey()
                    CoroutineScope(Dispatchers.IO).launch {
                        val retSyncKey = MTC.DEV_SyncKey(mFssDevice.ip, mFssDevice.port!!, mGenerateKey)
                        putKeyShareRx()
                    }
                }
            }, {
                // Error
            })
    }
    
    override fun onLoginError(status: Int) {}
    
    fun hashMD5(srcString: String): String {
        return DigestUtils.md5Hex(srcString).toLowerCase()
    }

/*    fun UnHashMD5(srcString : String, hPwd : String) : String{
        return DigestUtils.
    }
    */
}