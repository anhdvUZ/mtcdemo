package com.fss.mtcdemo.ui.addDevice

import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar.view.*

@AndroidEntryPoint
class AddDeviceActivity : BaseActivity() {
    override fun getLayoutRes() = R.layout.activity_add_device
    
    override fun initializeComponents() {
        setSupportActionBar(il_app_bar.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        initializePermission()
    }
}