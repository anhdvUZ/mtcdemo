package com.fss.mtcdemo.ui.device

import androidx.activity.viewModels
import androidx.navigation.NavController
import androidx.navigation.NavGraph
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.navArgs
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseActivity
import com.fss.mtcdemo.ui.device.main.DeviceControlFragmentDirections
import com.fss.mtcdemo.ui.device.main.UiDeviceControlState
import com.fss.mtcdemo.utils.AppLog
import com.fss.mtcdemo.utils.AppStatus
import com.fss.mtcdemo.utils.SharedPref
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar.view.*

@AndroidEntryPoint
class DeviceControlActivity : BaseActivity() {
    
    val args : DeviceControlActivityArgs by navArgs()
    
    var navController : NavController? = null
    var graph : NavGraph? = null
    
    private val mViewModel : DeviceControlParentViewModel by viewModels()
    
    override fun getLayoutRes() = R.layout.activity_device_control
    
    override fun initializeComponents() {
        setSupportActionBar(il_app_bar.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.device_control_nav_host_fragment) as NavHostFragment
        val inflater = navHostFragment.navController.navInflater
        graph = inflater.inflate(R.navigation.nav_device_control)
        
/*        mViewModel.events.observe(this) {
            handleAction(it.getContentIfNotHandled())
        }*/
/*        if (SharedPref.getInstance().get(SharedPref.AUTH_KEY_E2E_KEY, "").isEmpty()) {
            AppLog.d(TAG,">> setStartDestination verifyPinFragment")
            graph?.setStartDestination(R.id.verifyPinFragment)
        }else{
            AppLog.d(TAG,">> setStartDestination deviceControlFragment")
            graph?.setStartDestination(R.id.deviceControlFragment)
        }*/
        graph?.setStartDestination(R.id.deviceControlFragment)
        navController = navHostFragment.navController
        navController?.setGraph(graph!!,args.toBundle())
    }
    
    private fun handleAction(uiDeviceControlParentState: UiDeviceControlParentState?) {
        when(uiDeviceControlParentState){
            is UiDeviceControlParentState.SwitchToDeviceControl ->{
                graph?.setStartDestination(R.id.deviceControlFragment)
            }
            is UiDeviceControlParentState.SwitchToPin -> {
                graph?.setStartDestination(R.id.pinFragment)
            }
            else -> {
            
            }
        }
    }
}