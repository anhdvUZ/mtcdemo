package com.fss.mtcdemo.ui.motion.sdcard

import android.view.View
import androidx.fragment.app.viewModels
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseFragment
import com.fss.mtcdemo.base.ItemClickListener
import com.fss.mtcdemo.databinding.FragmentSdcardBinding
import com.fss.mtcdemo.utils.AppLog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SDCardFragment : DataBindingBaseFragment<FragmentSdcardBinding, BaseViewModel>(BaseViewModel::class.java)
,ItemClickListener {
    
    private val mViewModel: SDCardViewModel by viewModels()
    
    override fun getLayoutRes(): Int = R.layout.fragment_sdcard
    
    override fun initializeComponent() {
        binding.itemClickListener = this
        binding.sdCardViewModel = mViewModel
        AppLog.d(TAG,"initializeController done ");
    }
    
    override fun onItemClickListener(view: View) {
    
    }
    
    override fun getTitleRes() =R.string.text_title_tab_sdcard
}