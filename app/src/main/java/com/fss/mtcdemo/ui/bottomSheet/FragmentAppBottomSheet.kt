package com.fss.mtcdemo.ui.bottomSheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.setFragmentResult
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.ItemClickListener
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottomsheet_filter_motion.*

/**
 * Created by anhdv31 on 4/8/2022.
 */
class FragmentAppBottomSheet : BottomSheetDialogFragment(),
    View.OnClickListener {
    
    lateinit var mbinding : ViewDataBinding
    
    companion object{
        val TAG = FragmentAppBottomSheet::class.java.simpleName
        const val HALF_DAY  = "HALFDAY"
        const val CUSTOM  = "CUSTOM"
        const val WITHIN_DAY  = "WITHINDAY"
        const val TIMEFILTER  = "TIMEFILTER"
        @JvmStatic
        fun newInstance(type: Int) =
            FragmentAppBottomSheet().apply {
            
            }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mbinding = DataBindingUtil.inflate(inflater,getLayoutRes(),container,false)
        return mbinding.root
    }
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mbinding.lifecycleOwner = this
        tv_half_day.setOnClickListener(this)
        tv_custom.setOnClickListener(this)
        tv_within_day.setOnClickListener(this)
    }
    
    private fun getLayoutRes() = R.layout.bottomsheet_filter_motion
    
    override fun onClick(view: View?) {
        return when (view?.id){
            R.id.tv_half_day ->{
                Toast.makeText(requireActivity(),"Half day",Toast.LENGTH_SHORT).show()
                val result = Bundle().apply {
                    putInt(TIMEFILTER,1)
                }
                setFragmentResult(FragmentAppBottomSheet.TAG,result)
                dismiss()
            }
            R.id.tv_within_day ->{
                val result = Bundle().apply {
                    putInt(TIMEFILTER,2)
                }
                setFragmentResult(FragmentAppBottomSheet.TAG,result)
                dismiss()
                Toast.makeText(requireActivity(),"WithIn",Toast.LENGTH_SHORT).show()
            }
            R.id.tv_custom ->{
                val result = Bundle().apply {
                    putInt(TIMEFILTER,3)
                }
                setFragmentResult(FragmentAppBottomSheet.TAG,result)
                dismiss()
                Toast.makeText(requireActivity(),"Custom",Toast.LENGTH_SHORT).show()
            }
            else -> {}
        }
    }
    
/*    override fun onItemClickListener(view: View) {
        return when (view?.id){
            R.id.tv_half_day ->{
                Toast.makeText(requireActivity(),"Half day",Toast.LENGTH_SHORT).show()
            }
            R.id.tv_custom ->{
                Toast.makeText(requireActivity(),"Custom",Toast.LENGTH_SHORT).show()
            }
            R.id.tv_within_day ->{
                Toast.makeText(requireActivity(),"WithIn",Toast.LENGTH_SHORT).show()
            }
            else -> {}
        }
    }*/
}