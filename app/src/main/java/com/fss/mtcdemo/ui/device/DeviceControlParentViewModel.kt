package com.fss.mtcdemo.ui.device

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fss.mtcdemo.base.Event
import com.fss.mtcdemo.ui.home.main.HomeItemEvent
import com.fss.mtcdemo.utils.SharedPref
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by anhdv31 on 10/31/2022.
 */
@HiltViewModel
class DeviceControlParentViewModel @Inject constructor(
    private val deviceControlParentRepository: DeviceControlParentRepository
) : ViewModel() {
    
    val events: LiveData<Event<UiDeviceControlParentState>>
        get() = _events
    private val _events = MutableLiveData<Event<UiDeviceControlParentState>>()
    
    init {
        loadData()
    }
    
    private fun loadData() {
        viewModelScope.launch {
            if (SharedPref.getInstance().get(SharedPref.AUTH_KEY_E2E_PIN, "").isEmpty()) {
                _events.postValue(Event(UiDeviceControlParentState.SwitchToPin))
            }else{
                _events.postValue(Event(UiDeviceControlParentState.SwitchToDeviceControl))
            }
        }
    }
}

sealed class UiDeviceControlParentState {
    object SwitchToPin : UiDeviceControlParentState()
    object SwitchToDeviceControl : UiDeviceControlParentState()
}