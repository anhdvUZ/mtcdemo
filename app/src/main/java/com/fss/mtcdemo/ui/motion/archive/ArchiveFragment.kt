package com.fss.mtcdemo.ui.motion.archive

import android.view.View
import androidx.fragment.app.viewModels
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseFragment
import com.fss.mtcdemo.base.ItemClickListener
import com.fss.mtcdemo.databinding.FragmentArchiveBinding
import com.fss.mtcdemo.databinding.FragmentSdcardBinding
import com.fss.mtcdemo.ui.motion.sdcard.SDCardViewModel
import com.fss.mtcdemo.utils.AppLog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ArchiveFragment : DataBindingBaseFragment<FragmentArchiveBinding, BaseViewModel>(BaseViewModel::class.java),
ItemClickListener {
    
    private val mViewModel: ArchiveMotionViewModel by viewModels()
    
    override fun getLayoutRes() = R.layout.fragment_archive
    
    override fun initializeComponent() {
        binding.itemClickListener = this
        binding.archiveViewModel = mViewModel
        AppLog.d(TAG,"initializeController done ");
    }
    
    override fun onItemClickListener(view: View) {
    }
    
    override fun getTitleRes() = R.string.text_title_tab_archive
}