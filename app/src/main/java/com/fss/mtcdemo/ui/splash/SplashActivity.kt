package com.fss.mtcdemo.ui.splash

import android.os.Handler
import android.os.Looper
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.example.advmvvm.utils.startMyActivity
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseActivity
import com.fss.mtcdemo.databinding.ActivitySplashBinding
import com.fss.mtcdemo.ui.home.HomeActivity
import com.fss.mtcdemo.ui.login.LoginActivity
import com.fss.mtcdemo.utils.AppLog
import com.fss.mtcdemo.utils.SharedPref

class SplashActivity : DataBindingBaseActivity<ActivitySplashBinding, BaseViewModel>(BaseViewModel::class.java)
    , Animation.AnimationListener {
    
    override fun getLayoutRes() = R.layout.activity_splash
    
    override fun initializeComponents() {
        binding.lifecycleOwner = this
        val displayAnimation = AnimationUtils.loadAnimation(applicationContext,R.anim.transition_from_top)
        displayAnimation.setAnimationListener(this)
        //binding.imgIcon.startAnimation(displayAnimation)
    
        val delayMillis: Long = 1000
        if (SharedPref.getInstance().get(SharedPref.AUTH_TOKEN,"").isNotEmpty()){
            AppLog.d(TAG,"initializeComponents 0")
            Handler(Looper.getMainLooper()).postDelayed({ performNavigationHomeActivity() },delayMillis)
        }else{
            Handler(Looper.getMainLooper()).postDelayed({ performNavigationLoginActivity() },delayMillis)
        }
    }
    
    override fun onAnimationStart(p0: Animation?) {}
    
    override fun onAnimationEnd(p0: Animation?) {
        val delayMillis: Long = 1000
        Handler(Looper.getMainLooper()).postDelayed({ performNavigationHomeActivity() },delayMillis)
    }
    
    override fun onAnimationRepeat(p0: Animation?) {}
    
    private fun performNavigationLoginActivity(){
        this.startMyActivity(LoginActivity::class.java)
        finish()
    }
    
    private fun performNavigationHomeActivity(){
        this.startMyActivity(HomeActivity::class.java)
        finish()
    }
}