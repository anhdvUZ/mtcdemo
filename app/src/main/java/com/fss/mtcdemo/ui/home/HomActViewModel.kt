package com.fss.mtcdemo.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fss.mtcdemo.base.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by anhdv31 on 11/1/2022.
 */
@HiltViewModel
class HomActViewModel @Inject constructor(
    private val homeActRepository: HomeActRepository
) : ViewModel() {
    
    val events: LiveData<Event<UiHomeActState>>
        get() = _events
    private val _events = MutableLiveData<Event<UiHomeActState>>()
    
    init {
        loadData()
    }
    
    private fun loadData() {
        viewModelScope.launch {
            homeActRepository.isShowProgress.observeForever {
                _events.postValue(Event(UiHomeActState.ShowProgress(it)))
            }
            homeActRepository.success.observeForever {
                _events.postValue(Event(UiHomeActState.Success))
            }
            homeActRepository.failure.observeForever {
                _events.postValue(Event(UiHomeActState.Error(it)))
            }
        }
    }
    
    fun signOut() {
        homeActRepository.signOut()
    }
}

sealed class UiHomeActState {
    object Success : UiHomeActState()
    data class Error(val mes: String?) : UiHomeActState()
    data class ShowProgress(val isShow: Boolean?) : UiHomeActState()
}
