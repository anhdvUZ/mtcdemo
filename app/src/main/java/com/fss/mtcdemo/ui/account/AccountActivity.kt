package com.fss.mtcdemo.ui.account

import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar.view.*

@AndroidEntryPoint
class AccountActivity : BaseActivity() {
    override fun getLayoutRes() = R.layout.activity_account
    
    override fun initializeComponents() {
        setSupportActionBar(il_app_bar.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        initializePermission()
    }
    
/*    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }*/
}