package com.fss.mtcdemo.ui.login.pin

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fss.mtcdemo.base.Event
import com.fss.mtcdemo.ui.home.main.HomeItemEvent
import com.fss.mtcdemo.ui.home.main.HomeRepository
import com.fss.mtcdemo.utils.AppLog
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by anhdv31 on 10/21/2022.
 */
@HiltViewModel
class PinViewModel  @Inject constructor(
    private val pinRepository: PinRepository
) : ViewModel() {
    val TAG = javaClass.simpleName
    val events : LiveData<Event<PinUIState>>
        get() = _events
    private val _events = MutableLiveData<Event<PinUIState>>()
    
    init {
        loadData()
    }
    
    private fun loadData() {
        viewModelScope.launch {
    
            pinRepository.success.observeForever {
                AppLog.d(TAG,">>putRes $it")
                _events.postValue(Event(PinUIState.onPutKeyDone))
            }
    
            pinRepository.failure.observeForever {
                AppLog.d(TAG,">>putErr $it")
                _events.postValue(Event(PinUIState.onError(it)))
            }
            pinRepository.isShowProgress.observeForever {
                _events.postValue(Event(PinUIState.ShowProgress(it)))
            }
        }
    }
    
    fun requestAddCam(info : String?,pin: String){
        pinRepository.requestConnect(info,pin)
    }
}

sealed class PinUIState{
    object onPutKeyDone : PinUIState()
    //object onError : PinUIState()
    data class onError(val mes : String?) : PinUIState()
    data class ShowProgress(val isShow : Boolean?) : PinUIState()
}