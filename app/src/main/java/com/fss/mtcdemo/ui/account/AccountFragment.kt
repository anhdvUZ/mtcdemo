package com.fss.mtcdemo.ui.account

import androidx.activity.viewModels
import androidx.fragment.app.viewModels
import com.example.advmvvm.utils.startMyActivity
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseFragment
import com.fss.mtcdemo.databinding.FragmentAboutBinding
import com.fss.mtcdemo.databinding.FragmentAccountManagerBinding
import com.fss.mtcdemo.databinding.FragmentAccountManagerBindingImpl
import com.fss.mtcdemo.ui.home.HomActViewModel
import com.fss.mtcdemo.ui.home.UiHomeActState
import com.fss.mtcdemo.ui.login.LoginActivity
import com.fss.mtcdemo.utils.AppLog
import com.fss.mtcdemo.utils.SharedPref
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AccountFragment : DataBindingBaseFragment<FragmentAccountManagerBinding, BaseViewModel>(BaseViewModel::class.java) {
    override fun getLayoutRes() = R.layout.fragment_account_manager
    private val mViewModel: AccountViewModel by viewModels()
    override fun initializeComponent() {
        mViewModel.events.observe(this) {
            handleAction(it.getContentIfNotHandled())
        }
        
        binding.ilPhone.tvTitle.text = getString(R.string.text_user)
        binding.ilPhone.tvContent.text = SharedPref.getInstance().get(SharedPref.AUTH_PHONE_NUMBER,"")
        binding.ilUsername.tvTitle.text = getString(R.string.text_name)
        binding.ilUsername.tvContent.text = ""
        binding.ilInvoice.tvTitle.text = getString(R.string.text_all_invoice)
        binding.ilAccountManager.tvTitle.text = getString(R.string.text_account_manager)
        binding.ilLogout.clContainer.setOnClickListener {
            MaterialAlertDialogBuilder(requireActivity())
                .setTitle(resources.getString(R.string.text_logout))
                .setMessage(resources.getString(R.string.text_logout_content))
                .setNeutralButton(resources.getString(R.string.text_ok)) { dialog, which ->
                    mViewModel.signOut()
                }
                .setNegativeButton(resources.getString(R.string.text_cancel)) { dialog, which ->
                    dialog.dismiss()
                }
                .show()
        }
    }
    private fun handleAction(state: UiAccountState?) {
        when (state) {
            is UiAccountState.ShowProgress -> {
                setShowProgress(state.isShow!!)
            }
            
            is UiAccountState.Error -> {
                AppLog.d(TAG, "onNavigationItemSelected Error")
            }
            
            is UiAccountState.Success -> {
                AppLog.d(TAG, "onNavigationItemSelected Success")
                performNavigationLoginActivity()
            }
        }
    }
    
    private fun performNavigationLoginActivity(){
        requireActivity().startMyActivity(LoginActivity::class.java)
        requireActivity().finish()
        requireActivity().finishAffinity()
    }
    
    override fun getTitleRes() = R.string.text_title_account
}