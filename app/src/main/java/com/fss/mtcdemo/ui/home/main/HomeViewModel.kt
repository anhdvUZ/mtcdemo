package com.fss.mtcdemo.ui.home.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fss.mtcdemo.base.BaseItemViewModel
import com.fss.mtcdemo.base.Event
import com.fss.mtcdemo.model.Camera
import com.fss.mtcdemo.model.FssDevice
import com.fss.mtcdemo.ui.home.adapter.HomeCameraItemViewModel
import com.fss.mtcdemo.ui.login.loginByPhone.UiSignInState
import com.fss.mtcdemo.utils.AppLog
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by anhdv31 on 3/23/2022.
 */
@HiltViewModel
class HomeViewModel @Inject constructor(
    private val homeRepository: HomeRepository
) : ViewModel() {
    val data : LiveData<List<BaseItemViewModel>>
        get() = _data
    private val _data = MutableLiveData<List<BaseItemViewModel>>(emptyList())
    
    val events : LiveData<Event<HomeItemEvent>>
        get() = _events
    private val _events = MutableLiveData<Event<HomeItemEvent>>()
    
    init {
        loadData()
        homeRepository.getCams()
    }
    
    fun refreshCams(){
        homeRepository.getCams()
    }
    
    private fun loadData() {
        viewModelScope.launch {
    
            homeRepository.success.observeForever {
                //val viewData = createViewModel(homeRepository.getHomeListData())
                val viewModel = createViewModel(it)
                _data.postValue(viewModel)
            }
            
            homeRepository.addSuccess.observeForever {
                _events.postValue(Event(HomeItemEvent.onAddSuccess))
            }
            
            homeRepository.putErr.observeForever {
                _events.postValue(Event(HomeItemEvent.onPutKeyError))
            }
    
            homeRepository.isShowProgress.observeForever {
                _events.postValue(Event(HomeItemEvent.ShowProgress(it)))
            }
        }
    }
    
    private fun createViewModel(listHomeData : List<String>?) : List<BaseItemViewModel>{
        val viewData = mutableListOf<BaseItemViewModel>()
        listHomeData?.forEach {
            viewData.add(HomeCameraItemViewModel("","",0,1,it,::onCameraItemHomeClicked))
        }
        return viewData
    }
    
    private fun onCameraItemHomeClicked(serial: String){
        _events.postValue(Event(HomeItemEvent.SwitchToDeviceControl(serial)))
    }
    
    fun requestAddCam(info : String?,mainKey: String,pin:String){
        homeRepository.requestConnect(info,mainKey,pin)
    }
}
sealed class HomeItemEvent{
    data class SwitchToDeviceControl(val serial: String) : HomeItemEvent()
    data class ShowProgress(val isShow: Boolean?) : HomeItemEvent()
    object onAddSuccess : HomeItemEvent()
    object onPutKeyError : HomeItemEvent()
}