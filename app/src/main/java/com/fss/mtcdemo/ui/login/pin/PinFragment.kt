package com.fss.mtcdemo.ui.login.pin

import android.content.Intent
import android.view.View
import androidx.fragment.app.viewModels
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseFragment
import com.fss.mtcdemo.base.ItemClickListener
import com.fss.mtcdemo.databinding.FragmentPinBinding
import com.fss.mtcdemo.ui.home.HomeActivity
import com.fss.mtcdemo.utils.AppLog
import com.google.mlkit.vision.barcode.common.Barcode
import com.google.mlkit.vision.codescanner.GmsBarcodeScannerOptions
import com.google.mlkit.vision.codescanner.GmsBarcodeScanning
import dagger.hilt.android.AndroidEntryPoint


/**
 * Created by anhdv31 on 10/21/2022.
 */
@AndroidEntryPoint
class PinFragment : DataBindingBaseFragment<FragmentPinBinding,BaseViewModel>(BaseViewModel::class.java)
,ItemClickListener{
    
    private val mViewModel : PinViewModel by viewModels()
    
    override fun getLayoutRes() = R.layout.fragment_pin
    
    override fun initializeComponent() {
        binding.lifecycleOwner = this
        binding.itemClickListener = this
        mViewModel.events.observe(this) {
            handleAction(it.getContentIfNotHandled())
        }
    }
    
    override fun getTitleRes() = R.string.text_pin
    
    override fun onItemClickListener(view: View) {
        when(view.id){
            R.id.btn_accept -> {
                //Navigation.findNavController(binding.btnAccept).navigate(R.id.action_pinFragment_to_homeActivity,null)
                val optionsBuilder = GmsBarcodeScannerOptions.Builder()
                val gmsBarcodeScanner = GmsBarcodeScanning.getClient(requireActivity(), optionsBuilder.build())
                gmsBarcodeScanner
                    .startScan()
                    .addOnSuccessListener { barcode: Barcode ->
                        AppLog.d(TAG,">>initializeListerner addOnSuccessListener ${barcode.rawValue}")
                        mViewModel.requestAddCam(barcode.rawValue,binding.pinOtpEdt.text.toString())
                    }
                    .addOnFailureListener { e: Exception ->
                        AppLog.d(TAG,">>initializeListerner addOnFailureListener ${e.toString()}}")
                    }
                    .addOnCanceledListener {
                        AppLog.d(TAG,">>initializeListerner addOnCanceledListener")
            }
            }
        }
    }
    
    private fun handleAction(pinUIState: PinUIState?) {
        when (pinUIState) {
            is PinUIState.ShowProgress ->{
                setShowProgress(pinUIState.isShow!!)
            }
            is PinUIState.onError -> {
                setShowDialogNotice(R.string.text_error, pinUIState.mes!!)
            }
            is PinUIState.onPutKeyDone ->{
                setShowProgress(false)
                performNavigationHomeActivity()
            }
        }
    }
    
    private fun performNavigationHomeActivity(){
        val dialogIntent = Intent(requireContext(), HomeActivity::class.java)
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        requireActivity().startActivity(dialogIntent);
        requireActivity().finish()
    }
    
}