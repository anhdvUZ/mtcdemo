package com.fss.mtcdemo.ui.device.verifyPin

import com.fss.mtcdemo.BuildConfig
import com.fss.mtcdemo.api.ApiService
import com.fss.mtcdemo.api.retrofit.ApiServiceOutSide
import com.fss.mtcdemo.api.retrofit.RepositoryDTO
import com.fss.mtcdemo.base.BaseRepository
import com.fss.mtcdemo.base.QueuedMutableLiveData
import com.fss.mtcdemo.model.FssDevice
import com.fss.mtcdemo.model.StreamInfo
import com.fss.mtcdemo.secure.aes.FssJnCryptor
import com.fss.mtcdemo.secure.shamirSecret.E2E
import com.fss.mtcdemo.secure.shamirSecret.E2E.MIN_THRESHOLD
import com.fss.mtcdemo.secure.shamirSecret.E2E.createSearchKey
import com.fss.mtcdemo.secure.shamirSecret.ShamirSecretSharing
import com.fss.mtcdemo.utils.AppLog
import com.fss.mtcdemo.utils.AppStatus
import com.fss.mtcdemo.utils.Constants
import com.fss.mtcdemo.utils.SharedPref
import com.google.android.gms.common.util.Base64Utils
import com.google.gson.Gson
import ftel.cmr.tool.model.ShamirKeyShareReq
import ftel.cmr.tool.model.ShamirKeyShareRes
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.apache.commons.codec.digest.DigestUtils
import retrofit2.adapter.rxjava2.Result
import java.nio.charset.StandardCharsets
import java.util.*
import kotlin.collections.HashMap

/**
 * Created by anhdv31 on 11/1/2022.
 */
class VerifyPinRepository(var apiServiceOutSide: ApiServiceOutSide) : BaseRepository() {
    
    var success: QueuedMutableLiveData<FssDevice> = QueuedMutableLiveData<FssDevice>()
    var failure: QueuedMutableLiveData<Int?> = QueuedMutableLiveData<Int?>()
    
    var arrShareKeyInByteTemp: Array<ByteArray?> = arrayOfNulls(E2E.MAX_THRESHOLD)//E2E.THRESHOLD
    var arrShareKeyInByte: Array<ByteArray?> = arrayOfNulls(E2E.MAX_THRESHOLD)
    //var arrShareKeyInByte: Array<ByteArray?> = arrayOfNulls(E2E.THRESHOLD)
    var resKeyCombine = ""
    var mapHost = HashMap<Int?, String?>()
    var mapHostSearchKey = HashMap<Int?, String?>()
    var mFssDevice: FssDevice? = null
    var mPin : String? = ""
    
    init {
        initializeHost()
        mFssDevice = FssDevice("", "", "", "", 23456)
    }
    
    private fun initializeHost() {
        mapHost[Constants.PORT_01] = BuildConfig.URL_SM + ":${Constants.PORT_01}/keys/get/"
        mapHost[Constants.PORT_02] = BuildConfig.URL_SM + ":${Constants.PORT_02}/keys/get/"
        mapHost[Constants.PORT_03] = BuildConfig.URL_SM + ":${Constants.PORT_03}/keys/get/"
        mapHost[Constants.PORT_04] = BuildConfig.URL_SM + ":${Constants.PORT_04}/keys/get/"
        mapHost[Constants.PORT_05] = BuildConfig.URL_SM + ":${Constants.PORT_05}/keys/get/"
        
        mapHostSearchKey[Constants.PORT_01] = BuildConfig.URL_SM + ":${Constants.PORT_01}/"
        mapHostSearchKey[Constants.PORT_02] = BuildConfig.URL_SM + ":${Constants.PORT_02}/"
        mapHostSearchKey[Constants.PORT_03] = BuildConfig.URL_SM + ":${Constants.PORT_03}/"
        mapHostSearchKey[Constants.PORT_04] = BuildConfig.URL_SM + ":${Constants.PORT_04}/"
        mapHostSearchKey[Constants.PORT_05] = BuildConfig.URL_SM + ":${Constants.PORT_05}/"
    }
    
    private fun initializeReq(position: Int, host: String): ShamirKeyShareReq {
        val key = createSearchKey(mPin, host)
        val searchKey = hashMD5(key)
        AppLog.d(TAG, ">> createSearchKey $searchKey -- $key")
        return ShamirKeyShareReq(searchKey,"")
    }
    
    fun hashMD5(srcString: String): String {
        return DigestUtils.md5Hex(srcString).toLowerCase()
    }
    
    private fun getKeyShare(host: String, shareReq: ShamirKeyShareReq): Single<Result<RepositoryDTO<ShamirKeyShareRes>>> {
        return apiServiceOutSide.getKeyshare(host, shareReq.search_key)
    }
    
/*    private fun createSearchKey(serial: String?, host: String): String {
        return serial + "_" + host + "_" + SharedPref.getInstance().get(SharedPref.AUTH_PHONE_NUMBER, "") +"_"+mPin
    }*/
    
    fun verifyPin(fssDevice: FssDevice?,pin:String?){
        mFssDevice = fssDevice
        mPin = pin
        getKeyShareRx()
    }
    
    fun getKeyShareRx(){
        Single.zip(
            getKeyShare(mapHost[Constants.PORT_01]!!,initializeReq(0,mapHostSearchKey[Constants.PORT_01]!!)).subscribeOn(Schedulers.io()),
            getKeyShare(mapHost[Constants.PORT_02]!!,initializeReq(1,mapHostSearchKey[Constants.PORT_02]!!)).subscribeOn(Schedulers.io()),
            getKeyShare(mapHost[Constants.PORT_03]!!,initializeReq(2,mapHostSearchKey[Constants.PORT_03]!!)).subscribeOn(Schedulers.io()),
            getKeyShare(mapHost[Constants.PORT_04]!!,initializeReq(3,mapHostSearchKey[Constants.PORT_04]!!)).subscribeOn(Schedulers.io()),
            getKeyShare(mapHost[Constants.PORT_05]!!,initializeReq(4,mapHostSearchKey[Constants.PORT_05]!!)).subscribeOn(Schedulers.io())
        ){ r1,r2,r3,r4,r5 ->
            return@zip checkStatus(r1,r2,r3,r4,r5)
        }
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress() }
            .doOnSuccess { hideProgress() }
            .doOnError { hideProgress() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { value ->
                    if (resKeyCombine.isNotEmpty()){
                        SharedPref.getInstance().put(SharedPref.AUTH_KEY_E2E_PIN,mPin)
                        SharedPref.getInstance().put(SharedPref.AUTH_KEY_E2E_KEY,resKeyCombine)
                        AppLog.d(TAG, ">>value $mPin $resKeyCombine ${Gson().toJson(mFssDevice)}")
                        success.postValue(mFssDevice)
                    }else{
                        failure.postValue(AppStatus.EE_INVALID_PIN)
                    }
                    AppLog.d(TAG, "value " + value)
                },
                { error -> AppLog.d(TAG, "error ${error.message}") }
            )
    }
    
    fun checkStatus(r1:Result<RepositoryDTO<ShamirKeyShareRes>>,
                    r2:Result<RepositoryDTO<ShamirKeyShareRes>>,
                    r3:Result<RepositoryDTO<ShamirKeyShareRes>>,
                    r4:Result<RepositoryDTO<ShamirKeyShareRes>>,
                    r5:Result<RepositoryDTO<ShamirKeyShareRes>>) : Int{
        var sizeKey00 = r1.response()?.body()?.getData()?.data
        var sizeKey01 = r2.response()?.body()?.getData()?.data
        var sizeKey02 = r3.response()?.body()?.getData()?.data
        var sizeKey03 = r4.response()?.body()?.getData()?.data
        var sizeKey04 = r5.response()?.body()?.getData()?.data
        AppLog.d(TAG, ">> checkStatus -- ${sizeKey00 != null} ${sizeKey01 != null} ${sizeKey02 != null} ${sizeKey03 != null} ${sizeKey04 != null}")
        if (sizeKey00 == null  && sizeKey01 == null  && sizeKey02 == null  && sizeKey03 == null  && sizeKey04 == null ){
            AppLog.d(TAG, ">> checkStatus 0")
            return AppStatus.EE_INVALID_PIN
        }
        AppLog.d(TAG, ">> checkStatus $sizeKey00 $sizeKey01 $sizeKey02 $sizeKey03 $sizeKey04")
        var countGetShareKey = 0
        if (sizeKey00 != null){
            AppLog.d(TAG, ">> checkStatus decode r1 0 ${r1.response()?.body()?.getData()?.data}")
            arrShareKeyInByteTemp[countGetShareKey] = Base64Utils.decode(r1.response()?.body()?.getData()?.data!!)
            countGetShareKey++
        }else{
            AppLog.d(TAG, ">> checkStatus decode r1 1 null ")
        }
        if (sizeKey01 != null){
            AppLog.d(TAG, ">> checkStatus decode r2 0 $countGetShareKey")
            arrShareKeyInByteTemp[countGetShareKey] = Base64Utils.decode(r2.response()?.body()?.getData()?.data!!)
            countGetShareKey++
        }else{
            AppLog.d(TAG, ">> checkStatus decode r2 1 null ")
        }
        if (sizeKey02 != null){
            AppLog.d(TAG, ">> checkStatus decode r3 0 $countGetShareKey")
            arrShareKeyInByteTemp[countGetShareKey] =Base64Utils.decode(r3.response()?.body()?.getData()?.data!!)
            countGetShareKey++
        }else{
            AppLog.d(TAG, ">> checkStatus decode r3 1 null ")
        }
        if (sizeKey03 != null){
            AppLog.d(TAG, ">> checkStatus decode r4 0 $countGetShareKey")
            arrShareKeyInByteTemp[countGetShareKey] = Base64Utils.decode(r4.response()?.body()?.getData()?.data!!)
            countGetShareKey++
        }else{
            AppLog.d(TAG, ">> checkStatus decode r4 1 null ")
        }
        if (sizeKey04 != null){
            AppLog.d(TAG, ">> checkStatus decode r5 0 $countGetShareKey")
            arrShareKeyInByteTemp[countGetShareKey] = Base64Utils.decode(r5.response()?.body()?.getData()?.data!!)
            countGetShareKey++
        }else{
            AppLog.d(TAG, ">> checkStatus decode r5 1 null ")
        }
    
        AppLog.d(TAG, ">> checkStatus 1 $countGetShareKey ${arrShareKeyInByteTemp.size}")
        if (countGetShareKey >= MIN_THRESHOLD){
            AppLog.d(TAG, ">> checkStatus 2 ${arrShareKeyInByteTemp.size}")
            for ((index, value) in arrShareKeyInByteTemp.withIndex()) {
                if (value!=null){
                    arrShareKeyInByte[index] = value
                }
            }
            AppLog.d(TAG, ">> checkStatus 3 ${arrShareKeyInByte.size}")
            resKeyCombine = String(ShamirSecretSharing.combineShares(arrShareKeyInByte, E2E.GENERATE_KEY_LENGTH), StandardCharsets.UTF_8)
            return AppStatus.OK_E2E_READY_TO_COMBINE
        }else{
            AppLog.d(TAG, ">> checkStatus 4")
            return AppStatus.EE_E2E_NOT_READY_TO_COMBINE
        }
    }
    
}