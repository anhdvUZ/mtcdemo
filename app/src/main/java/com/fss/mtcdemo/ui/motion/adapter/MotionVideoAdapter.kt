package com.fss.mtcdemo.ui.motion.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.fss.mtcdemo.BR
import com.fss.mtcdemo.base.BaseItemViewModel
import com.fss.mtcdemo.ui.home.adapter.HomeAdapter
import com.fss.mtcdemo.ui.home.adapter.HomeViewHolder

/**
 * Created by anhdv31 on 3/24/2022.
 */
class MotionVideoAdapter : RecyclerView.Adapter<MotionVideoViewHolder>(){
    var itemViewModels: List<BaseItemViewModel> = emptyList()
    private val viewTypeToLayoutId: MutableMap<Int, Int> = mutableMapOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MotionVideoViewHolder {
        val binding: ViewDataBinding =
            DataBindingUtil
                .inflate(
                    LayoutInflater.from(parent.context), viewTypeToLayoutId[viewType] ?: 0,
                    parent,
                    false
                )
        return MotionVideoViewHolder(binding)
    }
    
    override fun onBindViewHolder(holder: MotionVideoViewHolder, position: Int) {
        holder.bind(itemViewModels[position])
    }
    
    override fun getItemViewType(position: Int): Int {
        val item = itemViewModels[position]
        if (!viewTypeToLayoutId.containsKey(item.viewType)) {
            viewTypeToLayoutId[item.viewType] = item.layoutId
        }
        return item.viewType
    }
    
    override fun getItemCount(): Int = itemViewModels.size
    
    fun updateItems(items: List<BaseItemViewModel>?) {
        itemViewModels = items ?: emptyList()
        notifyDataSetChanged()
    }
}

class MotionVideoViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(itemViewModel: BaseItemViewModel) {
        binding.setVariable(BR.videoMotionItemViewModel, itemViewModel)
    }
}

@BindingAdapter("motionVideoItemViewModels")
fun bindingItemViews(recyclerView: RecyclerView, itemViewModels:List<BaseItemViewModel>?){
    val adapter = getOrCreateAdapter(recyclerView)
    adapter.updateItems(itemViewModels)
}

private fun getOrCreateAdapter(recyclerView: RecyclerView): MotionVideoAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is MotionVideoAdapter) {
        recyclerView.adapter as MotionVideoAdapter
    } else {
        val bindableRecyclerAdapter = MotionVideoAdapter()
        recyclerView.adapter = bindableRecyclerAdapter
        bindableRecyclerAdapter
    }
}
