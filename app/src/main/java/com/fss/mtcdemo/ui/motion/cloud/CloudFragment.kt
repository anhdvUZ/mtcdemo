package com.fss.mtcdemo.ui.motion.cloud

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentResultListener
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseFragment
import com.fss.mtcdemo.base.ItemClickListener
import com.fss.mtcdemo.databinding.FragmentCloudBinding
import com.fss.mtcdemo.model.VideoDevice
import com.fss.mtcdemo.ui.bottomSheet.FragmentAppBottomSheet
import com.fss.mtcdemo.ui.motonPlayback.MotionPlaybackActivity
import com.fss.mtcdemo.utils.AppLog
import com.google.gson.Gson
import com.lib.MTC.MTC
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CloudFragment : DataBindingBaseFragment<FragmentCloudBinding, BaseViewModel>(BaseViewModel::class.java)
    , FragmentResultListener
    , ItemClickListener {
    
    private val mViewModel: CloudMotionViewModel by viewModels()
    
    override fun getLayoutRes() = R.layout.fragment_cloud
    
    private var mPlayHandleId = -1
    //val args : CloudFragmentArgs by navArgs()
    
    override fun initializeComponent() {
        binding.itemClickListener = this
        binding.cloudMotionViewModel = mViewModel
        
        mViewModel.setTypeFilter(1)
        
        mViewModel.events.observe(this) {
            handleAction(it.getContentIfNotHandled())
        }
        
        mViewModel.isShowLoading.observe(this) {
            binding.pgbLoading.visibility = (if (it) {
                View.VISIBLE
            } else {
                View.GONE
            })
        }
        
        parentFragmentManager.setFragmentResultListener(FragmentAppBottomSheet.TAG, viewLifecycleOwner,this)
    }
    
    private fun handleAction(cloudMotionItemEvent: CloudMotionItemEvent?) {
        when (cloudMotionItemEvent) {
            is CloudMotionItemEvent.OnMotionClick -> {
                var intent = Intent(requireContext(), MotionPlaybackActivity::class.java)
                intent.putExtra(VideoDevice::class.java.simpleName, cloudMotionItemEvent.videoDevice)
                startActivity(intent)
            }
        }
    }
    
    override fun onItemClickListener(view: View) {
    
    }
    
    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CloudFragment().apply {
            
            }
    }
    
    override fun onDestroyView() {
        MTC.Play_CloseStream(mPlayHandleId)
        super.onDestroyView()
    }
    
    override fun onFragmentResult(requestKey: String, result: Bundle) {
        when(requestKey){
            FragmentAppBottomSheet.TAG -> {
                val res = result.getInt(FragmentAppBottomSheet.TIMEFILTER)
                mViewModel.setTypeFilter(res)
            }
        }
    }
    
    override fun getTitleRes() = R.string.text_title_device_home_screen
}