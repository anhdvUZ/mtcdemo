package com.fss.mtcdemo.ui.motion.sdcard

import com.fss.mtcdemo.model.Camera
import com.fss.mtcdemo.model.VideoDevice
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Created by anhdv31 on 3/24/2022.
 */
class SDCardRepository {
    val gson = Gson()
    
    fun getSDCardVideosData():List<VideoDevice>{
        val typeToken = object: TypeToken<List<VideoDevice>>(){}.type
        //return gson.fromJson(getRawJsonVideosData(),typeToken)
        return gson.fromJson(getRawJsonVideosDataEmpty(),typeToken)
    }
    
    private fun getRawJsonVideosData() : String {
        return "[{id:0,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:0},{id:1,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:1},{id:2,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:0},{id:3,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:0},{id:4,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:0},{id:5,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:1},{id:6,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:0},{id:7,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:1},{id:8,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:0},{id:9,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:0},{id:10,startTime:1648092604,endTime:1648092604,motionImage:\"\",typeStorage:0}]".trimIndent()
    }
    
    private fun getRawJsonVideosDataEmpty() : String {
        return "[]".trimIndent()
    }
}