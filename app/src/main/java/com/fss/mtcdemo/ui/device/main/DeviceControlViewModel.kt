package com.fss.mtcdemo.ui.device.main

import androidx.lifecycle.*
import com.fss.mtcdemo.api.retrofit.RepositoryDTO
import com.fss.mtcdemo.base.BaseItemViewModel
import com.fss.mtcdemo.base.Event
import com.fss.mtcdemo.base.QueuedMutableLiveData
import com.fss.mtcdemo.model.Camera
import com.fss.mtcdemo.model.FssDevice
import com.fss.mtcdemo.ui.home.adapter.HomeCameraItemViewModel
import com.fss.mtcdemo.utils.AppLog
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.lifecycle.HiltViewModel
import ftel.cmr.tool.model.ShamirKeyShareReq
import io.reactivex.Single
import kotlinx.coroutines.launch
import retrofit2.adapter.rxjava2.Result
import javax.inject.Inject

/**
 * Created by anhdv31 on 3/31/2022.
 */
@HiltViewModel
class DeviceControlViewModel @Inject constructor(
    private val deviceControlRepository: DeviceControlRepository,
    savedStateHandle: SavedStateHandle
): ViewModel() {
    val TAG = javaClass.simpleName
    val events: LiveData<Event<UiDeviceControlState>>
        get() = _events
    private val _events = QueuedMutableLiveData<Event<UiDeviceControlState>>()
    
    var fssDevice : FssDevice? = null
    
    init {
        fssDevice = savedStateHandle["fssDevice"]
        AppLog.d(TAG,">>init ${Gson().toJson(fssDevice)}")
        loadData()
        deviceControlRepository.prepareStream(fssDevice)
    }
    
    private fun loadData() {
        viewModelScope.launch {
            deviceControlRepository.putStatus.observeForever {
                AppLog.d(TAG,">>putStatus.observeForever $${Gson().toJson(it)}" )
                _events.postValue(Event(UiDeviceControlState.ReadyToStream(it?.loginId,it?.key)))
            }
            deviceControlRepository.putErr.observeForever {
                AppLog.d(TAG,">>putErr.observeForever $it" )
                _events.postValue(Event(UiDeviceControlState.FailToStream(it)))
            }
        }
    }
    fun verifyPin(pin: String?){
        deviceControlRepository.verifyPin(fssDevice,pin)
    }
    
}
sealed class UiDeviceControlState{
    data class ReadyToStream (val loginId : Int?,val key : String?) : UiDeviceControlState()
    data class FailToStream (val errorCode : Int?) : UiDeviceControlState()
}