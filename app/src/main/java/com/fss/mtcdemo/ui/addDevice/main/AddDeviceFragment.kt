package com.fss.mtcdemo.ui.addDevice.main

import android.view.View
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseDialogFragment
import com.fss.mtcdemo.base.DataBindingBaseFragment
import com.fss.mtcdemo.base.ItemClickListener
import com.fss.mtcdemo.databinding.FragmentAddDeviceBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddDeviceFragment : DataBindingBaseFragment<FragmentAddDeviceBinding, BaseViewModel>(BaseViewModel::class.java)
, ItemClickListener {
    override fun getLayoutRes() = R.layout.fragment_add_device
    
    override fun initializeComponent() {
        binding.itemClickListener = this
    }
    
    override fun onItemClickListener(view: View) {
        TODO("Not yet implemented")
    }
    
    override fun getTitleRes() = R.string.text_title_add_device_screen
}