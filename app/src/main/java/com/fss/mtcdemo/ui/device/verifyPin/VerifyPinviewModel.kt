package com.fss.mtcdemo.ui.device.verifyPin

import androidx.lifecycle.*
import com.fss.mtcdemo.base.Event
import com.fss.mtcdemo.model.FssDevice
import com.fss.mtcdemo.utils.AppLog
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by anhdv31 on 11/1/2022.
 */
@HiltViewModel
class VerifyPinviewModel @Inject constructor(
    private val verifyPinRepository: VerifyPinRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    val TAG = javaClass.simpleName
    val events: LiveData<Event<VerifyPinUIState>>
        get() = _events
    private val _events = MutableLiveData<Event<VerifyPinUIState>>()
    var fssDevice : FssDevice? = null
    init {
        fssDevice = savedStateHandle["fssDevice"]
        AppLog.d(TAG,">>init ${Gson().toJson(fssDevice)}")
        loadData()
    }
    
    private fun loadData() {
        viewModelScope.launch {
            verifyPinRepository.success.observeForever {
                AppLog.d(TAG,"verifyPinRepository.success ${Gson().toJson(it)}")
                _events.postValue(Event(VerifyPinUIState.Success(it)))
            }
            
            verifyPinRepository.failure.observeForever {
                _events.postValue(Event(VerifyPinUIState.Error(it)))
            }
            
            verifyPinRepository.isShowProgress.observeForever {
                _events.postValue(Event(VerifyPinUIState.ShowProgress(it)))
            }
        }
    }
    
    fun verifyPin(pin: String?){
        verifyPinRepository.verifyPin(fssDevice,pin)
    }
    
    fun skipPin(){
        _events.postValue(Event(VerifyPinUIState.SkipPin(fssDevice!!)))
    }
}

sealed class VerifyPinUIState {
    data class SkipPin(val fssDevice: FssDevice) : VerifyPinUIState()
    data class Success(val fssDevice: FssDevice) : VerifyPinUIState()
    data class Error(val errCode: Int?) : VerifyPinUIState()
    data class ShowProgress(val isShow: Boolean?) : VerifyPinUIState()
}