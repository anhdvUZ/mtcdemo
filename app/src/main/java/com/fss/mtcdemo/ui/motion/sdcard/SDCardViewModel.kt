package com.fss.mtcdemo.ui.motion.sdcard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fss.mtcdemo.base.BaseItemViewModel
import com.fss.mtcdemo.base.Event
import com.fss.mtcdemo.model.VideoDevice
import com.fss.mtcdemo.ui.motion.adapter.MotionVideoItemViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.random.Random

/**
 * Created by anhdv31 on 3/24/2022.
 */
@HiltViewModel
class SDCardViewModel @Inject constructor(
    private val sdCardRepository: SDCardRepository
) : ViewModel() {
    val data: LiveData<List<BaseItemViewModel>>
        get() = _data
    private val _data = MutableLiveData<List<BaseItemViewModel>>(emptyList())
    
    val events: LiveData<Event<SDCardItemEvent>>
        get() = _events
    private val _events = MutableLiveData<Event<SDCardItemEvent>>()
    
    init {
        loadData()
    }
    
    private fun loadData() {
        viewModelScope.launch {
            val viewData = createViewModel(sdCardRepository.getSDCardVideosData())
            _data.postValue(viewData)
        }
    }
    
    private fun createViewModel(listVideoDevice: List<VideoDevice>): List<BaseItemViewModel> {
        val viewData = mutableListOf<BaseItemViewModel>()
        listVideoDevice.forEach {
            //viewData.add(MotionVideoItemViewModel(it.id, it.startTime, it.endTime, it.motionImage, it.typeStorage, ::onVideoItemSDCardClicked))
            viewData.add(MotionVideoItemViewModel(Random.nextInt(0, 10000),it.player_handleId,it.vid_url, it.start_time, it.thumb_url,1,it.type,::onVideoItemSDCardClicked))
        }
        return viewData
    }
    
    /*private fun onVideoItemSDCardClicked(idItem: Int) {
        _events.postValue(Event(SDCardItemEvent.OnVideoClick(idItem)))
    }*/
    
    private fun onVideoItemSDCardClicked(videoDevice: VideoDevice) {
        _events.postValue(Event(SDCardItemEvent.OnVideoClick(videoDevice)))
    }
}

sealed class SDCardItemEvent {
    data class OnVideoClick(val videoDevice: VideoDevice) : SDCardItemEvent()
}