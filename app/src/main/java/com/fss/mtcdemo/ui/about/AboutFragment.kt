package com.fss.mtcdemo.ui.about

import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseFragment
import com.fss.mtcdemo.databinding.FragmentAboutBinding

class AboutFragment : DataBindingBaseFragment<FragmentAboutBinding, BaseViewModel>(BaseViewModel::class.java) {
    override fun getLayoutRes() = R.layout.fragment_about
    
    override fun initializeComponent() {
    
    }
    
    override fun getTitleRes() = R.string.text_title_about
}