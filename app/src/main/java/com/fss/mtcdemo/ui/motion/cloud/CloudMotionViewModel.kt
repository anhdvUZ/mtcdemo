package com.fss.mtcdemo.ui.motion.cloud

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fss.mtcdemo.base.BaseItemViewModel
import com.fss.mtcdemo.base.Event
import com.fss.mtcdemo.model.VideoDevice
import com.fss.mtcdemo.ui.motion.adapter.MotionVideoItemViewModel
import com.fss.mtcdemo.utils.AppLog
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.random.Random

/**
 * Created by anhdv31 on 4/5/2022.
 */
@HiltViewModel
class CloudMotionViewModel @Inject constructor(
    private val cloudMotionRepository: CloudMotionRepository
) : ViewModel() {
    
    var isShowLoading = MutableLiveData<Boolean>()
    
    fun isShowLoading(value: Boolean) {
        isShowLoading.value = value
        isShowLoading.postValue(value)
    }
    
    val serial = MutableLiveData<Int>()
    
    fun setSerial(value: Int) {
        AppLog.d("CloudMotionViewModel","ANHDV loadData setPlayHandleId " + value)
        serial.value = value
        serial.postValue(value)
    }
    
    var typeFilter = MutableLiveData<Int>()
    
    fun setTypeFilter(value: Int) {
        typeFilter.value = value
        typeFilter.postValue(value)
        loadData()
    }
    
    val data: LiveData<List<BaseItemViewModel>>
        get() = _data
    private val _data = MutableLiveData<List<BaseItemViewModel>>(emptyList())
    
    val events: LiveData<Event<CloudMotionItemEvent>>
        get() = _events
    private val _events = MutableLiveData<Event<CloudMotionItemEvent>>()
    
    init {
        loadData()
    }
    
    private fun loadData() {
        isShowLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            //val viewData = createViewModel(cloudMotionRepository.getCloudMotionsData())
            val viewData = createViewModel(cloudMotionRepository.cloudSearchbyTime("8792934df4756401", typeFilter.value!!))
            _data.postValue(viewData)
            isShowLoading.postValue(false)
        }
    }
    
    private fun createViewModel(listVideoDevice: List<VideoDevice>): List<BaseItemViewModel> {
        val viewData = mutableListOf<BaseItemViewModel>()
        listVideoDevice.forEach {
            //viewData.add(MotionVideoItemViewModel(it.id, it.startTime, it.endTime, it.motionImage, it.typeStorage, ::onCloudMotionClicked))
            viewData.add(MotionVideoItemViewModel(Random.nextInt(0, 10000),it.player_handleId,it.vid_url, it.start_time, it.thumb_url,1,it.type,::onCloudMotionClicked))
        }
        return viewData
    }
    
    private fun onCloudMotionClicked(videoDevice: VideoDevice) {
        _events.postValue(Event(CloudMotionItemEvent.OnMotionClick(videoDevice)))
    }
}

sealed class CloudMotionItemEvent {
    data class OnMotionClick(val videoDevice: VideoDevice) : CloudMotionItemEvent()
}