package com.fss.mtcdemo.ui.login.loginByPhone

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fss.mtcdemo.base.Event
import com.fss.mtcdemo.utils.AppLog
import dagger.hilt.android.lifecycle.HiltViewModel
import ftel.cmr.tool.model.LoginReq
import ftel.cmr.tool.model.LoginRes
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by anhdv31 on 10/21/2022.
 */
@HiltViewModel
class LoginByPhoneViewModel @Inject constructor(
    private val loginRepository: LoginByPhoneRepository
) : ViewModel() {
    val dataLoginReq: LiveData<LoginReq>
        get() = _dataLoginReq
    private val _dataLoginReq = MutableLiveData<LoginReq>()
    
    val dataLoginRes: LiveData<LoginRes>
        get() = _dataLoginRes
    private val _dataLoginRes = MutableLiveData<LoginRes>()
    
    val events: LiveData<Event<UiSignInState>>
        get() = _events
    private val _events = MutableLiveData<Event<UiSignInState>>()
    
    var loginReq: LoginReq? = null
    
    init {
        loadData()
    }
    
    fun loadData() {
        viewModelScope.launch {
            loginRepository.success.observeForever {
                _events.postValue(Event(UiSignInState.Success(it)))
            }
            
            loginRepository.failure.observeForever {
                _events.postValue(Event(UiSignInState.Error(it)))
            }
            
            loginRepository.isShowProgress.observeForever {
                _events.postValue(Event(UiSignInState.ShowProgress(it)))
            }
        }
    }
    
    fun requestLogin(user: String, pwd: String) {
        loginReq = LoginReq(user, pwd)
        val regex = Regex("^[0-9]{10,12}\$")
        val result = user.matches(regex)
        if (result) {
            if (pwd.trim().length <=3 ){
                _events.postValue(Event(UiSignInState.ErrorInputPassword))
            }else{
                loginRepository.requestSignInRx(loginReq)
            }
        }else{
            _events.postValue(Event(UiSignInState.ErrorInputPhoneNum))
        }
    }
}

sealed class UiSignInState {
    data class SignIn(val id: String, val phone: String?) : UiSignInState()
    data class Error(val status: String?) : UiSignInState()
    object ErrorInputPhoneNum : UiSignInState()
    object ErrorInputPassword : UiSignInState()
    data class Success(val status: Int?) : UiSignInState()
    data class ShowProgress(val isShow: Boolean?) : UiSignInState()
}