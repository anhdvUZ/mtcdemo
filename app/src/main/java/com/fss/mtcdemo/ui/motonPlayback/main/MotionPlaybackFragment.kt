package com.fss.mtcdemo.ui.motonPlayback.main

import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.navigation.navArgs
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseViewModel
import com.fss.mtcdemo.base.DataBindingBaseFragment
import com.fss.mtcdemo.base.ItemClickListener
import com.fss.mtcdemo.databinding.FragmentMotionPlaybackBinding
import com.fss.mtcdemo.model.VideoDevice
import com.fss.mtcdemo.utils.AppLog
import com.fss.mtcdemo.widget.AppVideoView
import com.google.gson.Gson
import com.lib.MTC.OnMTCMediaListener
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MotionPlaybackFragment : DataBindingBaseFragment<FragmentMotionPlaybackBinding, BaseViewModel>(BaseViewModel::class.java)
    ,AppVideoView.AppVideoListener
, ItemClickListener {
    
    override fun getLayoutRes() = R.layout.fragment_motion_playback
    
    private val mViewModel : MotionPlaybackViewModel by activityViewModels()
    //val args : MotionPlaybackFragmentArgs by navArgs()
    
    override fun initializeComponent() {
        //var videoDevice : VideoDevice? = arguments?.getParcelable(VideoDevice::class.java.simpleName)
        binding.itemClickListener = this
        binding.videoDevice = mViewModel.videoDevice
        binding.mtcVideoview.setOnInfoListener(this)
        mViewModel.videoDevice.observe(viewLifecycleOwner, Observer {
            binding.mtcVideoview.handleId = it.player_handleId!!
            binding.mtcVideoview.cloudUrlStart(it.vid_url)
        })
    }
    
    override fun onDestroyView() {
        binding.mtcVideoview.unSetOnInfoListener(this)
        /*
        * release in CloudFragment
        * */
        //binding.mtcVideoview.release()
        super.onDestroyView()
    }
    
    override fun onAppMediaInfo(codecID: Int, width: Int, height: Int) {
        binding.pgbLoading.hide()
        val builder = StringBuilder()
        builder.append(binding.motionInfo.text).append(" ").append(width.toString()).append(" x ").append(height)
        binding.motionInfo.text = builder.toString()
        binding.ibPlay.setImageResource(R.drawable.ic_pause_motion)
    }
    
    override fun onAppMediaStop() {
    
    }
    
    override fun onItemClickListener(view: View) {
        when(view.id){
            R.id.ib_play -> {
                Toast.makeText(requireActivity(),"Motion Stop",Toast.LENGTH_SHORT).show()
                
            }
        }
    }
    
    override fun getTitleRes() = R.string.text_title_device_player_screen
    
}