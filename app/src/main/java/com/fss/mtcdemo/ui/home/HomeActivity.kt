package com.fss.mtcdemo.ui.home

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.advmvvm.utils.startMyActivity
import com.fss.mtcdemo.R
import com.fss.mtcdemo.base.BaseActivity
import com.fss.mtcdemo.ui.about.AboutFragment
import com.fss.mtcdemo.ui.account.AccountActivity
import com.fss.mtcdemo.ui.login.LoginActivity
import com.fss.mtcdemo.utils.AppLog
import com.fss.mtcdemo.utils.SharedPref
import com.google.android.material.navigation.NavigationView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar.view.*
import kotlinx.android.synthetic.main.nav_header_main.view.*


@AndroidEntryPoint
class HomeActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {
    
    private lateinit var appBarConfiguration: AppBarConfiguration
    private val mViewModel: HomActViewModel by viewModels()
    override fun getLayoutRes() = R.layout.activity_home
    var navController: NavController? = null
    override fun initializeComponents() {
        setSupportActionBar(il_app_bar.toolbar)
        //il_app_bar.tb_title.text = resources.getString(R.string.text_title_device_home_screen)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        navController = findNavController(R.id.home_nav_host_fragment)
        
        appBarConfiguration = AppBarConfiguration(
            setOf(R.id.nav_home
            ), drawer_layout
        )
        
        setupActionBarWithNavController(navController!!, appBarConfiguration)
        nav_view.setupWithNavController(navController!!)
        //nav_view.setNavigationItemSelectedListener(this)
    
        nav_view.getHeaderView(0).tv_username.text = String.format(getString(R.string.text_account_manager_welcome),
            SharedPref.getInstance().get(SharedPref.AUTH_PHONE_NUMBER,""))
        nav_view.getHeaderView(0).ib_account_manager.setOnClickListener {
            AppLog.d(TAG, ">> getHeaderView setOnClickListener")
            performNavigationAccountActivity()
        }
        mViewModel.events.observe(this) {
            handleAction(it.getContentIfNotHandled())
        }
        
    }
    
    private fun performNavigationAccountActivity(){
        this.startMyActivity(AccountActivity::class.java)
    }
    
    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.home_nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
    
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        
        when (item.itemId) {
            /*R.id.nav_sign_out -> {
                AppLog.d(TAG, "onNavigationItemSelected nav_sign_out")
                //mViewModel.signOut()
                MaterialAlertDialogBuilder(this)
                    .setTitle(resources.getString(R.string.text_logout))
                    .setMessage(resources.getString(R.string.text_logout_content))
                    .setNeutralButton(resources.getString(R.string.text_ok)) { dialog, which ->
                        mViewModel.signOut()
                    }
                    .setNegativeButton(resources.getString(R.string.text_cancel)) { dialog, which ->
                        dialog.dismiss()
                    }
                    .show()
            }*/
            R.id.nav_about -> {
                //navController?.navigate(R.id.action_homeFragment_to_aboutFragment, null)
                navController!!.navigate(R.id.action_homeFragment_to_aboutFragment, null)
            }
            
        }
        return true
    }
    
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val bundle = Bundle()
        return when (item.itemId) {
            com.fss.mtcdemo.R.id.nav_about -> {
            
                // Manually build the NavOptions that manually do
                // what NavigationUI.onNavDestinationSelected does for you
                val navOptions: NavOptions = NavOptions.Builder()
                    .setPopUpTo(com.fss.mtcdemo.R.id.nav_home, false, true)
                    .setRestoreState(true)
                    .build()
                /*val navController = Navigation.findNavController(
                    this,
                    R.id.home_nav_host_fragment
                )*/
                navController!!.navigate(R.id.action_homeFragment_to_aboutFragment, null)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    
    private fun handleAction(state: UiHomeActState?) {
        when (state) {
            is UiHomeActState.ShowProgress -> {
                setShowProgress(state.isShow!!)
            }
            
            is UiHomeActState.Error -> {
                AppLog.d(TAG, "onNavigationItemSelected Error")
                
            }
            
            is UiHomeActState.Success -> {
                AppLog.d(TAG, "onNavigationItemSelected Success")
                performNavigationLoginActivity()
            }
        }
    }
    
    private fun performNavigationLoginActivity(){
        this.startMyActivity(LoginActivity::class.java)
        finish()
    }
    
    override fun onBackPressed() {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        val f = supportFragmentManager.findFragmentById(R.id.home_nav_host_fragment)
        var currentTitle = il_app_bar.toolbar.tb_title.text
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            AppLog.d(TAG, ">>onBackPressed 1")
            drawer.closeDrawer(GravityCompat.START)
        } else {
            if(currentTitle.equals(getString(R.string.text_title_about))){
                AppLog.d(TAG, ">>onBackPressed 2")
                super.onBackPressed()
            }else{
                AppLog.d(TAG, ">>onBackPressed 3")
                finish()
            }
        }
    }
    
}