package com.fss.mtcdemo.ui.motion.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.fss.mtcdemo.ui.motion.archive.ArchiveFragment
import com.fss.mtcdemo.ui.motion.cloud.CloudFragment
import com.fss.mtcdemo.ui.motion.sdcard.SDCardFragment

/**
 * Created by anhdv31 on 3/24/2022.
 */
class MotionTabAdapter(fragment : FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter (fragment,lifecycle){
    override fun getItemCount() = 3
    
    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> {
                 SDCardFragment()
            }
            1 -> {
                 CloudFragment()
            }
            2 -> {
                 ArchiveFragment()
            }
            else -> {
                SDCardFragment()
            }
        }
    }
}