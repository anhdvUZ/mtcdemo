package ftel.cmr.tool.model

import com.google.gson.annotations.SerializedName

/**
 * Created by anhdv31 on 7/12/2022.
 */
data class ShamirKeyShareReq (
    @SerializedName("search_key")
    var search_key : String,
    @SerializedName("data")
    var data : String
)