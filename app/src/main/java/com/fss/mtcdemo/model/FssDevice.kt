package com.fss.mtcdemo.model

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by anhdv31 on 3/23/2022.
 */
@Parcelize
data class FssDevice(
    var serial : String,
    var usr : String,
    var pwd : String,
    var ip : String?,
    var port : Int?
):Parcelable{
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt()
    )
}