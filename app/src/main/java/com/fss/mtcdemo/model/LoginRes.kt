package ftel.cmr.tool.model

import com.google.gson.annotations.SerializedName

/**
 * Created by anhdv31 on 7/7/2022.
 */
data class LoginRes(
    @SerializedName("token")
    var token : String,
    @SerializedName("is_pincode")
    var is_pincode : String
)
