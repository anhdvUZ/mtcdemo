package ftel.cmr.tool.model

import com.google.gson.annotations.SerializedName

/**
 * Created by anhdv31 on 7/7/2022.
 */
data class LoginReq(
    @SerializedName("phone_number")
    var phone_number: String,
    @SerializedName("password")
    var password: String
)
