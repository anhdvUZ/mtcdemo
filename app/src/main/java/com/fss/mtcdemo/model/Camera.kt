package com.fss.mtcdemo.model

/**
 * Created by anhdv31 on 3/23/2022.
 */
data class Camera(
    val id : Int,
    val title : String,
    val description : String,
    val icon : Int,
    val status : Int,
)