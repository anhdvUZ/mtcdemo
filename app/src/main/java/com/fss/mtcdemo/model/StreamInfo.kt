package com.fss.mtcdemo.model

/**
 * Created by anhdv31 on 3/23/2022.
 */
data class StreamInfo(
    val loginId : Int?,
    val key : String?
)