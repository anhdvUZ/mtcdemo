package com.fss.mtcdemo.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by anhdv31 on 3/24/2022.
 */
@Parcelize
data class VideoDevice (
/*    val id : Int,
    val startTime : Long,
    val endTime : Long,
    val motionImage : String,
    val typeStorage : Int*/
/*    val id : Int,
    val startTime : Long,
    val endTime : Long,
    val motionImage : String,
    val typeStorage : Int,*/
    var player_handleId: Int?,
    val vid_url: String?,
    val start_time: String?,
    val thumb_url: String?,
    var type: Int?
): Parcelable

