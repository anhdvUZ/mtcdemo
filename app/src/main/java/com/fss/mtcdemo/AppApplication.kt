package com.fss.mtcdemo

import android.app.Application
import android.content.Context
import com.fss.mtcdemo.utils.Utils
import com.lib.MTC.MTC
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by anhdv31 on 3/23/2022.
 */
@HiltAndroidApp
class AppApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
        MTC.SDK_init()
        MTC.SetDebug()
        Utils.init(this)
    }
    
    companion object {
        @JvmStatic
        lateinit var appContext: Context
            private set
        
        @JvmStatic
        lateinit var app: AppApplication
            private set
        
        //Using web socket
        @JvmField
        var created = false
        
        @JvmField
        var enableLogFunSupport = false
        
        // dispatchTouchEvent
        @JvmField
        var inProgress = false
        
        operator fun get(context: Context): AppApplication {
            return context.applicationContext as AppApplication
        }
        
        fun create(context: Context): AppApplication {
            return Companion[context]
        }
    }
}