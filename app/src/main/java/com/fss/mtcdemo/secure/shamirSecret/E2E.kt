package com.fss.mtcdemo.secure.shamirSecret

import com.fss.mtcdemo.BuildConfig
import com.fss.mtcdemo.utils.Constants
import com.fss.mtcdemo.utils.SharedPref
import org.apache.commons.codec.digest.DigestUtils

/**
 * Created by anhdv31 on 10/31/2022.
 */
object E2E {
    
    const val COUNT = 5
    const val MIN_THRESHOLD = 3
    const val MAX_THRESHOLD = 5
    const val GENERATE_KEY_LENGTH = 20
    const val PIN_LENGTH = 6
    
    var mapHostPut = HashMap<Int?, String?>()
    var mapHostGet = HashMap<Int?, String?>()
    var mapHostSearchKey = HashMap<Int?, String?>()
    
    init {
        initializeHost()
    }
    
    private fun initializeHost() {
        mapHostGet[Constants.PORT_01] = BuildConfig.URL_SM + ":${Constants.PORT_01}/keys/get/"
        mapHostGet[Constants.PORT_02] = BuildConfig.URL_SM + ":${Constants.PORT_02}/keys/get/"
        mapHostGet[Constants.PORT_03] = BuildConfig.URL_SM + ":${Constants.PORT_03}/keys/get/"
        mapHostGet[Constants.PORT_04] = BuildConfig.URL_SM + ":${Constants.PORT_04}/keys/get/"
        mapHostGet[Constants.PORT_05] = BuildConfig.URL_SM + ":${Constants.PORT_05}/keys/get/"
    
        mapHostPut[Constants.PORT_01] = BuildConfig.URL_SM + ":${Constants.PORT_01}/keys/put/"
        mapHostPut[Constants.PORT_02] = BuildConfig.URL_SM + ":${Constants.PORT_02}/keys/put/"
        mapHostPut[Constants.PORT_03] = BuildConfig.URL_SM + ":${Constants.PORT_03}/keys/put/"
        mapHostPut[Constants.PORT_04] = BuildConfig.URL_SM + ":${Constants.PORT_04}/keys/put/"
        mapHostPut[Constants.PORT_05] = BuildConfig.URL_SM + ":${Constants.PORT_05}/keys/put/"
        
        mapHostSearchKey[Constants.PORT_01] = BuildConfig.URL_SM + ":${Constants.PORT_01}/"
        mapHostSearchKey[Constants.PORT_02] = BuildConfig.URL_SM + ":${Constants.PORT_02}/"
        mapHostSearchKey[Constants.PORT_03] = BuildConfig.URL_SM + ":${Constants.PORT_03}/"
        mapHostSearchKey[Constants.PORT_04] = BuildConfig.URL_SM + ":${Constants.PORT_04}/"
        mapHostSearchKey[Constants.PORT_05] = BuildConfig.URL_SM + ":${Constants.PORT_05}/"
    }
    
    fun createSearchKey(pin: String?, host: String) :String{
        return host + "_" + SharedPref.getInstance().get(SharedPref.AUTH_PHONE_NUMBER, "") +"_"+pin
    }
    
    fun hashMD5(srcString: String): String {
        return DigestUtils.md5Hex(srcString).toLowerCase()
    }
}