package com.fss.mtcdemo.secure.aes;

import android.util.Base64;

import com.fss.mtcdemo.utils.Constants;
import com.fss.mtcdemo.utils.SharedPref;

import org.apache.commons.lang3.StringUtils;
import org.cryptonode.jncryptor.AES256JNCryptor;
import org.cryptonode.jncryptor.CryptorException;

public class FssJnCryptor extends AES256JNCryptor {
    private static FssJnCryptor INSTANCE;

    public static FssJnCryptor getInstance() {
        if (INSTANCE == null) {
            synchronized (FssJnCryptor.class) {
                INSTANCE = new FssJnCryptor();
            }
        }
        return INSTANCE;
    }

    public FssJnCryptor() {
    }

    public String deCryptString(String encrypted, String passWord) {
        int iteration = SharedPref.getInstance().get(SharedPref.AUTH_USER_ITERATION, Constants.PBKDF_DEFAULT_ITERATIONS);
        return deCryptString(encrypted, passWord, iteration);
    }

    public String deCryptString(String encrypted, String passWord, int iteration) {
        if (StringUtils.isEmpty(encrypted) || StringUtils.isEmpty(passWord)) return "";
        setPBKDFIterations(iteration == 0 ? Constants.PBKDF_DEFAULT_ITERATIONS : iteration);
        try {
            byte[] encryptedData = Base64.decode(encrypted, 0);
            return new String(decryptData(encryptedData, passWord.toCharArray())).trim();
        } catch (CryptorException e) {
            // Something went wrong
            e.printStackTrace();
        }
        return "";
    }


    public String encryptString(String decrypted, String passWord) {
        int iteration = SharedPref.getInstance()
                .get(SharedPref.AUTH_USER_ITERATION, Constants.PBKDF_DEFAULT_ITERATIONS);
        return encryptString(decrypted, passWord, iteration);
    }

    public String encryptString(String decrypted, String passWord, int iteration) {
        if (StringUtils.isEmpty(decrypted) || StringUtils.isEmpty(passWord)) return "";
        setPBKDFIterations(iteration == 0 ? Constants.PBKDF_DEFAULT_ITERATIONS : iteration);
        byte[] plaintext = decrypted.getBytes();
        try {
            return Base64.encodeToString(encryptData(plaintext, passWord.toCharArray()), 0).trim();
        } catch (CryptorException e) {
            // Something went wrong
            e.printStackTrace();
        }
        return "";
    }
}
