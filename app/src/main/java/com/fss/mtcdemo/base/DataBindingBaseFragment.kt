package com.fss.mtcdemo.base

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import com.fss.mtcdemo.R
import com.fss.mtcdemo.widget.dialog.ProgressDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.activity_device_control.*
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.app_bar.view.*
import java.lang.Exception

/**
 * Created by anhdv31 on 2/14/2022.
 */
abstract class DataBindingBaseFragment<DB: ViewDataBinding,VM:BaseViewModel>(private val viewModelClass: Class<VM>) : Fragment(){
    protected var TAG = javaClass.simpleName
    lateinit var viewModel: VM
    lateinit var binding: DB
    private var dialogProgress: ProgressDialog? = null
    private var dialogNotice: Dialog? = null
    
    private fun getViewMD() : VM = ViewModelProvider(this).get(viewModelClass)
    private fun initializeBindingLayout(inflater: LayoutInflater, container: ViewGroup?) {
        binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false)
    }

    @LayoutRes
    abstract fun getLayoutRes(): Int

    abstract fun initializeComponent()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewMD();
    }
    
    private fun initializeProgressDialog(){
        /*val builder: AlertDialog.Builder = AlertDialog.Builder(requireActivity())
        builder.setView(R.layout.layout_progress_dialog)
        dialogProgress = builder.setCancelable(false).create()*/
        dialogProgress = ProgressDialog(requireActivity())
        dialogProgress!!.initializeProgressDialog()
    }
    
    fun setShowDialogNotice(title: Int,content: Int){
        MaterialAlertDialogBuilder(requireActivity())
            .setTitle(resources.getString(title))
            .setMessage(resources.getString(content))
            .setNeutralButton(resources.getString(R.string.text_ok)) { dialog, which ->
                dialog.dismiss()
            }
            .show()
    }
    
    fun setShowDialogNotice(title: Int,content: String?){
        setShowProgress(false)
        MaterialAlertDialogBuilder(requireActivity())
            .setTitle(resources.getString(title))
            .setMessage(content)
            .setNeutralButton(resources.getString(R.string.text_ok)) { dialog, which ->
                dialog.dismiss()
            }
            .show()
    }
    
    fun setShowProgress(isShow : Boolean){
        if (isShow)dialogProgress?.showProgressDialog();
        else dialogProgress?.dismissProgressDialog();
    }
    
    fun cancelProgress(){
        if (dialogProgress!=null && dialogProgress!!.isShowing()){
            dialogProgress!!.cancelProgressDialog()
        }
    }
    
    override fun onDestroyView() {
        super.onDestroyView()
        cancelProgress()
    }
    
    private fun initializeToolbar(){
        //(requireActivity() as AppCompatActivity).supportActionBar?.title = getTitleRes().toString()
        //(requireActivity() as AppCompatActivity).il_app_bar.tb_title.text = getTitleRes().toString()
        if (getTitleRes() != null
            && requireActivity().il_app_bar != null
            && requireActivity().il_app_bar.toolbar.tb_title != null){
            requireActivity().il_app_bar.toolbar.tb_title.setText(getTitleRes())
        }
        /*if (getTitleRes() != null
            && requireActivity().appBarLayout != null
            && requireActivity().appBarLayout.tb_title != null){
            requireActivity().appBarLayout.tb_title.setText(getTitleRes())
        }*/
         /*if (getTitleRes() != null && requireActivity().appBarLayout != null){
             AppLog.d("ANHDV","initializeToolbar " + getTitleRes())
             requireActivity().appBarLayout.toolbar.setTitle(getTitleRes())
         }else {
             AppLog.d("ANHDV","initializeToolbar " + (getTitleRes()!=null))
         }*/
    }
    
    @StringRes
    abstract fun getTitleRes(): Int
    
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        initializeBindingLayout(inflater, container)
        super.onCreateView(inflater, container, savedInstanceState)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeToolbar()
        initializeComponent()
        initializeProgressDialog()
    }
    
    protected open fun getNavOptions(): NavOptions? {
        return NavOptions.Builder()
            .setEnterAnim(R.anim.slide_in_right)
            .setExitAnim(R.anim.slide_out_left)
            .setPopEnterAnim(R.anim.slide_in_left)
            .setPopExitAnim(R.anim.slide_out_right)
            .build()
    }
    
}