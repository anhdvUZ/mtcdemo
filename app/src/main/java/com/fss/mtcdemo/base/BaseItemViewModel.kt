package com.fss.mtcdemo.base

import androidx.annotation.LayoutRes

/**
 * Created by anhdv31 on 2/21/2022.
 */
interface BaseItemViewModel {
    @get: LayoutRes
    val layoutId: Int
    val viewType: Int
        get() = 0
}
    
        