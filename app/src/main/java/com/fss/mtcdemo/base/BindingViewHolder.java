package com.fss.mtcdemo.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

public abstract class BindingViewHolder<T, V extends ViewDataBinding> extends RecyclerView.ViewHolder {
    protected String TAG = getClass().getSimpleName();
    protected V mViewBinding;
    protected View mView;

    abstract int getResLayout();

    public BindingViewHolder(@NonNull V viewBinding) {
        super(viewBinding.getRoot());
        mView = viewBinding.getRoot();
        mViewBinding = viewBinding;
    }
/*    RecyclerView.ViewHolder create(ViewGroup parent){
        View view = LayoutInflater.from(parent.getContext()).inflate(getResLayout(),parent,false);
        return BindingViewHolder()
    }*/

    public abstract void bind(T data);
}
