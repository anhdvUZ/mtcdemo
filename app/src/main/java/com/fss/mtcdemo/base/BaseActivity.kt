package com.fss.mtcdemo.base

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.fss.mtcdemo.R
import com.fss.mtcdemo.utils.AppLog
import com.fss.mtcdemo.utils.PermissionUtils
import com.fss.mtcdemo.utils.PermissionUtils.REQUEST_PERMISSION_CAMERA
import com.fss.mtcdemo.widget.dialog.ProgressDialog

/**
 * Created by anhdv31 on 1/25/2022.
 */
abstract  class BaseActivity : AppCompatActivity(){
    
    val TAG = javaClass.simpleName
    
    private var dialogProgress: ProgressDialog? = null
    
    abstract fun getLayoutRes(): Int

    abstract fun initializeComponents()
    
    private fun initializeProgressDialog(){
        /*val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setView(R.layout.layout_progress_dialog)
        dialogProgress = builder.create()*/
        dialogProgress = ProgressDialog(this)
        dialogProgress!!.initializeProgressDialog()
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutRes())
        initializeProgressDialog()
        initializeComponents()
    }
    
    fun setShowProgress(isShow : Boolean){
        if (isShow)dialogProgress?.showProgressDialog();
        else dialogProgress?.dismissProgressDialog();
    }
    
    override fun onSupportNavigateUp(): Boolean {
        AppLog.d(TAG,"ANHDV onSupportNavigateUp ")
        onBackPressed()
        return true
    }
    
    fun initializePermission(){
        if (!PermissionUtils.permissionCheck(this, PermissionUtils.PERMISSIONS_CAMERA)) {
            //FunPath.init(this, FunPath.APP_PATH)
        } else {
            PermissionUtils.permissionCheck(this, PermissionUtils.PERMISSIONS_CAMERA, REQUEST_PERMISSION_CAMERA)
        }
    }
}