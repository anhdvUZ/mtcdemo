package com.fss.mtcdemo.base

import com.fss.mtcdemo.api.retrofit.ApiFilterResultState
import com.fss.mtcdemo.api.retrofit.RepositoryDTO
import com.fss.mtcdemo.api.retrofit.resultRxJava2MapToApiFilterResultState
import retrofit2.adapter.rxjava2.Result

/**
 * Created by anhdv31 on 7/11/2022.
 */
open class BaseRepository {
    val TAG = javaClass.simpleName
    val isShowProgress: QueuedMutableLiveData<Boolean?> = QueuedMutableLiveData<Boolean?>()
    
    fun showProgress() {
        isShowProgress.postValue(true)
    }
    
    fun hideProgress() {
        isShowProgress.postValue(false)
    }
    
    interface OnFilterCallBack<T> {
        fun callback(apiFilterResult: ApiFilterResultState<T>)
    }
    
    fun <T> filterApi(
        ipcResult: Result<RepositoryDTO<T>>? = null,
        callBack: OnFilterCallBack<T>? = null,
    ): Boolean {
        callBack?.callback(ipcResult.resultRxJava2MapToApiFilterResultState())
        return ipcResult?.response()?.body()?.getResult() ?: false
    }
}