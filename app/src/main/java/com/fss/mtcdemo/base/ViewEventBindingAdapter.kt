package com.fss.mtcdemo.base

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.fss.mtcdemo.utils.AppLog

/**
 * Created by anhdv31 on 4/1/2022.
 */
object ViewEventBindingAdapter {
    @JvmStatic
    @BindingAdapter("visibleGone")
    fun showHide(view: View, show: Boolean) {
        view.visibility = if (show) View.VISIBLE else View.GONE
    }
    
    @JvmStatic
    @BindingAdapter("imageUrl")
    fun loadImage(view: ImageView , url: String?) {
        AppLog.d("ViewEventBindingAdapter","ANHDV ViewEventBindingAdapter " + url)
        Glide.with(view.context).load(url).into(view)
    }
}