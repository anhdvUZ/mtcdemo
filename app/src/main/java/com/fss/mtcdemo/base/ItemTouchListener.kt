package com.fss.mtcdemo.base

import android.view.View

/**
 * Created by anhdv31 on 2/14/2022.
 */
interface ItemTouchListener {
    fun onItemTouchListener(view: View)
}