package com.fss.mtcdemo.api.retrofit


/**
 * Author: bangdinh95@gmail.com
 * Date: 3/30/2022
 */
sealed class ApiFilterResultState<out T> {
    object OnUnAuthorized : ApiFilterResultState<Nothing>()
    data class OnLicenseExpired(var error: Failure) : ApiFilterResultState<Nothing>()
    data class OnLostPermissionPlaceOrCamera(var error: Failure) : ApiFilterResultState<Nothing>()
    data class OnCameraCannotApplyCode(var error: Failure) : ApiFilterResultState<Nothing>()
    data class OnError(var error: Failure) : ApiFilterResultState<Nothing>()
    data class OnSuccess<out R>(val resultApi: ResultApi<R>) : ApiFilterResultState<R>()
    data class OnUnKnown(val status: Status) : ApiFilterResultState<Nothing>()
}

data class Status(var code: Int = StatusCodeServer.ERROR_UNKNOWN, var msg: String? = "")

//data class ResultApi<out R>(val datas: List<R>, val data: R?, val status: Status)
data class ResultApi<out R>(val data: R?, val status: Status)