package com.fss.mtcdemo.api

class RouteService {
    companion object {
        const val u = "{u}"
        const val up = "u"
        private var mInstance: RouteService? = null
        val instance: RouteService
            get() {
                if (mInstance == null) {
                    mInstance = RouteService()
                }
                return mInstance!!
            }
    }
}