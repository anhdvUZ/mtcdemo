package com.fss.mtcdemo.api.retrofit

import com.fss.mtcdemo.api.retrofit.RepositoryDTO
import com.fss.mtcdemo.model.FssDevice
import ftel.cmr.tool.model.LoginReq
import ftel.cmr.tool.model.LoginRes
import ftel.cmr.tool.model.ShamirKeyShareReq
import ftel.cmr.tool.model.ShamirKeyShareRes
import io.reactivex.Single
import retrofit2.adapter.rxjava2.Result
import retrofit2.http.*
/**
 * Created by anhdv31 on 7/7/2022.
 */
interface ApiServiceOutSide {
    @POST
    @FormUrlEncoded
    fun getKeyshare(@Url url:String, @Field("search_key") searchKey : String): Single<Result<RepositoryDTO<ShamirKeyShareRes>>>
    
    @POST
    @FormUrlEncoded
    fun putKeyshare(@Url url:String, @Field("search_key") searchKey : String, @Field("data") data: String): Single<Result<RepositoryDTO<Any>>>
}