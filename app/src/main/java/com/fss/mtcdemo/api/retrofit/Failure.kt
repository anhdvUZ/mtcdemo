package com.fss.mtcdemo.api.retrofit


/**
 * Author: bangdinh95@gmail.com
 * Date: 3/31/2022
 */
data class Failure(
    var codeStatus: Int = StatusCodeServer.ERROR_UNKNOWN,
    var title: String = "",
    var message: String = "",
)