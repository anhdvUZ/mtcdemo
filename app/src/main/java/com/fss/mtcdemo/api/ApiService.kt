package com.fss.mtcdemo.api

import com.fss.mtcdemo.api.retrofit.RepositoryDTO
import com.fss.mtcdemo.model.FssDevice
import ftel.cmr.tool.model.LoginReq
import ftel.cmr.tool.model.LoginRes
import ftel.cmr.tool.model.ShamirKeyShareReq
import ftel.cmr.tool.model.ShamirKeyShareRes
import io.reactivex.Single
import retrofit2.adapter.rxjava2.Result
import retrofit2.http.*
/**
 * Created by anhdv31 on 7/7/2022.
 */
interface ApiService {
    @POST
    @FormUrlEncoded
    @Headers("No-Authentication: true")
    fun getKeyshare(@Url url:String, @Field("search_key") searchKey : String): Single<Result<RepositoryDTO<ShamirKeyShareRes>>>
    
    @POST("user/sign-in/")
    fun signInUp(@Body loginRequest: LoginReq?): Single<Result<RepositoryDTO<Any>>>
    
    @POST("user/sign-out/")
    fun signOut(): Single<Result<RepositoryDTO<Any>>>
    
    @POST("user/pincode-marked/")
    fun pincodeMarked(): Single<Result<RepositoryDTO<Any>>>
    
    @POST("cmrs/add/")
    @FormUrlEncoded
    fun addCam(@Field("serial") serial : String?): Single<Result<RepositoryDTO<Any>>>
    
    @POST("cmrs/list/")
    fun getCams(): Single<Result<RepositoryDTO<List<String>>>>
}