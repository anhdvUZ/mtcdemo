package com.fss.mtcdemo.api


import com.fss.mtcdemo.utils.AppLog
import com.fss.mtcdemo.utils.SharedPref
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import org.apache.commons.lang3.StringUtils
import java.util.*

class RequestInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        var compressedRequest: Request.Builder = originalRequest.newBuilder()
        //.addHeader("Content-Type", "application/x-www-form-urlencoded")
        //.addHeader("Content-Type", "charset=UTF-8")
        //.addHeader("gis", BuildConfig.GIS)
        //.addHeader("gis", "d0123d2e185f4c5961d53d21c0ef8df2")
        //.addHeader("package", BuildConfig.APP_PACKAGE);
        compressedRequest.addHeader("Content-Type", "application/json; charset=UTF-8")
        var token =
            //AppSharedPrefsUtil!!.instance.getString(AppSharedPrefsUtil.AUTH_TOKEN, "")
            SharedPref.getInstance().get(SharedPref.AUTH_TOKEN, "")
                ?.trim { it <= ' ' }
        //val language = IPCBoxApplication.localeManager!!.language
        val language = Locale.getDefault().language
        
        if (StringUtils.isNotEmpty(token)) {
            AppLog.d("RequestInterceptor", "ANHDV Authorization " + token)
            compressedRequest.addHeader("Authorization", String.format("Token %s", token))
        }
        //compressedRequest.addHeader("language", language)
        //.method(originalRequest.method, originalRequest.body)
        return chain.proceed(compressedRequest.build())
    }
}
