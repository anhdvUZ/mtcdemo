package com.fss.mtcdemo.api.retrofit

import com.fss.mtcdemo.utils.AppLog
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import javax.net.ssl.HttpsURLConnection
import retrofit2.adapter.rxjava2.Result

data class RepositoryDTO<T>(

    @SerializedName("data")
    //private var datas: List<T>? = null,
    private var datas: T? = null,

    @SerializedName("message")
    private var message: String? = null,

    @SerializedName("code_status")
    private var codeStatus: Int? = null,

    @SerializedName("result")
    private var result: Boolean? = null,

    @SerializedName("title")
    private var title: String? = null,
) {

/*    fun getDatas(): List<T> {
        return if (datas.isNullOrEmpty()) {
            arrayListOf()
        } else {
            datas!!
        }
    }*/

    fun getData(): T? {
        return datas
    }

    fun getMessage(): String {
        return message ?: ""
    }

    fun getCodeStatus(): Int {
        return codeStatus ?: StatusCodeServer.ERROR_UNKNOWN
    }

    fun getResult(): Boolean {
        return result ?: false
    }

    fun getTitle(): String {
        return title ?: ""
    }
}

fun <T> RepositoryDTO<T>.mapToFailure(): Failure {
    return Failure(
        getCodeStatus(),
        getTitle(),
        getMessage(),
    )
}

fun <T> RepositoryDTO<T>?.mapStatus(): Status {
    if (this == null)
        return Status()
    return Status(
        getCodeStatus(),
        getMessage(),
    )
}

fun <T> RepositoryDTO<T>.mapToResultApi(): ResultApi<T> {
    return ResultApi(
        getData(),
        mapStatus()
    )
}

private fun <T> RepositoryDTO<T>?.mapToApiFilterResultState(): ApiFilterResultState<T> {
    if (this == null) {
        return ApiFilterResultState.OnError(Failure())
    }
    val state = if (getResult()) {
        ApiFilterResultState.OnSuccess(mapToResultApi())
    } else {
        when (getCodeStatus()) {
            StatusCodeServer.LICENSE_EXPIRED, StatusCodeServer.LOST_PERMISSION_COMPANY -> {
                ApiFilterResultState.OnLicenseExpired(
                    mapToFailure()
                )
            }

            StatusCodeServer.LOST_PERMISSION_PLACE_OR_CAMERA -> {
                ApiFilterResultState.OnLostPermissionPlaceOrCamera(
                    mapToFailure()
                )
            }
            else -> {
                ApiFilterResultState.OnError(
                    mapToFailure()
                )
            }
        }
    }
    return state
}

fun <T> Result<RepositoryDTO<T>>?.resultRxJava2MapToApiFilterResultState(): ApiFilterResultState<T>  {
    val response = this?.response()
    val body = response?.body()
    AppLog.d("RepositoryDTO",">> resultRxJava2MapToApiFilterResultState ${response?.message()} ${Gson().toJson(body)}")
    return when (response?.code()) {
        HttpsURLConnection.HTTP_OK -> {
            AppLog.d("RepositoryDTO",">> HttpsURLConnection.HTTP_OK")
            body.mapToApiFilterResultState()
        }
        HttpsURLConnection.HTTP_UNAUTHORIZED -> {
            ApiFilterResultState.OnUnAuthorized
        }
        else -> {
            //throw Exception()
            ApiFilterResultState.OnUnKnown(body?.mapStatus() ?: Status())
        }
    }
}
