package com.fss.mtcdemo.api.retrofit

/**
 * Author: bangdinh95@gmail.com
 * Date: 3/30/2022
 */
object StatusCodeServer {
    const val ERROR_UNKNOWN = -1
    const val LICENSE_EXPIRED = 1440 //giấy phép hết hạn hiện màn hình hết hạn
    const val LOST_PERMISSION_COMPANY =
        1445 //mất quyền truy cập công ty hiện màn hình mất quyền truy cập
    const val LOST_PERMISSION_PLACE_OR_CAMERA = 1447 //mất quyền thao tác hiện backdrop
    const val LOST_ACCESS_UAC = 1446 // user input không còn quyền truy cập vào công ty.
    const val CAMERA_CANNOT_APPLY_CODE =
        1449 // camera đã apply code trước đó, nên không thể apply thêm

    const val MOTIONS = 1401
}