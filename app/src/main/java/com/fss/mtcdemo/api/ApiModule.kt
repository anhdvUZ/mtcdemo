package com.fss.mtcdemo.api

import android.app.Application
import androidx.navigation.Navigator
import com.fss.mtcdemo.BuildConfig
import com.fss.mtcdemo.api.retrofit.ApiServiceOutSide
import com.fss.mtcdemo.utils.HostFactory
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.security.SecureRandom
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

/**
 * Created by anhdv31 on 7/7/2022.
 */
@Module
@InstallIn(SingletonComponent::class)
object ApiModule {
    
    /*    @Singleton
        @Provides
        fun providesHttpLoggingInterceptor() = HttpLoggingInterceptor()
            .apply {
                level = HttpLoggingInterceptor.Level.HEADERS
            }
        */
    @Provides
    @Singleton
    internal fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        return gsonBuilder.create()
    }
    
    @Provides
    @Singleton
    internal fun provideCache(application: Application): Cache {
        val cacheSize = (10 * 1024 * 1024).toLong() // 10 MB
        val httpCacheDirectory = File(application.cacheDir, "http-cache")
        return Cache(httpCacheDirectory, cacheSize)
    }
    
    @Singleton
    @Provides
    @Named("InSide")
    fun providesOkHttpClientInSide(cache: Cache): OkHttpClient {
        val trustAllCerts =
            arrayOf<TrustManager>(
                object : X509TrustManager {
                    override fun checkClientTrusted(
                        chain: Array<X509Certificate>,
                        authType: String
                    ) {
                    }
                    
                    override fun checkServerTrusted(
                        chain: Array<X509Certificate>,
                        authType: String
                    ) {
                    }
                    
                    override fun getAcceptedIssuers(): Array<X509Certificate> {
                        return arrayOf()
                    }
                }
            )
        
        // Install the all-trusting trust manager
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, SecureRandom())
        // Create an ssl socket factory with our all-trusting manager
        val sslSocketFactory = sslContext.socketFactory
        
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        
        val httpClient = OkHttpClient.Builder()
        httpClient.sslSocketFactory(sslSocketFactory, (trustAllCerts[0] as X509TrustManager))
        httpClient.cache(cache)
        //httpClient.addInterceptor(RequestInterceptor())
        /*if (Constants.IS_DEBUG){
            httpClient.addInterceptor(logging)
        }*/
        httpClient.addInterceptor(RequestInterceptor())
        httpClient.addInterceptor(logging)
        httpClient.connectTimeout(30, TimeUnit.SECONDS)
        return httpClient.build()
    }
    
    @Singleton
    @Provides
    @Named("OutSide")
    fun providesOkHttpClientOutSide(cache: Cache): OkHttpClient {
        val trustAllCerts =
            arrayOf<TrustManager>(
                object : X509TrustManager {
                    override fun checkClientTrusted(
                        chain: Array<X509Certificate>,
                        authType: String
                    ) {
                    }
                    
                    override fun checkServerTrusted(
                        chain: Array<X509Certificate>,
                        authType: String
                    ) {
                    }
                    
                    override fun getAcceptedIssuers(): Array<X509Certificate> {
                        return arrayOf()
                    }
                }
            )
        
        // Install the all-trusting trust manager
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, SecureRandom())
        // Create an ssl socket factory with our all-trusting manager
        val sslSocketFactory = sslContext.socketFactory
        
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        
        val httpClient = OkHttpClient.Builder()
        httpClient.sslSocketFactory(sslSocketFactory, (trustAllCerts[0] as X509TrustManager))
        httpClient.cache(cache)
        //httpClient.addInterceptor(RequestInterceptor())
        /*if (Constants.IS_DEBUG){
            httpClient.addInterceptor(logging)
        }*/
        httpClient.addInterceptor(RequestInterceptorOutSide())
        httpClient.addInterceptor(logging)
        httpClient.connectTimeout(30, TimeUnit.SECONDS)
        return httpClient.build()
    }
    
/*    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson,okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .build()*/
    
    @Provides
    @Singleton
    @Named("InSide")
    fun provideRetrofitInSide(gson: Gson, @Named("InSide")okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(HostFactory.API_HOST)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()
    }
    
    @Provides
    @Singleton
    @Named("OutSide")
    fun provideRetrofitOutSide(gson: Gson, @Named("OutSide")okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(HostFactory.API_HOST)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()
    }
    
    @Singleton
    @Provides
    fun provideApiServiceInSide(@Named("InSide")retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)
    
    @Singleton
    @Provides
    fun provideApiServiceOutSide(@Named("OutSide")retrofit: Retrofit): ApiServiceOutSide = retrofit.create(ApiServiceOutSide::class.java)
    
    /* @Singleton
     @Provides
     fun providesRepository(apiService: ApiService) = LoginRepository(apiService)*/
}