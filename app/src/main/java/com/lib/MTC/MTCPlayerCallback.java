package com.lib.MTC;

public interface MTCPlayerCallback {
    void onResponse(int playHandle);
}

//public class MTCPlayerCallback {
//    private long swigCPtr;
//    protected boolean swigCMemOwn;
//
//    protected MTCPlayerCallback(long cPtr, boolean cMemoryOwn) {
//        swigCMemOwn = cMemoryOwn;
//        swigCPtr = cPtr;
//    }
//
//    protected static long getCPtr(MTCPlayerCallback obj) {
//        return (obj == null) ? 0 : obj.swigCPtr;
//    }

//    protected void finalize() {
//        delete();
//    }
//
//    public synchronized void delete() {
//        if (swigCPtr != 0) {
//            if (swigCMemOwn) {
//                swigCMemOwn = false;
//                MTC.delete_PjsuaAppCallback(swigCPtr);
//            }
//            swigCPtr = 0;
//        }
//    }
//}
