package com.lib.MTC.struct;

public class EventItem {
    public int st_0_channel;                 /* Channel number */
    public int st_1_fileType;                /* refer to MTSP_FILE_TYPE_E */
    public int st_2_size;                    /* Size file */
    public byte[] st_3_fileName = new byte[88];           /* File name*/
    public SystemTime st_4_startTime = new SystemTime(); /* Start time */
    public SystemTime st_5_endTime = new SystemTime();   /* Stop time */
}
