package com.lib.MTC.struct;

public class EventHandle {
    public int st_0_channel;    // Channel camera
    public int st_1_eventLatch; // Linkage start delay time, s in units
    public int st_2_interval;
    public int st_3_recordChannel;

    public int st_4_bAlarmOut;
    public int st_5_bFTP;
    public int st_6_bLog;
    public int st_7_bMail;
    public int st_8_bMessage;
    public int st_9_bSnap;
    public int st_a0_bRecordEn;
    public int st_b0_bVoice;

    public Worksheet st_c0_workSheet = new Worksheet();
}
