package com.lib.MTC.struct;

public class EventListReq {
    public int st_0_channel;
    public int st_1_fileType;                /* refer to MTSP_FILE_TYPE_E */
    public SystemTime st_2_startTime = new SystemTime(); /* Start time */
    public SystemTime st_3_endTime = new SystemTime();   /* Stop time */
}
