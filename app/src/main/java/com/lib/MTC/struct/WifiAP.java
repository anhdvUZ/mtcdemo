package com.lib.MTC.struct;

public class WifiAP {
    public byte[] st_0_ssid = new byte[36];            // wifi ssid
    public int st_1_rssi;       // refer to MTSP_RSSI_SINGNAL_E
    public int st_2_mode;       // refer to MTSP_AP_MODE_E
    public int st_3_encrypType; // refer to MTSP_AP_ENCTYPE_E
    public int st_4_status;     // refer to MTSP_WIFI_STATUS_E
}
