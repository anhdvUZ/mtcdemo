package com.lib.MTC.struct;

import com.lib.MTC.basic.Futils;

public class DevicePlayInfo {
    public int st_0_channel; // Stream channle : 0 main, 1 sub
    public int st_1_mode;	 // Feature
    public int st_2_type;	 // Feature

    public byte[] ToBytes()
    {
       return Futils.ObjToBytes(this);
    }
}
