package com.lib.MTC.struct;

public class TimeSection {
    public int st_0_bEnable; /* 1: Enable; 0: Disable */
    public int st_1_startHour;
    public int st_2_startMinute;
    public int st_3_startSecond;
    public int st_4_endHour;
    public int st_5_endMinute;
    public int st_6_endSecond;

    public String getStartTime()
    {
        return String.format("%02d", st_1_startHour) + ":" + String.format("%02d", st_2_startMinute) + ":" + String.format("%02d", st_3_startSecond);
    }

    public String getStopTime()
    {
        return String.format("%02d", st_4_endHour) + ":" + String.format("%02d", st_5_endMinute) + ":" + String.format("%02d", st_6_endSecond);
    }
}
