package com.lib.MTC.struct;

public class WifiDeviceList {
    public int st_0_number;
    public WifiAP[] st_1_wifiDeviceAll = new WifiAP[32];

    public WifiDeviceList() {
        int i;
        for (i = 0; i < st_1_wifiDeviceAll.length; i++) {
            st_1_wifiDeviceAll[i] = new WifiAP();
        }
    }
}
