package com.lib.MTC.struct;

public class SystemTime {
    public int st_0_year;   //
    public int st_1_month;  // anuary = 1, February = 2, and so on.
    public int st_2_day;    // 1 - 31
    public int st_3_wday;   // Sunday = 0, Monday = 1, and so on
    public int st_4_hour;   // 0 - 23
    public int st_5_minute; // 0 - 59
    public int st_6_second; // 0 - 59
    public int st_7_dst;    //

    public String getTimeHour()
    {
        return String.format("%02d", st_4_hour) + ":" + String.format("%02d", st_5_minute) + ":" + String.format("%02d", st_6_second);
    }
}
