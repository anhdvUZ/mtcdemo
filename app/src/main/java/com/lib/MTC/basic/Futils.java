package com.lib.MTC.basic;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Arrays;

public class Futils {
    public Futils() {
    }

    public static String ByteToString(byte[] b)
    {
        int i;
        String s;
        int t = 0;
        for(i = 0; i < b.length; i++)
        {
            if (b[i] == 0)
            {
                t = i;
                break;
            }

        }
        if ( t >0) {
            byte[] n = Arrays.copyOf(b, t);
            s = new String(n);
        } else
        {
            s = new String();
        }
        return s;

    }

    public static String TimeToString(int time) {
        return String.format("%02d", time / 60) + ":" + String.format("%02d", time % 60);
    }

    public static byte[] IntToBytes(int i) {
        byte[] result = new byte[]{(byte) (i & 255), (byte) (i >> 8 & 255), (byte) (i >> 16 & 255), (byte) (i >> 24 & 255)};
        return result;
    }


    public static byte[] LongToBytes(long i) {
        byte[] result = new byte[]{(byte) ((int) (i & 255L)), (byte) ((int) (i >> 8 & 255L)), (byte) ((int) (i >> 16 & 255L)), (byte) ((int) (i >> 24 & 255L)), (byte) ((int) (i >> 32 & 255L)), (byte) ((int) (i >> 40 & 255L)), (byte) ((int) (i >> 48 & 255L)), (byte) ((int) (i >> 56 & 255L))};
        return result;
    }

    public static byte[] ToBytes(int i) {
        return IntToBytes(i);
    }

    public static byte[] ShortToBytes(int i) {
        byte[] result = new byte[]{(byte) (i & 255), (byte) (i >> 8 & 255)};
        return result;
    }

    public static short ToShort(byte[] bytes, int start) {
        if (bytes != null && start >= 0 && start + 2 <= bytes.length) {
            int[] ia = new int[2];

            for (int i = 0; i < 2; ++i) {
                ia[i] = bytes[i + start] < 0 ? bytes[i + start] + 256 : bytes[i + start];
            }

            return (short) (ia[1] << 8 | ia[0]);
        } else {
            return 0;
        }
    }

    public static int ToInt(byte[] bytes, int start) {
        if (bytes != null && start >= 0 && start + 4 <= bytes.length) {
            int[] ia = new int[4];

            for (int i = 0; i < 4; ++i) {
                ia[i] = bytes[i + start] < 0 ? bytes[i + start] + 256 : bytes[i + start];
            }

            return ia[3] << 24 | ia[2] << 16 | ia[1] << 8 | ia[0];
        } else {
            return 0;
        }
    }

    public static long ToLong(byte[] bytes, int start) {
        if (bytes != null && start >= 0 && start + 8 <= bytes.length) {
            long[] ia = new long[8];

            for (int i = 0; i < 8; ++i) {
                ia[i] = (long) (bytes[i + start] < 0 ? bytes[i + start] + 256 : bytes[i + start]);
            }

            return ia[3] << 56 | ia[2] << 48 | ia[1] << 40 | ia[0] << 32 | ia[7] << 24 | ia[6] << 16 | ia[5] << 8 | ia[4];
        } else {
            return 0L;
        }
    }

    public static float ToFloat(byte[] bytes, int start) {
        return Float.intBitsToFloat(ToInt(bytes, start));
    }

    public static byte[] ObjToBytes(Object obj) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ObjToBytes(obj, os);
        return os.toByteArray();
    }

    public static byte[] ObjToBytes(Object[] objs) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        for (int j = 0; j < objs.length; ++j) {

            ObjToBytes(objs[j], os);
        }

        return os.toByteArray();
    }

    public static int BytesToObj(Object obj, byte[] bytes) {
        int[] i = new int[]{0};
        return BytesToObj(obj, bytes, i);
    }

    public static int BytesToObj(Object[] objs, byte[] bytes) {
        return BytesToObj((Object[])objs, bytes, 0);
    }

    public static int BytesToObj(Object[] objs, byte[] bytes, int nStart) {
        int[] i = new int[]{nStart};

        for(int j = 0; j < objs.length; ++j) {
            BytesToObj(objs[j], bytes, i);
        }

        return 0;
    }

    public static int BytesToObj(Object obj, byte[] bytes, int nStart) {
        int[] i = new int[]{nStart};
        return BytesToObj(obj, bytes, i);
    }

    /*--------------------------------------------- Private ------------------------------------------------*/
    private static int ObjToBytes(Object obj, ByteArrayOutputStream os) {
        try {
            Field[] field = GetClassFields(obj);

            for (int j = 0; j < field.length; ++j) {
                String name = field[j].getName();
//                System.out.printf("MTC feild : [" + name + "]\n");
                if (name.startsWith("st_")) {
                    (new StringBuilder(String.valueOf(name.substring(0, 1).toUpperCase()))).append(name.substring(1)).toString();
                    String type = field[j].getGenericType().toString();
                    Type t = field[j].getGenericType();
                    if (t == Integer.TYPE) {
                        os.write(IntToBytes(field[j].getInt(obj)));
                    } else if (t == Byte.TYPE) {
                        os.write(field[j].getByte(obj));
                    } else if (t == Boolean.TYPE) {
                        byte buf = (byte) (field[j].getBoolean(obj) ? 1 : 0);
                        os.write(buf);
                    } else if (t == Short.TYPE) {
                        os.write(ShortToBytes(field[j].getShort(obj)));
                    } else if (t == Long.TYPE) {
                        os.write(LongToBytes(field[j].getLong(obj)));
                    } else if (!type.startsWith("class [")) {
                        ObjToBytes(field[j].get(obj), os);
                    } else {
                        String tm = type.toString();
                        int nLevel = 1;
                        char cType = 0;

                        for (int f = 7; f < tm.length(); ++f) {
                            cType = tm.charAt(f);
                            if (cType != '[') {
                                break;
                            }

                            ++nLevel;
                        }

                        ObjArrayToBytes(cType, field[j].get(obj), os, nLevel);
                    }
                }
            }

            return 0;
        } catch (Exception var11) {
            var11.printStackTrace();
            return -1;
        }
    }

    private static int ObjArrayToBytes(char type, Object obj, ByteArrayOutputStream os, int nLevel) throws IOException {
        int k;
        Object[] vs;
        if (nLevel == 1) {
            if (type == 'B') {
                byte[] bytes = (byte[]) obj;
                os.write(bytes);
            } else if (type == 'I') {
                int[] vs1 = (int[]) obj;

                for (k = 0; k < vs1.length; ++k) {
                    os.write(IntToBytes(vs1[k]));
                }

            } else {
                vs = (Object[]) obj;

                for (k = 0; k < vs.length; ++k) {
                    ObjToBytes(vs[k], os);
                }
            }
        } else {
            vs = (Object[]) obj;
            --nLevel;

            for (k = 0; k < vs.length; ++k) {
                ObjArrayToBytes(type, vs[k], os, nLevel);
            }
        }

        return 0;
    }

    private static int BytesToObj(Object obj, byte[] bytes, int[] i) {
        try {
            Field[] field = GetClassFields(obj);

            for(int j = 0; j < field.length; ++j) {
                String name = field[j].getName();
                if (name.startsWith("st_")) {
                    (new StringBuilder(String.valueOf(name.substring(0, 1).toUpperCase()))).append(name.substring(1)).toString();
                    String type = field[j].getGenericType().toString();
                    Type t = field[j].getGenericType();
                    if (t == Integer.TYPE) {
                        field[j].setInt(obj, ToInt(bytes, i[0]));
                        i[0] += 4;
                    } else {
                        int var10002;
                        if (t == Byte.TYPE) {
                            field[j].setByte(obj, bytes[i[0]]);
                            var10002 = i[0]++;
                        } else if (t == Boolean.TYPE) {
                            field[j].setBoolean(obj, bytes[i[0]] == 1);
                            var10002 = i[0]++;
                        } else if (t == Short.TYPE) {
                            field[j].setShort(obj, ToShort(bytes, i[0]));
                            i[0] += 2;
                        } else if (t == Long.TYPE) {
                            field[j].setLong(obj, ToLong(bytes, i[0]));
                            i[0] += 8;
                        } else if (t == Float.TYPE) {
                            field[j].setFloat(obj, ToFloat(bytes, i[0]));
                            i[0] += 4;
                        } else if (!type.startsWith("class [")) {
                            BytesToObj(field[j].get(obj), bytes, i);
                        } else {
                            String tm = type.toString();
                            int nLevel = 1;
                            char cType = 0;

                            for(int f = 7; f < tm.length(); ++f) {
                                cType = tm.charAt(f);
                                if (cType != '[') {
                                    break;
                                }

                                ++nLevel;
                            }

                            BytesArrayToObj(cType, field[j].get(obj), bytes, i, nLevel);
                        }
                    }
                }
            }

            return 0;
        } catch (Exception var12) {
            var12.printStackTrace();
            return -1;
        }
    }

    private static int BytesArrayToObj(char type, Object obj, byte[] bytes, int[] i, int nLevel) throws IOException {
        if (obj == null) {
            return 0;
        } else {
            int k;
            Object[] v;
            if (nLevel == 1) {
                if (type == 'B') {
                    byte[] v1 = (byte[])obj;
                    System.arraycopy(bytes, i[0], v1, 0, v1.length);
                    i[0] += v1.length;
                } else if (type == 'I') {
                    int[] v1 = (int[])obj;

                    for(k = 0; k < v1.length; ++k) {
                        v1[k] = ToInt(bytes, i[0]);
                        i[0] += 4;
                    }
                } else {
                    v = (Object[])obj;

                    for(k = 0; k < v.length; ++k) {
                        BytesToObj(v[k], bytes, i);
                    }
                }
            } else {
                v = (Object[])obj;
                --nLevel;

                for(k = 0; k < v.length; ++k) {
                    BytesArrayToObj(type, v[k], bytes, i, nLevel);
                }
            }

            return 0;
        }
    }

    static Field[] GetClassFields(Object obj) {
        Field[] field = obj.getClass().getFields();
        if (field != null && field.length > 0) {
            int count = field.length;
            Field[] ret = new Field[count];
            int[] intRet = new int[count];

            int k;
            for (k = 0; k < count; ++k) {
                intRet[k] = -1;
            }

            k = 0;
            String kName = "";

            for (int i = 0; i < count; ++i) {
                int j;
                for (j = 0; j < count; ++j) {
                    if (intRet[j] == -1) {
                        k = j;
                        break;
                    }
                }

                kName = field[k].getName();

                for (j = 0; j < count; ++j) {
                    if (intRet[j] == -1) {
                        String name = field[j].getName();
                        if (name.compareTo(kName) < 0) {
                            kName = name;
                            k = j;
                        }
                    }
                }

                intRet[k] = 1;
                ret[i] = field[k];
            }

            return ret;
        } else {
            return null;
        }
    }

}