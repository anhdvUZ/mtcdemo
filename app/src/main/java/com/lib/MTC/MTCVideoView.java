package com.lib.MTC;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.view.Surface;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.lib.MTC.basic.Futils;
import com.lib.MTC.struct.DevicePlayInfo;
import com.lib.MTC.struct.EventItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Base64;


/**
 * Created by PhucNV17 on 3/2/2022.
 */
public class MTCVideoView extends LinearLayout implements OnMTCMediaListener {
    private String TAG = getClass().getSimpleName();
    private Context mContext;
    private ImageView mImage;
    private AudioTrack mAudioTrack = null;
    private int mUserID = -1;
    private int mChannel = -1;
    private int mloginID = -1;
    private String mDeviceSn = "";
    private int mPlayHandle = -1;
    private boolean mInited = false;
    private boolean bLoginRun = false;
    private boolean bVidListRun = false;

    private int sampleRate = 8000;//44100;
    private int channelConfig = AudioFormat.CHANNEL_OUT_MONO;
    private int channelCount = 1;
    private int audioFormat = AudioFormat.ENCODING_PCM_16BIT;
    int minBufSize = AudioTrack.getMinBufferSize(sampleRate, channelConfig, audioFormat);
//    Au.Encoder encoder = new G711UCodec();


    private MediaCodec mMediaCodec;// Media decoder
    private MediaFormat mMediaformat; // video format

    private MainHandler handler = new MainHandler();

    private OnMTCMediaListener mOnListener = null;

    /* Play Record control type */
    public static final int MTSP_RECORD_PLAY_STOP = 0;
    public static final int MTSP_RECORD_PLAY_START = 1;
    public static final int MTSP_RECORD_PLAY_PAUSE = 2;
    public static final int MTSP_RECORD_PLAY_SEEKTIME = 3;
    public static final int MTSP_RECORD_PLAY_END = 4;


    /* message type */
    public static final int MSGTYPE_VIDEO_LIST = 1;
    public static final int MSGTYPE_DURATION = 2;
    public static final int MSGTYPE_TIME = 3;
    public static final int MSGTYPE_START = 4;
    public static final int MSGTYPE_END = 5;
    public static final int MSGTYPE_IMG = 6;

//    Handler handler;

    // private MediaPlayer.OnInfoListener mInfoListener = null;

    public void setOnInfoListener(OnMTCMediaListener listener) {
        mOnListener = listener;
    }

    public void UnSetOnInfoListener() {
        mOnListener = null;
    }

    public MTCVideoView(Context context) {
        super(context);
    }

    public MTCVideoView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public MTCVideoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    public MTCVideoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
        init();
    }

    private void init() {
        if (mUserID == -1) {
            mUserID = 0;
            //mUserID = FssSDK.RegUser(this);
            mPlayHandle = MTC.Play_OpenStream();
            MTC.Play_SetOnMediaListener(mPlayHandle, this);
            int ret = MTC.Play_Start(mPlayHandle);
            System.out.printf("phucnv17 playhandle [%d]\n", mPlayHandle);
            if (ret != 0) {
                mPlayHandle = -1;
                System.out.printf("phucnv17 VideoView Media play start fail\n");
            }

            mAudioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                    sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO,
                    AudioFormat.ENCODING_PCM_16BIT, minBufSize,
                    AudioTrack.MODE_STREAM);
            mAudioTrack.play();

            /* decode audio */
            try {
                String codec_type = MediaFormat.MIMETYPE_AUDIO_G711_ALAW;
                mMediaformat = MediaFormat.createAudioFormat(codec_type, sampleRate, channelCount);
                // Set the buffer size
                mMediaformat.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, 1000);

                mMediaCodec = MediaCodec.createDecoderByType(MediaFormat.MIMETYPE_AUDIO_G711_ALAW);

                // Configure the Codec
                mMediaCodec.configure(mMediaformat, null, null, 0);
                // Start the codec
                mMediaCodec.start();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void initSurfaceView() {
        if (null == mImage) {
            Log.d(TAG, "phucnv17 init image View ");
            mImage = new ImageView(getContext());
            mImage.setLongClickable(true);
            LayoutParams lp = new LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            this.addView(mImage, lp);
            mImage.requestLayout();
//            mImage.setColorFilter(Color.YELLOW);
//            mImage.setBackgroundColor(Color.GRAY);


        }
    }

    private void audioDecode(byte[] data, int size) {

        try {
            // Get the input buffer from the decoder
            int inputIndex = mMediaCodec.dequeueInputBuffer(-1);// Pass in -1 here as in this example we don't have a playback time reference

            // If  the buffer number is valid use the buffer with that index
            if (inputIndex >= 0) {
                ByteBuffer buffer = mMediaCodec.getInputBuffer(inputIndex);
                buffer.put(data);
                // tell the decoder to process the frame
                mMediaCodec.queueInputBuffer(inputIndex, 0, size, 0, 0);
            }

            MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
            int outputIndex = mMediaCodec.dequeueOutputBuffer(info, 0);

            if (outputIndex >= 0) {
                ByteBuffer outputBuffer;
                outputBuffer = mMediaCodec.getOutputBuffer(outputIndex);
                byte[] buffer = new byte[info.size];
                outputBuffer.get(buffer);
                mAudioTrack.write(buffer, 0, buffer.length);
                mMediaCodec.releaseOutputBuffer(outputIndex, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ReleaseCodec() {
        try {
            if (mMediaCodec != null) {
                mMediaCodec.stop();
                mMediaCodec.release();
                mMediaCodec = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        if (!mInited) {
            initSurfaceView();
            mInited = true;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private int getUserId() {
        return mUserID;
    }

//        public void setStreamType(FunStreamType streamType) {
//            mStreamType = streamType;
//        }

    public void release() {
        handler = null;
        Log.d(TAG, "release " + mUserID);
        if (mloginID >= 0)
            MTC.DEV_StopPlay(mloginID);

        MTC.Play_RmOnMediaListener(mPlayHandle);
        MTC.Play_CloseStream(mPlayHandle);
        ReleaseCodec();

        if (mAudioTrack != null)
            mAudioTrack.release();


        mImage = null;
        mPlayHandle = -1;
        mloginID = -1;
        mOnListener = null;
    }

    public void setRealDevice(int loginID, int channel) {
        mloginID = loginID;

        DevicePlayInfo devicePlayInfo = new DevicePlayInfo();
        devicePlayInfo.st_0_channel = channel;
        devicePlayInfo.st_1_mode = 0;
        devicePlayInfo.st_2_type = 0;

        int ret = MTC.DEV_SetPlayHandle(loginID, mPlayHandle);
        System.out.printf("phucnv17 DEV_SetPlayHandle [%d] playHandle [%d]\n", ret, mPlayHandle);
        ret = MTC.DEV_StartRealPlay(loginID, devicePlayInfo.ToBytes());
        System.out.printf("phucnv17 startRealPlay [%d] playHandle [%d]\n", ret, mPlayHandle);
        if (ret != 0) {
            System.out.printf("phucnv17 DEV_StartRealPlay error [%d]n\n", ret);
        }
    }

    public void setPlayRecordStart(int loginID, EventItem event)
    {
        mloginID = loginID;
        int ret = MTC.DEV_SetPlayHandle(loginID, mPlayHandle);

        MTC.DEV_PlayRecordStop(loginID);

        if (ret == 0)
        {
            ret = MTC.DEV_PlayRecordStart(loginID, Futils.ObjToBytes(event));
        }
        if (ret != 0)
            System.out.printf("phucnv17 DEV_PlayRecord error [%d]n\n", ret);
    }

    public void playRecordStop()
    {
        MTC.DEV_PlayRecordStop(mloginID);
    }

    public void cloudSearchByTime(String serial, String start_time, String stop_time) {
//        String[] videoList = new String[1];
//        int ret = MTC.Play_CloudSearchByTime(mPlayHandle, serial, start_time, stop_time, videoList);
//        System.out.printf("phucnv17 Play_CloudSearchByTime [%d] playHandle [%d]\n", ret, mPlayHandle);
//        if (ret == 0)
//        {
//            System.out.printf("phucnv17 searchbytime success ["+ videoList[0] + "]\n");
//        } else {
//            System.out.printf("phucnv17 searchbytime error\n");
//        }
        if (!bVidListRun) {
            bVidListRun = true;
            Thread thread = new Thread(new GetVideoListThread(serial, start_time, stop_time, ""),
                    "Create GetVideoList Thread");
            thread.start();
        }
    }

    public void cloudUrlStart(String url) {
        int ret = MTC.Play_CloudUrlStart(mPlayHandle, url);
        System.out.printf("phucnv17 CloudUrlStart [%d]\n", ret);
    }

    public void SetDecryptKey(String key)
    {
        int ret = MTC.Play_SetDecryptKey(mPlayHandle, key);
        System.out.printf("phucnv17 Play_SetDecryptKey [%d]\n", ret);
    }

    public void SetDecryptEnable(int enable)
    {
        int ret = MTC.Play_SetDecryptEnable(mPlayHandle, enable);
        System.out.printf("phucnv17 Play_SetDecryptEnable [%d]\n", ret);
    }

    private class GetVideoListThread implements Runnable {
        String serial;
        String StartTime;
        String StopTime;
        String Auth;

        public GetVideoListThread(String serial, String StartTime, String StopTime, String Auth) {
            this.serial = serial;
            this.StartTime = StartTime;
            this.StopTime = StopTime;
            this.Auth = Auth;
        }

        @Override
        public void run() {
            System.out.println("Player get Video List start...");
            do {
                String[] VideoList = new String[1];
                int ret = MTC.Play_CloudSearchByTime(mPlayHandle, serial, StartTime, StopTime, VideoList);
                if (ret == 0) {
                    System.out.printf("GetVideoList success\n");
                    try {
                        JSONObject doc = new JSONObject(VideoList[0]);

                        JSONObject data_obj = (JSONObject) doc.get("data");
                        JSONArray VideoList_obj = (JSONArray) data_obj.get("obj_list");

                        String mString = VideoList_obj.toString();

                        System.out.printf("GetVideoList success [%s]\n", mString, "\n");

//                        Message msg = new Message();
//                        msg.what = MSGTYPE_VIDEO_LIST;
//                        msg.obj = VideoList_obj;
//                        handler.sendMessage(msg);
                        if (mOnListener != null)
                            mOnListener.MediaVideoList(mString);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.printf("GetVideoList false %d\n", ret);
                    break;
                }
            } while (false);
            bVidListRun = false;
        }
    }

    @Override
    public void MediaFrameData(byte[] data, int size, int width, int height) {

//        System.out.printf("phucnv17 Listener MediaFrameData [%d] %dx%d [%02X %02X %02X %02X]\n", size, width, height, data[0], data[1], data[2], data[3]);

        final Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        // Wrap our array of pixels into a ByteBuffer object
        ByteBuffer ourBuffer = ByteBuffer.wrap(data);

        // Copy pixels from the ByteBuffer that wrapped our array of bytes
        bitmap.copyPixelsFromBuffer(ourBuffer);


        if (handler != null) {
            Message msg = new Message();
            msg.what = MSGTYPE_IMG;
            msg.obj = bitmap;
            handler.sendMessage(msg);
        }
    }


    public void MediaAudio(byte[] data, int size) {
        audioDecode(data, size);
        /* send talk audio */
//        MTC.DEV_AudioSend(mloginID, data, size);
    }


    @Override
    public void MediaDuration(int duration) {
        System.out.printf("phucnv17 Listener MediaDuration %d\n", duration);
        if (mOnListener != null)
            mOnListener.MediaDuration(duration);
    }

    @Override
    public void MediaInfo(int codecID, int width, int height) {
        System.out.printf("phucnv17 Listener MediaInfo %d %dx%d\n", codecID, width, height);
        if (mOnListener != null)
            mOnListener.MediaStart();
    }

    @Override
    public void MediaStart() {
        System.out.printf("phucnv17 Listener MediaStart\n");
    }

    @Override
    public void MediaSnapshot() {
        System.out.printf("phucnv17 Listener MediaSnapshot\n");
    }

    @Override
    public void MediaRecordStart() {
        System.out.printf("phucnv17 Listener MediaRecordStart\n");
    }

    @Override
    public void MediaStop() {
        System.out.printf("phucnv17 Listener MediaStop\n");

    }

    @Override
    public void MediaVideoList(String videoList) {

    }

    @Override
    public void MediaPlayTime(int playTime) {

//        System.out.printf("phucnv17 listener playTime [%d]\n", playTime);
        if (mOnListener != null)
            mOnListener.MediaPlayTime(playTime);
    }


    private class PlayThread implements Runnable {
        int loginID;
        int channel;

        public PlayThread(int loginID, int channel) {
            this.loginID = loginID;
            this.channel = channel;

            DevicePlayInfo devicePlayInfo = new DevicePlayInfo();
            devicePlayInfo.st_0_channel = channel;
            devicePlayInfo.st_1_mode = 0;
            devicePlayInfo.st_2_type = 0;


            new Handler().postDelayed(() -> {
                int ret = MTC.DEV_SetPlayHandle(loginID, mPlayHandle);
                System.out.printf("phucnv17 DEV_SetPlayHandle [%d] playHandle [%d]\n", ret, mPlayHandle);
                ret = MTC.DEV_StartRealPlay(loginID, devicePlayInfo.ToBytes());
                System.out.printf("phucnv17 startRealPlay [%d] playHandle [%d]\n", ret, mPlayHandle);
                if (ret != 0) {
                    System.out.printf("phucnv17 DEV_StartRealPlay error [%d]n\n", ret);
                }
            }, 500);


//            MTC.DEV_SetPlayHandle(MTC.loginID, mPlayHandle);
//            int ret = MTC.DEV_StartRealPlay(loginID, devicePlayInfo.ToBytes());
//            System.out.printf("phucnv17 startRealPlay [%d] playHandle [%d]\n", ret, mPlayHandle);
//            if (ret != 0) {
//                System.out.printf("phucnv17 DEV_StartRealPlay error [%d]\n", ret);
//            }


        }

        @Override
        public void run() {
            bLoginRun = false;
        }
    }

    class MainHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            handleMsg(msg);
            super.handleMessage(msg);
        }

        void handleMsg(Message msg) {
//            System.out.printf("handleMsg [%d]\n", msg.what);
            switch (msg.what) {
                case MSGTYPE_IMG: {
                    final Bitmap bitmap = (Bitmap) msg.obj;
                    if (bitmap != null)
                        try {
                            if (mImage != null)
                            {
                                mImage.clearColorFilter();
                                mImage.setImageBitmap(bitmap);
                            }

                        } catch (Exception err) {

                        }
                    break;
                }
                default:
                    break;
            }
        }
    }
}

