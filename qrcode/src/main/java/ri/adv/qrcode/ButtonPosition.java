package ri.adv.qrcode;

/**
 * Button position
 *
 * @see com.budiyev.android.codescanner.CodeScannerView#getAutoFocusButtonPosition
 * @see com.budiyev.android.codescanner.CodeScannerView#setAutoFocusButtonPosition
 * @see com.budiyev.android.codescanner.CodeScannerView#getFlashButtonPosition
 * @see com.budiyev.android.codescanner.CodeScannerView#setFlashButtonPosition
 */
public enum ButtonPosition {
    TOP_START,
    TOP_END,
    BOTTOM_START,
    BOTTOM_END
}
